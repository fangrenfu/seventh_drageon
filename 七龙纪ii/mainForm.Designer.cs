﻿namespace 七龙纪II
{
    partial class mainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.workStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.timesStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.timeOutStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.goldStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.resStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageCurrent = new System.Windows.Forms.TabPage();
            this.webBrowser = new 七龙纪II.ExtendedWebBrowser();
            this.tabPageSetting = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btSearch = new System.Windows.Forms.Button();
            this.ComboBoxSearch = new System.Windows.Forms.ComboBox();
            this.tbGoldMax = new System.Windows.Forms.TextBox();
            this.cbGoldMax = new System.Windows.Forms.CheckBox();
            this.tbGoldMin = new System.Windows.Forms.TextBox();
            this.cbGoldMin = new System.Windows.Forms.CheckBox();
            this.groupbBox = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbBuyResGold = new System.Windows.Forms.TextBox();
            this.cbBuyRes = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbStoreRest = new System.Windows.Forms.TextBox();
            this.cbFullBuildStore = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.udTaxHours = new System.Windows.Forms.NumericUpDown();
            this.cbAutoTax = new System.Windows.Forms.CheckBox();
            this.cbAutoHero = new System.Windows.Forms.CheckBox();
            this.cbAutoGov = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbAutoHunt = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.udAttackRange = new System.Windows.Forms.NumericUpDown();
            this.cbWind = new System.Windows.Forms.CheckBox();
            this.cbWater = new System.Windows.Forms.CheckBox();
            this.cbWild = new System.Windows.Forms.CheckBox();
            this.cbHell = new System.Windows.Forms.CheckBox();
            this.gbMission = new System.Windows.Forms.GroupBox();
            this.cbAutoMission = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.udMinStep = new System.Windows.Forms.NumericUpDown();
            this.gbBasic = new System.Windows.Forms.GroupBox();
            this.brRemoteDatabase = new System.Windows.Forms.RadioButton();
            this.rbLocalDatabase = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.udOperateTimeOut = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.udReadTimes = new System.Windows.Forms.NumericUpDown();
            this.gbBuildUpdate = new System.Windows.Forms.GroupBox();
            this.udUseDiamond = new System.Windows.Forms.NumericUpDown();
            this.cbUseDiamond = new System.Windows.Forms.CheckBox();
            this.udStoreLevel = new System.Windows.Forms.NumericUpDown();
            this.udFoodLevel = new System.Windows.Forms.NumericUpDown();
            this.cbStoreUpdate = new System.Windows.Forms.CheckBox();
            this.udCrystalLevel = new System.Windows.Forms.NumericUpDown();
            this.cbFoodUpdate = new System.Windows.Forms.CheckBox();
            this.cbCrystalUpdate = new System.Windows.Forms.CheckBox();
            this.udShopLevel = new System.Windows.Forms.NumericUpDown();
            this.cbShopUpdate = new System.Windows.Forms.CheckBox();
            this.udWoodLevel = new System.Windows.Forms.NumericUpDown();
            this.cbWoodUpdate = new System.Windows.Forms.CheckBox();
            this.udStoneLevel = new System.Windows.Forms.NumericUpDown();
            this.cbStoneUpdate = new System.Windows.Forms.CheckBox();
            this.cbAutoBuild = new System.Windows.Forms.CheckBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.序号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.账户 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.坐标 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.等级 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.挂单 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.任务 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.金币 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.钻石 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.仓库 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.木材 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.石头 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.水晶 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.食物 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.udClearStoreTime = new System.Windows.Forms.NumericUpDown();
            this.btAddPrice = new System.Windows.Forms.Button();
            this.btSubPrice = new System.Windows.Forms.Button();
            this.udKeepHours = new System.Windows.Forms.NumericUpDown();
            this.udFoodPrice = new System.Windows.Forms.NumericUpDown();
            this.udStonePrice = new System.Windows.Forms.NumericUpDown();
            this.udCrystalPrice = new System.Windows.Forms.NumericUpDown();
            this.udWoodPrice = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.cbAutoSell = new System.Windows.Forms.CheckBox();
            this.cbStopNight = new System.Windows.Forms.CheckBox();
            this.cbClearStore = new System.Windows.Forms.CheckBox();
            this.cbFoodSell = new System.Windows.Forms.CheckBox();
            this.cbCrystalSell = new System.Windows.Forms.CheckBox();
            this.cbStoneSell = new System.Windows.Forms.CheckBox();
            this.cbWoodSell = new System.Windows.Forms.CheckBox();
            this.tabPageProxy = new System.Windows.Forms.TabPage();
            this.tbProxyList = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btProxyCopy = new System.Windows.Forms.Button();
            this.tbProxyProtocolSplit = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbProxyPortSplit = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btProxyWeb = new System.Windows.Forms.Button();
            this.tbProxyReg = new System.Windows.Forms.TextBox();
            this.btGetProxy = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tbProxyURL = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPageLog = new System.Windows.Forms.TabPage();
            this.btClearWorkLogs = new System.Windows.Forms.Button();
            this.tbWorkLogs = new System.Windows.Forms.TextBox();
            this.tabPageUpdate = new System.Windows.Forms.TabPage();
            this.tbUpdateLog = new System.Windows.Forms.TextBox();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.ComboBoxServer = new System.Windows.Forms.ToolStripComboBox();
            this.ComboBoxSection = new System.Windows.Forms.ToolStripComboBox();
            this.ComboBoxAccount = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.btEnterUser = new System.Windows.Forms.ToolStripButton();
            this.btLastUser = new System.Windows.Forms.ToolStripButton();
            this.btNextUser = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ComboBoxAutoStyle = new System.Windows.Forms.ToolStripComboBox();
            this.btStartAuto = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btRegister = new System.Windows.Forms.ToolStripButton();
            this.tbRegCount = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btGetUserInfo = new System.Windows.Forms.ToolStripButton();
            this.btUnLockUser = new System.Windows.Forms.ToolStripButton();
            this.btSaveSetting = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btDeleteAccount = new System.Windows.Forms.ToolStripButton();
            this.btClearStore = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btDownLoad = new System.Windows.Forms.ToolStripButton();
            this.timerWait = new System.Windows.Forms.Timer(this.components);
            this.timerOut = new System.Windows.Forms.Timer(this.components);
            this.timerOperate = new System.Windows.Forms.Timer(this.components);
            this.timerAction = new System.Windows.Forms.Timer(this.components);
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageCurrent.SuspendLayout();
            this.tabPageSetting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupbBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxHours)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udAttackRange)).BeginInit();
            this.gbMission.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udMinStep)).BeginInit();
            this.gbBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udOperateTimeOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udReadTimes)).BeginInit();
            this.gbBuildUpdate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udUseDiamond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udStoreLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udFoodLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCrystalLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udShopLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udWoodLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udStoneLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udClearStoreTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udKeepHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udFoodPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udStonePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCrystalPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udWoodPrice)).BeginInit();
            this.tabPageProxy.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPageLog.SuspendLayout();
            this.tabPageUpdate.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.tabControl);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1038, 433);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1038, 480);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip);
            // 
            // statusStrip
            // 
            this.statusStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressBar,
            this.workStatus,
            this.timesStatus,
            this.timeOutStatus,
            this.goldStatus,
            this.resStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 0);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1038, 22);
            this.statusStrip.TabIndex = 0;
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(30, 16);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // workStatus
            // 
            this.workStatus.Name = "workStatus";
            this.workStatus.Size = new System.Drawing.Size(44, 17);
            this.workStatus.Text = "就绪。";
            // 
            // timesStatus
            // 
            this.timesStatus.Name = "timesStatus";
            this.timesStatus.Size = new System.Drawing.Size(32, 17);
            this.timesStatus.Text = "次数";
            // 
            // timeOutStatus
            // 
            this.timeOutStatus.Name = "timeOutStatus";
            this.timeOutStatus.Size = new System.Drawing.Size(32, 17);
            this.timeOutStatus.Text = "超时";
            // 
            // goldStatus
            // 
            this.goldStatus.Name = "goldStatus";
            this.goldStatus.Size = new System.Drawing.Size(44, 17);
            this.goldStatus.Text = "总金币";
            // 
            // resStatus
            // 
            this.resStatus.Name = "resStatus";
            this.resStatus.Size = new System.Drawing.Size(839, 17);
            this.resStatus.Spring = true;
            this.resStatus.Text = "资源情况";
            this.resStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPageCurrent);
            this.tabControl.Controls.Add(this.tabPageSetting);
            this.tabControl.Controls.Add(this.tabPageProxy);
            this.tabControl.Controls.Add(this.tabPageLog);
            this.tabControl.Controls.Add(this.tabPageUpdate);
            this.tabControl.Location = new System.Drawing.Point(12, 15);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1015, 415);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageCurrent
            // 
            this.tabPageCurrent.Controls.Add(this.webBrowser);
            this.tabPageCurrent.Location = new System.Drawing.Point(4, 22);
            this.tabPageCurrent.Name = "tabPageCurrent";
            this.tabPageCurrent.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCurrent.Size = new System.Drawing.Size(1007, 389);
            this.tabPageCurrent.TabIndex = 1;
            this.tabPageCurrent.Text = "当前";
            this.tabPageCurrent.UseVisualStyleBackColor = true;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(3, 3);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.ScriptErrorsSuppressed = true;
            this.webBrowser.Size = new System.Drawing.Size(1001, 383);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.BeforeNewWindow += new System.EventHandler(this.webBrowser_BeforeNewWindow);
            this.webBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser_DocumentCompleted);
            this.webBrowser.ProgressChanged += new System.Windows.Forms.WebBrowserProgressChangedEventHandler(this.webBrowser_ProgressChanged);
            // 
            // tabPageSetting
            // 
            this.tabPageSetting.Controls.Add(this.groupBox1);
            this.tabPageSetting.Controls.Add(this.groupbBox);
            this.tabPageSetting.Controls.Add(this.groupBox6);
            this.tabPageSetting.Controls.Add(this.gbMission);
            this.tabPageSetting.Controls.Add(this.gbBasic);
            this.tabPageSetting.Controls.Add(this.gbBuildUpdate);
            this.tabPageSetting.Controls.Add(this.dataGridView);
            this.tabPageSetting.Controls.Add(this.groupBox2);
            this.tabPageSetting.Location = new System.Drawing.Point(4, 22);
            this.tabPageSetting.Name = "tabPageSetting";
            this.tabPageSetting.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSetting.Size = new System.Drawing.Size(1007, 385);
            this.tabPageSetting.TabIndex = 0;
            this.tabPageSetting.Text = "设置";
            this.tabPageSetting.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btSearch);
            this.groupBox1.Controls.Add(this.ComboBoxSearch);
            this.groupBox1.Controls.Add(this.tbGoldMax);
            this.groupBox1.Controls.Add(this.cbGoldMax);
            this.groupBox1.Controls.Add(this.tbGoldMin);
            this.groupBox1.Controls.Add(this.cbGoldMin);
            this.groupBox1.Location = new System.Drawing.Point(15, 134);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 127);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "搜索";
            // 
            // btSearch
            // 
            this.btSearch.Location = new System.Drawing.Point(112, 92);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(58, 23);
            this.btSearch.TabIndex = 6;
            this.btSearch.Text = "搜索";
            this.btSearch.UseVisualStyleBackColor = true;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // ComboBoxSearch
            // 
            this.ComboBoxSearch.DisplayMember = "1";
            this.ComboBoxSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxSearch.FormattingEnabled = true;
            this.ComboBoxSearch.Items.AddRange(new object[] {
            "全部",
            "正常",
            "锁定",
            "封号"});
            this.ComboBoxSearch.Location = new System.Drawing.Point(26, 92);
            this.ComboBoxSearch.Name = "ComboBoxSearch";
            this.ComboBoxSearch.Size = new System.Drawing.Size(72, 20);
            this.ComboBoxSearch.TabIndex = 5;
            // 
            // tbGoldMax
            // 
            this.tbGoldMax.Location = new System.Drawing.Point(112, 56);
            this.tbGoldMax.Name = "tbGoldMax";
            this.tbGoldMax.Size = new System.Drawing.Size(58, 21);
            this.tbGoldMax.TabIndex = 4;
            this.tbGoldMax.Text = "1000000";
            this.tbGoldMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbGoldMax
            // 
            this.cbGoldMax.AutoSize = true;
            this.cbGoldMax.Location = new System.Drawing.Point(26, 58);
            this.cbGoldMax.Name = "cbGoldMax";
            this.cbGoldMax.Size = new System.Drawing.Size(84, 16);
            this.cbGoldMax.TabIndex = 3;
            this.cbGoldMax.Text = "金币数少于";
            this.cbGoldMax.UseVisualStyleBackColor = true;
            // 
            // tbGoldMin
            // 
            this.tbGoldMin.Location = new System.Drawing.Point(112, 22);
            this.tbGoldMin.Name = "tbGoldMin";
            this.tbGoldMin.Size = new System.Drawing.Size(58, 21);
            this.tbGoldMin.TabIndex = 2;
            this.tbGoldMin.Text = "50000";
            this.tbGoldMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbGoldMin
            // 
            this.cbGoldMin.AutoSize = true;
            this.cbGoldMin.Location = new System.Drawing.Point(26, 24);
            this.cbGoldMin.Name = "cbGoldMin";
            this.cbGoldMin.Size = new System.Drawing.Size(84, 16);
            this.cbGoldMin.TabIndex = 1;
            this.cbGoldMin.Text = "金币数大于";
            this.cbGoldMin.UseVisualStyleBackColor = true;
            // 
            // groupbBox
            // 
            this.groupbBox.Controls.Add(this.label14);
            this.groupbBox.Controls.Add(this.tbBuyResGold);
            this.groupbBox.Controls.Add(this.cbBuyRes);
            this.groupbBox.Controls.Add(this.label11);
            this.groupbBox.Controls.Add(this.tbStoreRest);
            this.groupbBox.Controls.Add(this.cbFullBuildStore);
            this.groupbBox.Controls.Add(this.label10);
            this.groupbBox.Controls.Add(this.udTaxHours);
            this.groupbBox.Controls.Add(this.cbAutoTax);
            this.groupbBox.Controls.Add(this.cbAutoHero);
            this.groupbBox.Controls.Add(this.cbAutoGov);
            this.groupbBox.Location = new System.Drawing.Point(219, 103);
            this.groupbBox.Name = "groupbBox";
            this.groupbBox.Size = new System.Drawing.Size(234, 161);
            this.groupbBox.TabIndex = 8;
            this.groupbBox.TabStop = false;
            this.groupbBox.Text = "内政服务";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(167, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 52;
            this.label14.Text = "金以上";
            // 
            // tbBuyResGold
            // 
            this.tbBuyResGold.Location = new System.Drawing.Point(120, 123);
            this.tbBuyResGold.Name = "tbBuyResGold";
            this.tbBuyResGold.Size = new System.Drawing.Size(41, 21);
            this.tbBuyResGold.TabIndex = 51;
            this.tbBuyResGold.Text = "2000";
            this.tbBuyResGold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbBuyRes
            // 
            this.cbBuyRes.AutoSize = true;
            this.cbBuyRes.Location = new System.Drawing.Point(27, 128);
            this.cbBuyRes.Name = "cbBuyRes";
            this.cbBuyRes.Size = new System.Drawing.Size(72, 16);
            this.cbBuyRes.TabIndex = 50;
            this.cbBuyRes.Text = "购入木石";
            this.cbBuyRes.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(167, 103);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 49;
            this.label11.Text = "余量";
            // 
            // tbStoreRest
            // 
            this.tbStoreRest.Location = new System.Drawing.Point(120, 98);
            this.tbStoreRest.Name = "tbStoreRest";
            this.tbStoreRest.Size = new System.Drawing.Size(41, 21);
            this.tbStoreRest.TabIndex = 48;
            this.tbStoreRest.Text = "5000";
            this.tbStoreRest.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbFullBuildStore
            // 
            this.cbFullBuildStore.AutoSize = true;
            this.cbFullBuildStore.Checked = true;
            this.cbFullBuildStore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFullBuildStore.Location = new System.Drawing.Point(27, 101);
            this.cbFullBuildStore.Name = "cbFullBuildStore";
            this.cbFullBuildStore.Size = new System.Drawing.Size(72, 16);
            this.cbFullBuildStore.TabIndex = 47;
            this.cbFullBuildStore.Text = "库满扩仓";
            this.cbFullBuildStore.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(122, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 46;
            this.label10.Text = "时以前";
            // 
            // udTaxHours
            // 
            this.udTaxHours.Location = new System.Drawing.Point(80, 70);
            this.udTaxHours.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.udTaxHours.Name = "udTaxHours";
            this.udTaxHours.Size = new System.Drawing.Size(35, 21);
            this.udTaxHours.TabIndex = 45;
            this.udTaxHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udTaxHours.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // cbAutoTax
            // 
            this.cbAutoTax.AutoSize = true;
            this.cbAutoTax.Checked = true;
            this.cbAutoTax.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoTax.Location = new System.Drawing.Point(27, 74);
            this.cbAutoTax.Name = "cbAutoTax";
            this.cbAutoTax.Size = new System.Drawing.Size(48, 16);
            this.cbAutoTax.TabIndex = 44;
            this.cbAutoTax.Text = "收税";
            this.cbAutoTax.UseVisualStyleBackColor = true;
            // 
            // cbAutoHero
            // 
            this.cbAutoHero.AutoSize = true;
            this.cbAutoHero.Checked = true;
            this.cbAutoHero.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoHero.Location = new System.Drawing.Point(27, 47);
            this.cbAutoHero.Name = "cbAutoHero";
            this.cbAutoHero.Size = new System.Drawing.Size(120, 16);
            this.cbAutoHero.TabIndex = 43;
            this.cbAutoHero.Text = "复活、升级、加点";
            this.cbAutoHero.UseVisualStyleBackColor = true;
            // 
            // cbAutoGov
            // 
            this.cbAutoGov.AutoSize = true;
            this.cbAutoGov.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbAutoGov.Location = new System.Drawing.Point(27, 20);
            this.cbAutoGov.Name = "cbAutoGov";
            this.cbAutoGov.Size = new System.Drawing.Size(72, 16);
            this.cbAutoGov.TabIndex = 41;
            this.cbAutoGov.Text = "自动内政";
            this.cbAutoGov.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbAutoHunt);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.udAttackRange);
            this.groupBox6.Controls.Add(this.cbWind);
            this.groupBox6.Controls.Add(this.cbWater);
            this.groupBox6.Controls.Add(this.cbWild);
            this.groupBox6.Controls.Add(this.cbHell);
            this.groupBox6.Location = new System.Drawing.Point(847, 15);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(145, 246);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "英雄出征";
            // 
            // cbAutoHunt
            // 
            this.cbAutoHunt.AutoSize = true;
            this.cbAutoHunt.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbAutoHunt.Location = new System.Drawing.Point(27, 30);
            this.cbAutoHunt.Name = "cbAutoHunt";
            this.cbAutoHunt.Size = new System.Drawing.Size(72, 16);
            this.cbAutoHunt.TabIndex = 28;
            this.cbAutoHunt.Text = "自动出征";
            this.cbAutoHunt.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 35;
            this.label5.Text = "攻击范围";
            // 
            // udAttackRange
            // 
            this.udAttackRange.Location = new System.Drawing.Point(84, 59);
            this.udAttackRange.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udAttackRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udAttackRange.Name = "udAttackRange";
            this.udAttackRange.Size = new System.Drawing.Size(42, 21);
            this.udAttackRange.TabIndex = 36;
            this.udAttackRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udAttackRange.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // cbWind
            // 
            this.cbWind.AutoSize = true;
            this.cbWind.Location = new System.Drawing.Point(27, 119);
            this.cbWind.Name = "cbWind";
            this.cbWind.Size = new System.Drawing.Size(48, 16);
            this.cbWind.TabIndex = 38;
            this.cbWind.Text = "风车";
            this.cbWind.UseVisualStyleBackColor = true;
            // 
            // cbWater
            // 
            this.cbWater.AutoSize = true;
            this.cbWater.Location = new System.Drawing.Point(27, 88);
            this.cbWater.Name = "cbWater";
            this.cbWater.Size = new System.Drawing.Size(48, 16);
            this.cbWater.TabIndex = 37;
            this.cbWater.Text = "水车";
            this.cbWater.UseVisualStyleBackColor = true;
            // 
            // cbWild
            // 
            this.cbWild.AutoSize = true;
            this.cbWild.Checked = true;
            this.cbWild.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWild.Location = new System.Drawing.Point(27, 188);
            this.cbWild.Name = "cbWild";
            this.cbWild.Size = new System.Drawing.Size(48, 16);
            this.cbWild.TabIndex = 39;
            this.cbWild.Text = "打野";
            this.cbWild.UseVisualStyleBackColor = true;
            // 
            // cbHell
            // 
            this.cbHell.AutoSize = true;
            this.cbHell.Checked = true;
            this.cbHell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHell.Location = new System.Drawing.Point(27, 155);
            this.cbHell.Name = "cbHell";
            this.cbHell.Size = new System.Drawing.Size(60, 16);
            this.cbHell.TabIndex = 40;
            this.cbHell.Text = "恶魔城";
            this.cbHell.UseVisualStyleBackColor = true;
            // 
            // gbMission
            // 
            this.gbMission.Controls.Add(this.cbAutoMission);
            this.gbMission.Controls.Add(this.label4);
            this.gbMission.Controls.Add(this.udMinStep);
            this.gbMission.Location = new System.Drawing.Point(219, 16);
            this.gbMission.Name = "gbMission";
            this.gbMission.Size = new System.Drawing.Size(234, 79);
            this.gbMission.TabIndex = 11;
            this.gbMission.TabStop = false;
            this.gbMission.Text = "前期任务";
            // 
            // cbAutoMission
            // 
            this.cbAutoMission.AutoSize = true;
            this.cbAutoMission.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbAutoMission.Location = new System.Drawing.Point(27, 26);
            this.cbAutoMission.Name = "cbAutoMission";
            this.cbAutoMission.Size = new System.Drawing.Size(72, 16);
            this.cbAutoMission.TabIndex = 23;
            this.cbAutoMission.Text = "自动任务";
            this.cbAutoMission.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(109, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 30;
            this.label4.Text = "结束步骤";
            // 
            // udMinStep
            // 
            this.udMinStep.Location = new System.Drawing.Point(170, 24);
            this.udMinStep.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udMinStep.Name = "udMinStep";
            this.udMinStep.Size = new System.Drawing.Size(42, 21);
            this.udMinStep.TabIndex = 29;
            this.udMinStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udMinStep.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // gbBasic
            // 
            this.gbBasic.Controls.Add(this.brRemoteDatabase);
            this.gbBasic.Controls.Add(this.rbLocalDatabase);
            this.gbBasic.Controls.Add(this.label2);
            this.gbBasic.Controls.Add(this.udOperateTimeOut);
            this.gbBasic.Controls.Add(this.label3);
            this.gbBasic.Controls.Add(this.udReadTimes);
            this.gbBasic.Location = new System.Drawing.Point(15, 15);
            this.gbBasic.Name = "gbBasic";
            this.gbBasic.Size = new System.Drawing.Size(183, 104);
            this.gbBasic.TabIndex = 10;
            this.gbBasic.TabStop = false;
            this.gbBasic.Text = "基本参数";
            // 
            // brRemoteDatabase
            // 
            this.brRemoteDatabase.AutoSize = true;
            this.brRemoteDatabase.Checked = true;
            this.brRemoteDatabase.Location = new System.Drawing.Point(101, 78);
            this.brRemoteDatabase.Name = "brRemoteDatabase";
            this.brRemoteDatabase.Size = new System.Drawing.Size(47, 16);
            this.brRemoteDatabase.TabIndex = 24;
            this.brRemoteDatabase.TabStop = true;
            this.brRemoteDatabase.Text = "远程";
            this.brRemoteDatabase.UseVisualStyleBackColor = true;
            this.brRemoteDatabase.Click += new System.EventHandler(this.brRemoteDatabase_Click);
            // 
            // rbLocalDatabase
            // 
            this.rbLocalDatabase.AutoSize = true;
            this.rbLocalDatabase.Location = new System.Drawing.Point(26, 77);
            this.rbLocalDatabase.Name = "rbLocalDatabase";
            this.rbLocalDatabase.Size = new System.Drawing.Size(47, 16);
            this.rbLocalDatabase.TabIndex = 23;
            this.rbLocalDatabase.Text = "本地";
            this.rbLocalDatabase.UseVisualStyleBackColor = true;
            this.rbLocalDatabase.Click += new System.EventHandler(this.rbLocalDatabase_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "操作超时（秒）";
            // 
            // udOperateTimeOut
            // 
            this.udOperateTimeOut.Location = new System.Drawing.Point(119, 20);
            this.udOperateTimeOut.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.udOperateTimeOut.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udOperateTimeOut.Name = "udOperateTimeOut";
            this.udOperateTimeOut.Size = new System.Drawing.Size(42, 21);
            this.udOperateTimeOut.TabIndex = 20;
            this.udOperateTimeOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udOperateTimeOut.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 21;
            this.label3.Text = "读取上限（次）";
            // 
            // udReadTimes
            // 
            this.udReadTimes.Location = new System.Drawing.Point(119, 47);
            this.udReadTimes.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.udReadTimes.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.udReadTimes.Name = "udReadTimes";
            this.udReadTimes.Size = new System.Drawing.Size(42, 21);
            this.udReadTimes.TabIndex = 22;
            this.udReadTimes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udReadTimes.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // gbBuildUpdate
            // 
            this.gbBuildUpdate.Controls.Add(this.udUseDiamond);
            this.gbBuildUpdate.Controls.Add(this.cbUseDiamond);
            this.gbBuildUpdate.Controls.Add(this.udStoreLevel);
            this.gbBuildUpdate.Controls.Add(this.udFoodLevel);
            this.gbBuildUpdate.Controls.Add(this.cbStoreUpdate);
            this.gbBuildUpdate.Controls.Add(this.udCrystalLevel);
            this.gbBuildUpdate.Controls.Add(this.cbFoodUpdate);
            this.gbBuildUpdate.Controls.Add(this.cbCrystalUpdate);
            this.gbBuildUpdate.Controls.Add(this.udShopLevel);
            this.gbBuildUpdate.Controls.Add(this.cbShopUpdate);
            this.gbBuildUpdate.Controls.Add(this.udWoodLevel);
            this.gbBuildUpdate.Controls.Add(this.cbWoodUpdate);
            this.gbBuildUpdate.Controls.Add(this.udStoneLevel);
            this.gbBuildUpdate.Controls.Add(this.cbStoneUpdate);
            this.gbBuildUpdate.Controls.Add(this.cbAutoBuild);
            this.gbBuildUpdate.Location = new System.Drawing.Point(477, 15);
            this.gbBuildUpdate.Name = "gbBuildUpdate";
            this.gbBuildUpdate.Size = new System.Drawing.Size(156, 249);
            this.gbBuildUpdate.TabIndex = 9;
            this.gbBuildUpdate.TabStop = false;
            this.gbBuildUpdate.Text = "建筑升级";
            // 
            // udUseDiamond
            // 
            this.udUseDiamond.Location = new System.Drawing.Point(89, 40);
            this.udUseDiamond.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udUseDiamond.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udUseDiamond.Name = "udUseDiamond";
            this.udUseDiamond.Size = new System.Drawing.Size(35, 21);
            this.udUseDiamond.TabIndex = 55;
            this.udUseDiamond.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udUseDiamond.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // cbUseDiamond
            // 
            this.cbUseDiamond.AutoSize = true;
            this.cbUseDiamond.Location = new System.Drawing.Point(23, 45);
            this.cbUseDiamond.Name = "cbUseDiamond";
            this.cbUseDiamond.Size = new System.Drawing.Size(60, 16);
            this.cbUseDiamond.TabIndex = 54;
            this.cbUseDiamond.Text = "用礼券";
            this.cbUseDiamond.UseVisualStyleBackColor = true;
            // 
            // udStoreLevel
            // 
            this.udStoreLevel.Location = new System.Drawing.Point(89, 131);
            this.udStoreLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udStoreLevel.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udStoreLevel.Name = "udStoreLevel";
            this.udStoreLevel.Size = new System.Drawing.Size(35, 21);
            this.udStoreLevel.TabIndex = 22;
            this.udStoreLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udStoreLevel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // udFoodLevel
            // 
            this.udFoodLevel.Location = new System.Drawing.Point(89, 160);
            this.udFoodLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udFoodLevel.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udFoodLevel.Name = "udFoodLevel";
            this.udFoodLevel.Size = new System.Drawing.Size(35, 21);
            this.udFoodLevel.TabIndex = 20;
            this.udFoodLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udFoodLevel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cbStoreUpdate
            // 
            this.cbStoreUpdate.AutoSize = true;
            this.cbStoreUpdate.Checked = true;
            this.cbStoreUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStoreUpdate.Location = new System.Drawing.Point(23, 132);
            this.cbStoreUpdate.Name = "cbStoreUpdate";
            this.cbStoreUpdate.Size = new System.Drawing.Size(48, 16);
            this.cbStoreUpdate.TabIndex = 21;
            this.cbStoreUpdate.Text = "仓库";
            this.cbStoreUpdate.UseVisualStyleBackColor = true;
            // 
            // udCrystalLevel
            // 
            this.udCrystalLevel.Location = new System.Drawing.Point(89, 189);
            this.udCrystalLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udCrystalLevel.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udCrystalLevel.Name = "udCrystalLevel";
            this.udCrystalLevel.Size = new System.Drawing.Size(35, 21);
            this.udCrystalLevel.TabIndex = 18;
            this.udCrystalLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udCrystalLevel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cbFoodUpdate
            // 
            this.cbFoodUpdate.AutoSize = true;
            this.cbFoodUpdate.Checked = true;
            this.cbFoodUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFoodUpdate.Location = new System.Drawing.Point(23, 161);
            this.cbFoodUpdate.Name = "cbFoodUpdate";
            this.cbFoodUpdate.Size = new System.Drawing.Size(48, 16);
            this.cbFoodUpdate.TabIndex = 19;
            this.cbFoodUpdate.Text = "农场";
            this.cbFoodUpdate.UseVisualStyleBackColor = true;
            // 
            // cbCrystalUpdate
            // 
            this.cbCrystalUpdate.AutoSize = true;
            this.cbCrystalUpdate.Checked = true;
            this.cbCrystalUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCrystalUpdate.Location = new System.Drawing.Point(23, 190);
            this.cbCrystalUpdate.Name = "cbCrystalUpdate";
            this.cbCrystalUpdate.Size = new System.Drawing.Size(60, 16);
            this.cbCrystalUpdate.TabIndex = 17;
            this.cbCrystalUpdate.Text = "水晶矿";
            this.cbCrystalUpdate.UseVisualStyleBackColor = true;
            // 
            // udShopLevel
            // 
            this.udShopLevel.Location = new System.Drawing.Point(89, 218);
            this.udShopLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udShopLevel.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udShopLevel.Name = "udShopLevel";
            this.udShopLevel.Size = new System.Drawing.Size(35, 21);
            this.udShopLevel.TabIndex = 16;
            this.udShopLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udShopLevel.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // cbShopUpdate
            // 
            this.cbShopUpdate.AutoSize = true;
            this.cbShopUpdate.Checked = true;
            this.cbShopUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbShopUpdate.Location = new System.Drawing.Point(23, 219);
            this.cbShopUpdate.Name = "cbShopUpdate";
            this.cbShopUpdate.Size = new System.Drawing.Size(48, 16);
            this.cbShopUpdate.TabIndex = 15;
            this.cbShopUpdate.Text = "市场";
            this.cbShopUpdate.UseVisualStyleBackColor = true;
            // 
            // udWoodLevel
            // 
            this.udWoodLevel.Location = new System.Drawing.Point(89, 102);
            this.udWoodLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udWoodLevel.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udWoodLevel.Name = "udWoodLevel";
            this.udWoodLevel.Size = new System.Drawing.Size(35, 21);
            this.udWoodLevel.TabIndex = 14;
            this.udWoodLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udWoodLevel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cbWoodUpdate
            // 
            this.cbWoodUpdate.AutoSize = true;
            this.cbWoodUpdate.Checked = true;
            this.cbWoodUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWoodUpdate.Location = new System.Drawing.Point(23, 103);
            this.cbWoodUpdate.Name = "cbWoodUpdate";
            this.cbWoodUpdate.Size = new System.Drawing.Size(60, 16);
            this.cbWoodUpdate.TabIndex = 13;
            this.cbWoodUpdate.Text = "伐木场";
            this.cbWoodUpdate.UseVisualStyleBackColor = true;
            // 
            // udStoneLevel
            // 
            this.udStoneLevel.Location = new System.Drawing.Point(89, 73);
            this.udStoneLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.udStoneLevel.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.udStoneLevel.Name = "udStoneLevel";
            this.udStoneLevel.Size = new System.Drawing.Size(35, 21);
            this.udStoneLevel.TabIndex = 12;
            this.udStoneLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udStoneLevel.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // cbStoneUpdate
            // 
            this.cbStoneUpdate.AutoSize = true;
            this.cbStoneUpdate.Checked = true;
            this.cbStoneUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStoneUpdate.Location = new System.Drawing.Point(23, 74);
            this.cbStoneUpdate.Name = "cbStoneUpdate";
            this.cbStoneUpdate.Size = new System.Drawing.Size(60, 16);
            this.cbStoneUpdate.TabIndex = 11;
            this.cbStoneUpdate.Text = "石头矿";
            this.cbStoneUpdate.UseVisualStyleBackColor = true;
            // 
            // cbAutoBuild
            // 
            this.cbAutoBuild.AutoSize = true;
            this.cbAutoBuild.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbAutoBuild.Location = new System.Drawing.Point(23, 20);
            this.cbAutoBuild.Name = "cbAutoBuild";
            this.cbAutoBuild.Size = new System.Drawing.Size(72, 16);
            this.cbAutoBuild.TabIndex = 0;
            this.cbAutoBuild.Text = "自动升级";
            this.cbAutoBuild.UseVisualStyleBackColor = true;
            // 
            // dataGridView
            // 
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.序号,
            this.账户,
            this.坐标,
            this.等级,
            this.挂单,
            this.任务,
            this.金币,
            this.钻石,
            this.仓库,
            this.木材,
            this.石头,
            this.水晶,
            this.食物,
            this.时间});
            this.dataGridView.Location = new System.Drawing.Point(15, 278);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.RowTemplate.Height = 23;
            this.dataGridView.Size = new System.Drawing.Size(973, 95);
            this.dataGridView.TabIndex = 7;
            this.dataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_CellMouseDoubleClick);
            // 
            // 序号
            // 
            this.序号.HeaderText = "序号";
            this.序号.Name = "序号";
            this.序号.ReadOnly = true;
            this.序号.Width = 55;
            // 
            // 账户
            // 
            this.账户.HeaderText = "账户";
            this.账户.Name = "账户";
            this.账户.ReadOnly = true;
            this.账户.Width = 120;
            // 
            // 坐标
            // 
            this.坐标.HeaderText = "坐标";
            this.坐标.Name = "坐标";
            this.坐标.ReadOnly = true;
            this.坐标.Width = 70;
            // 
            // 等级
            // 
            this.等级.HeaderText = "等级";
            this.等级.Name = "等级";
            this.等级.ReadOnly = true;
            this.等级.Width = 55;
            // 
            // 挂单
            // 
            this.挂单.HeaderText = "挂单";
            this.挂单.Name = "挂单";
            this.挂单.ReadOnly = true;
            this.挂单.Width = 60;
            // 
            // 任务
            // 
            this.任务.HeaderText = "任务";
            this.任务.Name = "任务";
            this.任务.ReadOnly = true;
            this.任务.Width = 60;
            // 
            // 金币
            // 
            this.金币.HeaderText = "金币";
            this.金币.Name = "金币";
            this.金币.ReadOnly = true;
            // 
            // 钻石
            // 
            this.钻石.HeaderText = "钻石";
            this.钻石.Name = "钻石";
            this.钻石.ReadOnly = true;
            this.钻石.Width = 60;
            // 
            // 仓库
            // 
            this.仓库.HeaderText = "仓库";
            this.仓库.Name = "仓库";
            this.仓库.ReadOnly = true;
            this.仓库.Width = 120;
            // 
            // 木材
            // 
            this.木材.HeaderText = "木材";
            this.木材.Name = "木材";
            this.木材.ReadOnly = true;
            // 
            // 石头
            // 
            this.石头.HeaderText = "石头";
            this.石头.Name = "石头";
            this.石头.ReadOnly = true;
            // 
            // 水晶
            // 
            this.水晶.HeaderText = "水晶";
            this.水晶.Name = "水晶";
            this.水晶.ReadOnly = true;
            // 
            // 食物
            // 
            this.食物.HeaderText = "食物";
            this.食物.Name = "食物";
            this.食物.ReadOnly = true;
            // 
            // 时间
            // 
            this.时间.HeaderText = "时间";
            this.时间.Name = "时间";
            this.时间.ReadOnly = true;
            this.时间.Width = 150;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.udClearStoreTime);
            this.groupBox2.Controls.Add(this.btAddPrice);
            this.groupBox2.Controls.Add(this.btSubPrice);
            this.groupBox2.Controls.Add(this.udKeepHours);
            this.groupBox2.Controls.Add(this.udFoodPrice);
            this.groupBox2.Controls.Add(this.udStonePrice);
            this.groupBox2.Controls.Add(this.udCrystalPrice);
            this.groupBox2.Controls.Add(this.udWoodPrice);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cbAutoSell);
            this.groupBox2.Controls.Add(this.cbStopNight);
            this.groupBox2.Controls.Add(this.cbClearStore);
            this.groupBox2.Controls.Add(this.cbFoodSell);
            this.groupBox2.Controls.Add(this.cbCrystalSell);
            this.groupBox2.Controls.Add(this.cbStoneSell);
            this.groupBox2.Controls.Add(this.cbWoodSell);
            this.groupBox2.Location = new System.Drawing.Point(657, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(165, 249);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "挂单设置";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(82, 102);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 25;
            this.label12.Text = "时后清仓";
            // 
            // udClearStoreTime
            // 
            this.udClearStoreTime.Location = new System.Drawing.Point(43, 98);
            this.udClearStoreTime.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.udClearStoreTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udClearStoreTime.Name = "udClearStoreTime";
            this.udClearStoreTime.Size = new System.Drawing.Size(35, 21);
            this.udClearStoreTime.TabIndex = 24;
            this.udClearStoreTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udClearStoreTime.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // btAddPrice
            // 
            this.btAddPrice.Location = new System.Drawing.Point(125, 144);
            this.btAddPrice.Name = "btAddPrice";
            this.btAddPrice.Size = new System.Drawing.Size(24, 23);
            this.btAddPrice.TabIndex = 23;
            this.btAddPrice.Text = "加";
            this.btAddPrice.UseVisualStyleBackColor = true;
            this.btAddPrice.Click += new System.EventHandler(this.btAddPrice_Click);
            // 
            // btSubPrice
            // 
            this.btSubPrice.Location = new System.Drawing.Point(125, 193);
            this.btSubPrice.Name = "btSubPrice";
            this.btSubPrice.Size = new System.Drawing.Size(24, 23);
            this.btSubPrice.TabIndex = 22;
            this.btSubPrice.Text = "减";
            this.btSubPrice.UseVisualStyleBackColor = true;
            this.btSubPrice.Click += new System.EventHandler(this.btSubPrice_Click);
            // 
            // udKeepHours
            // 
            this.udKeepHours.Location = new System.Drawing.Point(76, 68);
            this.udKeepHours.Maximum = new decimal(new int[] {
            48,
            0,
            0,
            0});
            this.udKeepHours.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udKeepHours.Name = "udKeepHours";
            this.udKeepHours.Size = new System.Drawing.Size(35, 21);
            this.udKeepHours.TabIndex = 19;
            this.udKeepHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udKeepHours.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // udFoodPrice
            // 
            this.udFoodPrice.Location = new System.Drawing.Point(76, 189);
            this.udFoodPrice.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.udFoodPrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udFoodPrice.Name = "udFoodPrice";
            this.udFoodPrice.Size = new System.Drawing.Size(35, 21);
            this.udFoodPrice.TabIndex = 18;
            this.udFoodPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udFoodPrice.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // udStonePrice
            // 
            this.udStonePrice.Location = new System.Drawing.Point(76, 159);
            this.udStonePrice.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.udStonePrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udStonePrice.Name = "udStonePrice";
            this.udStonePrice.Size = new System.Drawing.Size(35, 21);
            this.udStonePrice.TabIndex = 17;
            this.udStonePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udStonePrice.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // udCrystalPrice
            // 
            this.udCrystalPrice.Location = new System.Drawing.Point(76, 216);
            this.udCrystalPrice.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.udCrystalPrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udCrystalPrice.Name = "udCrystalPrice";
            this.udCrystalPrice.Size = new System.Drawing.Size(35, 21);
            this.udCrystalPrice.TabIndex = 16;
            this.udCrystalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udCrystalPrice.Value = new decimal(new int[] {
            14,
            0,
            0,
            0});
            // 
            // udWoodPrice
            // 
            this.udWoodPrice.Location = new System.Drawing.Point(76, 130);
            this.udWoodPrice.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.udWoodPrice.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udWoodPrice.Name = "udWoodPrice";
            this.udWoodPrice.Size = new System.Drawing.Size(35, 21);
            this.udWoodPrice.TabIndex = 15;
            this.udWoodPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.udWoodPrice.Value = new decimal(new int[] {
            17,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "小时数";
            // 
            // cbAutoSell
            // 
            this.cbAutoSell.AutoSize = true;
            this.cbAutoSell.Checked = true;
            this.cbAutoSell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoSell.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbAutoSell.Location = new System.Drawing.Point(25, 20);
            this.cbAutoSell.Name = "cbAutoSell";
            this.cbAutoSell.Size = new System.Drawing.Size(72, 16);
            this.cbAutoSell.TabIndex = 1;
            this.cbAutoSell.Text = "自动挂单";
            this.cbAutoSell.UseVisualStyleBackColor = true;
            // 
            // cbStopNight
            // 
            this.cbStopNight.AutoSize = true;
            this.cbStopNight.Checked = true;
            this.cbStopNight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStopNight.Location = new System.Drawing.Point(25, 45);
            this.cbStopNight.Name = "cbStopNight";
            this.cbStopNight.Size = new System.Drawing.Size(96, 16);
            this.cbStopNight.TabIndex = 4;
            this.cbStopNight.Text = "7时-23时挂单";
            this.cbStopNight.UseVisualStyleBackColor = true;
            // 
            // cbClearStore
            // 
            this.cbClearStore.AutoSize = true;
            this.cbClearStore.Checked = true;
            this.cbClearStore.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbClearStore.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cbClearStore.Location = new System.Drawing.Point(24, 102);
            this.cbClearStore.Name = "cbClearStore";
            this.cbClearStore.Size = new System.Drawing.Size(15, 14);
            this.cbClearStore.TabIndex = 2;
            this.cbClearStore.UseVisualStyleBackColor = true;
            // 
            // cbFoodSell
            // 
            this.cbFoodSell.AutoSize = true;
            this.cbFoodSell.Checked = true;
            this.cbFoodSell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbFoodSell.Location = new System.Drawing.Point(25, 191);
            this.cbFoodSell.Name = "cbFoodSell";
            this.cbFoodSell.Size = new System.Drawing.Size(48, 16);
            this.cbFoodSell.TabIndex = 12;
            this.cbFoodSell.Text = "粮食";
            this.cbFoodSell.UseVisualStyleBackColor = true;
            // 
            // cbCrystalSell
            // 
            this.cbCrystalSell.AutoSize = true;
            this.cbCrystalSell.Checked = true;
            this.cbCrystalSell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbCrystalSell.Location = new System.Drawing.Point(25, 219);
            this.cbCrystalSell.Name = "cbCrystalSell";
            this.cbCrystalSell.Size = new System.Drawing.Size(48, 16);
            this.cbCrystalSell.TabIndex = 10;
            this.cbCrystalSell.Text = "水晶";
            this.cbCrystalSell.UseVisualStyleBackColor = true;
            // 
            // cbStoneSell
            // 
            this.cbStoneSell.AutoSize = true;
            this.cbStoneSell.Checked = true;
            this.cbStoneSell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbStoneSell.Location = new System.Drawing.Point(25, 161);
            this.cbStoneSell.Name = "cbStoneSell";
            this.cbStoneSell.Size = new System.Drawing.Size(48, 16);
            this.cbStoneSell.TabIndex = 8;
            this.cbStoneSell.Text = "石头";
            this.cbStoneSell.UseVisualStyleBackColor = true;
            // 
            // cbWoodSell
            // 
            this.cbWoodSell.AutoSize = true;
            this.cbWoodSell.Checked = true;
            this.cbWoodSell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWoodSell.Location = new System.Drawing.Point(25, 132);
            this.cbWoodSell.Name = "cbWoodSell";
            this.cbWoodSell.Size = new System.Drawing.Size(48, 16);
            this.cbWoodSell.TabIndex = 6;
            this.cbWoodSell.Text = "木头";
            this.cbWoodSell.UseVisualStyleBackColor = true;
            // 
            // tabPageProxy
            // 
            this.tabPageProxy.Controls.Add(this.tbProxyList);
            this.tabPageProxy.Controls.Add(this.groupBox3);
            this.tabPageProxy.Location = new System.Drawing.Point(4, 22);
            this.tabPageProxy.Name = "tabPageProxy";
            this.tabPageProxy.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageProxy.Size = new System.Drawing.Size(1007, 385);
            this.tabPageProxy.TabIndex = 3;
            this.tabPageProxy.Text = "代理";
            this.tabPageProxy.UseVisualStyleBackColor = true;
            // 
            // tbProxyList
            // 
            this.tbProxyList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbProxyList.Location = new System.Drawing.Point(21, 173);
            this.tbProxyList.Multiline = true;
            this.tbProxyList.Name = "tbProxyList";
            this.tbProxyList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbProxyList.Size = new System.Drawing.Size(962, 197);
            this.tbProxyList.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btProxyCopy);
            this.groupBox3.Controls.Add(this.tbProxyProtocolSplit);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.tbProxyPortSplit);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.btProxyWeb);
            this.groupBox3.Controls.Add(this.tbProxyReg);
            this.groupBox3.Controls.Add(this.btGetProxy);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.tbProxyURL);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(21, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(962, 135);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "代理收集";
            // 
            // btProxyCopy
            // 
            this.btProxyCopy.Location = new System.Drawing.Point(383, 93);
            this.btProxyCopy.Name = "btProxyCopy";
            this.btProxyCopy.Size = new System.Drawing.Size(75, 23);
            this.btProxyCopy.TabIndex = 9;
            this.btProxyCopy.Text = "复制";
            this.btProxyCopy.UseVisualStyleBackColor = true;
            this.btProxyCopy.Click += new System.EventHandler(this.btProxyCopy_Click);
            // 
            // tbProxyProtocolSplit
            // 
            this.tbProxyProtocolSplit.Location = new System.Drawing.Point(220, 93);
            this.tbProxyProtocolSplit.Name = "tbProxyProtocolSplit";
            this.tbProxyProtocolSplit.Size = new System.Drawing.Size(29, 21);
            this.tbProxyProtocolSplit.TabIndex = 7;
            this.tbProxyProtocolSplit.Text = "@";
            this.tbProxyProtocolSplit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(149, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "协议分隔：";
            // 
            // tbProxyPortSplit
            // 
            this.tbProxyPortSplit.Location = new System.Drawing.Point(97, 93);
            this.tbProxyPortSplit.Name = "tbProxyPortSplit";
            this.tbProxyPortSplit.Size = new System.Drawing.Size(29, 21);
            this.tbProxyPortSplit.TabIndex = 2;
            this.tbProxyPortSplit.Text = ":";
            this.tbProxyPortSplit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "端口分隔：";
            // 
            // btProxyWeb
            // 
            this.btProxyWeb.Location = new System.Drawing.Point(392, 18);
            this.btProxyWeb.Name = "btProxyWeb";
            this.btProxyWeb.Size = new System.Drawing.Size(75, 23);
            this.btProxyWeb.TabIndex = 5;
            this.btProxyWeb.Text = "打开网址";
            this.btProxyWeb.UseVisualStyleBackColor = true;
            this.btProxyWeb.Click += new System.EventHandler(this.btProxyWeb_Click);
            // 
            // tbProxyReg
            // 
            this.tbProxyReg.Location = new System.Drawing.Point(73, 57);
            this.tbProxyReg.Name = "tbProxyReg";
            this.tbProxyReg.Size = new System.Drawing.Size(394, 21);
            this.tbProxyReg.TabIndex = 4;
            this.tbProxyReg.Text = "([0-9]*.[0-9]*.[0-9]*.[0-9]*)\\s([0-9]*)\\s(HTTP)";
            // 
            // btGetProxy
            // 
            this.btGetProxy.Location = new System.Drawing.Point(281, 93);
            this.btGetProxy.Name = "btGetProxy";
            this.btGetProxy.Size = new System.Drawing.Size(75, 23);
            this.btGetProxy.TabIndex = 3;
            this.btGetProxy.Text = "获取代理";
            this.btGetProxy.UseVisualStyleBackColor = true;
            this.btGetProxy.Click += new System.EventHandler(this.btGetProxy_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 2;
            this.label7.Text = "正则：";
            // 
            // tbProxyURL
            // 
            this.tbProxyURL.Location = new System.Drawing.Point(73, 20);
            this.tbProxyURL.Name = "tbProxyURL";
            this.tbProxyURL.Size = new System.Drawing.Size(298, 21);
            this.tbProxyURL.TabIndex = 1;
            this.tbProxyURL.Text = "http://www.sooip.cn/";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "网址：";
            // 
            // tabPageLog
            // 
            this.tabPageLog.Controls.Add(this.btClearWorkLogs);
            this.tabPageLog.Controls.Add(this.tbWorkLogs);
            this.tabPageLog.Location = new System.Drawing.Point(4, 22);
            this.tabPageLog.Name = "tabPageLog";
            this.tabPageLog.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLog.Size = new System.Drawing.Size(1007, 385);
            this.tabPageLog.TabIndex = 2;
            this.tabPageLog.Text = "日志";
            this.tabPageLog.UseVisualStyleBackColor = true;
            // 
            // btClearWorkLogs
            // 
            this.btClearWorkLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btClearWorkLogs.Location = new System.Drawing.Point(22, 358);
            this.btClearWorkLogs.Name = "btClearWorkLogs";
            this.btClearWorkLogs.Size = new System.Drawing.Size(75, 23);
            this.btClearWorkLogs.TabIndex = 1;
            this.btClearWorkLogs.Text = "清空日志";
            this.btClearWorkLogs.UseVisualStyleBackColor = true;
            this.btClearWorkLogs.Click += new System.EventHandler(this.btClearWorkLogs_Click);
            // 
            // tbWorkLogs
            // 
            this.tbWorkLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWorkLogs.Location = new System.Drawing.Point(22, 19);
            this.tbWorkLogs.Multiline = true;
            this.tbWorkLogs.Name = "tbWorkLogs";
            this.tbWorkLogs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbWorkLogs.Size = new System.Drawing.Size(965, 333);
            this.tbWorkLogs.TabIndex = 0;
            this.tbWorkLogs.TextChanged += new System.EventHandler(this.tbWorkLogs_TextChanged);
            // 
            // tabPageUpdate
            // 
            this.tabPageUpdate.Controls.Add(this.tbUpdateLog);
            this.tabPageUpdate.Location = new System.Drawing.Point(4, 22);
            this.tabPageUpdate.Name = "tabPageUpdate";
            this.tabPageUpdate.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUpdate.Size = new System.Drawing.Size(1007, 385);
            this.tabPageUpdate.TabIndex = 4;
            this.tabPageUpdate.Text = "更新";
            this.tabPageUpdate.UseVisualStyleBackColor = true;
            // 
            // tbUpdateLog
            // 
            this.tbUpdateLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUpdateLog.Location = new System.Drawing.Point(27, 23);
            this.tbUpdateLog.Multiline = true;
            this.tbUpdateLog.Name = "tbUpdateLog";
            this.tbUpdateLog.ReadOnly = true;
            this.tbUpdateLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbUpdateLog.Size = new System.Drawing.Size(952, 347);
            this.tbUpdateLog.TabIndex = 0;
            this.tbUpdateLog.Text = resources.GetString("tbUpdateLog.Text");
            // 
            // toolStrip
            // 
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ComboBoxServer,
            this.ComboBoxSection,
            this.ComboBoxAccount,
            this.toolStripSeparator4,
            this.btEnterUser,
            this.btLastUser,
            this.btNextUser,
            this.toolStripSeparator3,
            this.ComboBoxAutoStyle,
            this.btStartAuto,
            this.toolStripSeparator2,
            this.btRegister,
            this.tbRegCount,
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.btGetUserInfo,
            this.btUnLockUser,
            this.btSaveSetting,
            this.toolStripSeparator5,
            this.btDeleteAccount,
            this.btClearStore,
            this.toolStripSeparator6,
            this.btDownLoad});
            this.toolStrip.Location = new System.Drawing.Point(3, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1016, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // ComboBoxServer
            // 
            this.ComboBoxServer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxServer.Name = "ComboBoxServer";
            this.ComboBoxServer.Size = new System.Drawing.Size(100, 25);
            this.ComboBoxServer.SelectedIndexChanged += new System.EventHandler(this.ComboBoxServer_SelectedIndexChanged);
            // 
            // ComboBoxSection
            // 
            this.ComboBoxSection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxSection.Name = "ComboBoxSection";
            this.ComboBoxSection.Size = new System.Drawing.Size(75, 25);
            this.ComboBoxSection.SelectedIndexChanged += new System.EventHandler(this.ComboBoxSection_SelectedIndexChanged);
            // 
            // ComboBoxAccount
            // 
            this.ComboBoxAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAccount.Name = "ComboBoxAccount";
            this.ComboBoxAccount.Size = new System.Drawing.Size(200, 25);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // btEnterUser
            // 
            this.btEnterUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btEnterUser.Image = ((System.Drawing.Image)(resources.GetObject("btEnterUser.Image")));
            this.btEnterUser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEnterUser.Name = "btEnterUser";
            this.btEnterUser.Size = new System.Drawing.Size(36, 22);
            this.btEnterUser.Text = "进入";
            this.btEnterUser.Click += new System.EventHandler(this.btEnterUser_Click);
            // 
            // btLastUser
            // 
            this.btLastUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btLastUser.Image = ((System.Drawing.Image)(resources.GetObject("btLastUser.Image")));
            this.btLastUser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btLastUser.Name = "btLastUser";
            this.btLastUser.Size = new System.Drawing.Size(36, 22);
            this.btLastUser.Text = "上个";
            this.btLastUser.Click += new System.EventHandler(this.btLastUser_Click);
            // 
            // btNextUser
            // 
            this.btNextUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btNextUser.Image = ((System.Drawing.Image)(resources.GetObject("btNextUser.Image")));
            this.btNextUser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNextUser.Name = "btNextUser";
            this.btNextUser.Size = new System.Drawing.Size(57, 22);
            this.btNextUser.Text = "下个(F1)";
            this.btNextUser.Click += new System.EventHandler(this.btNextUser_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // ComboBoxAutoStyle
            // 
            this.ComboBoxAutoStyle.AutoSize = false;
            this.ComboBoxAutoStyle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAutoStyle.Items.AddRange(new object[] {
            "常规",
            "挂单",
            "建造",
            "发展",
            "任务"});
            this.ComboBoxAutoStyle.Name = "ComboBoxAutoStyle";
            this.ComboBoxAutoStyle.Size = new System.Drawing.Size(50, 25);
            this.ComboBoxAutoStyle.SelectedIndexChanged += new System.EventHandler(this.ComboBoxAutoStyle_SelectedIndexChanged);
            // 
            // btStartAuto
            // 
            this.btStartAuto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btStartAuto.Image = ((System.Drawing.Image)(resources.GetObject("btStartAuto.Image")));
            this.btStartAuto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btStartAuto.Name = "btStartAuto";
            this.btStartAuto.Size = new System.Drawing.Size(36, 22);
            this.btStartAuto.Text = "开始";
            this.btStartAuto.Click += new System.EventHandler(this.btStartAuto_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btRegister
            // 
            this.btRegister.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btRegister.Image = ((System.Drawing.Image)(resources.GetObject("btRegister.Image")));
            this.btRegister.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRegister.Name = "btRegister";
            this.btRegister.Size = new System.Drawing.Size(57, 22);
            this.btRegister.Text = "注册(F3)";
            this.btRegister.Click += new System.EventHandler(this.btRegister_Click);
            // 
            // tbRegCount
            // 
            this.tbRegCount.Name = "tbRegCount";
            this.tbRegCount.Size = new System.Drawing.Size(30, 25);
            this.tbRegCount.Text = "20";
            this.tbRegCount.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(20, 22);
            this.toolStripLabel1.Text = "个";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btGetUserInfo
            // 
            this.btGetUserInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btGetUserInfo.Image = ((System.Drawing.Image)(resources.GetObject("btGetUserInfo.Image")));
            this.btGetUserInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGetUserInfo.Name = "btGetUserInfo";
            this.btGetUserInfo.Size = new System.Drawing.Size(36, 22);
            this.btGetUserInfo.Text = "读取";
            this.btGetUserInfo.Click += new System.EventHandler(this.btGetUserInfo_Click);
            // 
            // btUnLockUser
            // 
            this.btUnLockUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btUnLockUser.Image = ((System.Drawing.Image)(resources.GetObject("btUnLockUser.Image")));
            this.btUnLockUser.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btUnLockUser.Name = "btUnLockUser";
            this.btUnLockUser.Size = new System.Drawing.Size(57, 22);
            this.btUnLockUser.Text = "已开(F2)";
            this.btUnLockUser.Click += new System.EventHandler(this.btUnLockUser_Click);
            // 
            // btSaveSetting
            // 
            this.btSaveSetting.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btSaveSetting.Image = ((System.Drawing.Image)(resources.GetObject("btSaveSetting.Image")));
            this.btSaveSetting.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSaveSetting.Name = "btSaveSetting";
            this.btSaveSetting.Size = new System.Drawing.Size(36, 22);
            this.btSaveSetting.Text = "保存";
            this.btSaveSetting.Click += new System.EventHandler(this.btSaveSetting_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // btDeleteAccount
            // 
            this.btDeleteAccount.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btDeleteAccount.Image = ((System.Drawing.Image)(resources.GetObject("btDeleteAccount.Image")));
            this.btDeleteAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDeleteAccount.Name = "btDeleteAccount";
            this.btDeleteAccount.Size = new System.Drawing.Size(36, 22);
            this.btDeleteAccount.Text = "被封";
            this.btDeleteAccount.Click += new System.EventHandler(this.btDeleteAccount_Click);
            // 
            // btClearStore
            // 
            this.btClearStore.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btClearStore.Image = ((System.Drawing.Image)(resources.GetObject("btClearStore.Image")));
            this.btClearStore.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btClearStore.Name = "btClearStore";
            this.btClearStore.Size = new System.Drawing.Size(36, 22);
            this.btClearStore.Text = "清仓";
            this.btClearStore.Click += new System.EventHandler(this.btClearStore_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // btDownLoad
            // 
            this.btDownLoad.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btDownLoad.Image = ((System.Drawing.Image)(resources.GetObject("btDownLoad.Image")));
            this.btDownLoad.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDownLoad.Name = "btDownLoad";
            this.btDownLoad.Size = new System.Drawing.Size(60, 22);
            this.btDownLoad.Text = "更新软件";
            this.btDownLoad.Click += new System.EventHandler(this.btDownLoad_Click);
            // 
            // timerWait
            // 
            this.timerWait.Interval = 1000;
            this.timerWait.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // timerOut
            // 
            this.timerOut.Interval = 1000;
            this.timerOut.Tick += new System.EventHandler(this.timerOut_Tick);
            // 
            // timerOperate
            // 
            this.timerOperate.Tick += new System.EventHandler(this.timerOperate_Tick);
            // 
            // timerAction
            // 
            this.timerAction.Tick += new System.EventHandler(this.timerAction_Tick);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 480);
            this.Controls.Add(this.toolStripContainer1);
            this.KeyPreview = true;
            this.Name = "mainForm";
            this.Text = "七龙纪多号管理器";
            this.Activated += new System.EventHandler(this.mainForm_Activated);
            this.Deactivate += new System.EventHandler(this.mainForm_Deactivate);
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPageCurrent.ResumeLayout(false);
            this.tabPageSetting.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupbBox.ResumeLayout(false);
            this.groupbBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxHours)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udAttackRange)).EndInit();
            this.gbMission.ResumeLayout(false);
            this.gbMission.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udMinStep)).EndInit();
            this.gbBasic.ResumeLayout(false);
            this.gbBasic.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udOperateTimeOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udReadTimes)).EndInit();
            this.gbBuildUpdate.ResumeLayout(false);
            this.gbBuildUpdate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udUseDiamond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udStoreLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udFoodLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCrystalLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udShopLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udWoodLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udStoneLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udClearStoreTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udKeepHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udFoodPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udStonePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udCrystalPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udWoodPrice)).EndInit();
            this.tabPageProxy.ResumeLayout(false);
            this.tabPageProxy.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPageLog.ResumeLayout(false);
            this.tabPageLog.PerformLayout();
            this.tabPageUpdate.ResumeLayout(false);
            this.tabPageUpdate.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageSetting;
        private System.Windows.Forms.TabPage tabPageCurrent;
        private System.Windows.Forms.TabPage tabPageLog;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton btLastUser;
        private System.Windows.Forms.ToolStripButton btStartAuto;
        private System.Windows.Forms.ToolStripButton btNextUser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripStatusLabel workStatus;
        private System.Windows.Forms.ToolStripStatusLabel resStatus;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Timer timerWait;
        private System.Windows.Forms.CheckBox cbWoodSell;
        private System.Windows.Forms.CheckBox cbFoodSell;
        private System.Windows.Forms.CheckBox cbCrystalSell;
        private System.Windows.Forms.CheckBox cbStoneSell;
        private System.Windows.Forms.ToolStripButton btRegister;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripTextBox tbRegCount;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox ComboBoxServer;
        private System.Windows.Forms.ToolStripComboBox ComboBoxAccount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.CheckBox cbStopNight;
        private System.Windows.Forms.CheckBox cbClearStore;
        private System.Windows.Forms.CheckBox cbAutoSell;
        private System.Windows.Forms.ToolStripButton btEnterUser;
        private System.Windows.Forms.Label label1;
        private ExtendedWebBrowser  webBrowser;
        private System.Windows.Forms.Timer timerOut;
        private System.Windows.Forms.TextBox tbWorkLogs;
        private System.Windows.Forms.ToolStripStatusLabel timesStatus;
        private System.Windows.Forms.GroupBox groupbBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripStatusLabel timeOutStatus;
        private System.Windows.Forms.NumericUpDown udWoodPrice;
        private System.Windows.Forms.NumericUpDown udFoodPrice;
        private System.Windows.Forms.NumericUpDown udStonePrice;
        private System.Windows.Forms.NumericUpDown udCrystalPrice;
        private System.Windows.Forms.NumericUpDown udKeepHours;
        private System.Windows.Forms.NumericUpDown udOperateTimeOut;
        private System.Windows.Forms.ToolStripStatusLabel goldStatus;
        private System.Windows.Forms.NumericUpDown udReadTimes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btClearWorkLogs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btGetUserInfo;
        private System.Windows.Forms.GroupBox gbBuildUpdate;
        private System.Windows.Forms.CheckBox cbAutoBuild;
        private System.Windows.Forms.NumericUpDown udStoneLevel;
        private System.Windows.Forms.CheckBox cbStoneUpdate;
        private System.Windows.Forms.NumericUpDown udStoreLevel;
        private System.Windows.Forms.CheckBox cbStoreUpdate;
        private System.Windows.Forms.NumericUpDown udFoodLevel;
        private System.Windows.Forms.CheckBox cbFoodUpdate;
        private System.Windows.Forms.NumericUpDown udCrystalLevel;
        private System.Windows.Forms.CheckBox cbCrystalUpdate;
        private System.Windows.Forms.NumericUpDown udShopLevel;
        private System.Windows.Forms.CheckBox cbShopUpdate;
        private System.Windows.Forms.NumericUpDown udWoodLevel;
        private System.Windows.Forms.CheckBox cbWoodUpdate;
        private System.Windows.Forms.CheckBox cbAutoMission;
        private System.Windows.Forms.CheckBox cbAutoHunt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown udMinStep;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btDeleteAccount;
        private System.Windows.Forms.Timer timerOperate;
        private System.Windows.Forms.Timer timerAction;
        private System.Windows.Forms.NumericUpDown udAttackRange;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPageProxy;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btGetProxy;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbProxyURL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbProxyReg;
        private System.Windows.Forms.Button btProxyWeb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbProxyProtocolSplit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbProxyPortSplit;
        private System.Windows.Forms.TextBox tbProxyList;
        private System.Windows.Forms.Button btProxyCopy;
        private System.Windows.Forms.CheckBox cbHell;
        private System.Windows.Forms.CheckBox cbWild;
        private System.Windows.Forms.CheckBox cbWind;
        private System.Windows.Forms.CheckBox cbWater;
        private System.Windows.Forms.CheckBox cbAutoGov;
        private System.Windows.Forms.CheckBox cbAutoHero;
        private System.Windows.Forms.CheckBox cbAutoTax;
        private System.Windows.Forms.NumericUpDown udTaxHours;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gbMission;
        private System.Windows.Forms.GroupBox gbBasic;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ToolStripComboBox ComboBoxSection;
        private System.Windows.Forms.ToolStripButton btClearStore;
        private System.Windows.Forms.Button btAddPrice;
        private System.Windows.Forms.Button btSubPrice;
        private System.Windows.Forms.ToolStripButton btSaveSetting;
        private System.Windows.Forms.ToolStripComboBox ComboBoxAutoStyle;
        private System.Windows.Forms.CheckBox cbFullBuildStore;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbStoreRest;
        private System.Windows.Forms.ToolStripButton btUnLockUser;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbBuyResGold;
        private System.Windows.Forms.CheckBox cbBuyRes;
        private System.Windows.Forms.CheckBox cbUseDiamond;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.TabPage tabPageUpdate;
        private System.Windows.Forms.TextBox tbUpdateLog;
        private System.Windows.Forms.NumericUpDown udClearStoreTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown udUseDiamond;
        private System.Windows.Forms.RadioButton brRemoteDatabase;
        private System.Windows.Forms.RadioButton rbLocalDatabase;
        private System.Windows.Forms.DataGridViewTextBoxColumn 序号;
        private System.Windows.Forms.DataGridViewTextBoxColumn 账户;
        private System.Windows.Forms.DataGridViewTextBoxColumn 坐标;
        private System.Windows.Forms.DataGridViewTextBoxColumn 等级;
        private System.Windows.Forms.DataGridViewTextBoxColumn 挂单;
        private System.Windows.Forms.DataGridViewTextBoxColumn 任务;
        private System.Windows.Forms.DataGridViewTextBoxColumn 金币;
        private System.Windows.Forms.DataGridViewTextBoxColumn 钻石;
        private System.Windows.Forms.DataGridViewTextBoxColumn 仓库;
        private System.Windows.Forms.DataGridViewTextBoxColumn 木材;
        private System.Windows.Forms.DataGridViewTextBoxColumn 石头;
        private System.Windows.Forms.DataGridViewTextBoxColumn 水晶;
        private System.Windows.Forms.DataGridViewTextBoxColumn 食物;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时间;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbGoldMin;
        private System.Windows.Forms.TextBox tbGoldMin;
        private System.Windows.Forms.CheckBox cbGoldMax;
        private System.Windows.Forms.TextBox tbGoldMax;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.ComboBox ComboBoxSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton btDownLoad;
    }
}

