﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Net;
using mshtml;

namespace 七龙纪II
{
    public partial class mainForm : Form
    {
        #region 数据定义
        string sVersion = "-V20100618A";
        //操作数据集列表
        ArrayList alOperate = new ArrayList();
        int iOperateIndex;
        //动作列表
        ArrayList alAction = new ArrayList();
        int iActionIndex; //当前动作序号
        UserInfo userInfo; //用户信息，用户名，金币，木材，石头，水晶，粮食等的数量
        BuildingURL buildingURL;//建筑物URL列表
        
        RegUser regUser;//注册时候提取身份证号和姓名
        string sRegEmail; //注册时候的用户名
        string sRegPassword; //注册时候的密码
        string sRegVerifyCode; //注册、登录时候的验证码


        ServerInfo serverInfo;
        string sCurrentURL;//当前的URL地址
        string sUserName; //登录时的用户名
        string sPassword; //登录时的密码

        int iUserOrder;
        int iReadTimes;//最多读取信息次数，由于有些时候数据显示有点延迟，网页加载完成时数据不一定能读取到。
        int iAccountTimes ; //账户循环次数
        int iOperateTimeOut;//操作超时时间
        int iMissionStep;//任务步骤
        //升级建筑
        string sBuilding1;//第一个要建造的 农场,1
        string sBuilding2;//第二个要建造的 石头矿,2
        
        //市场参数
        int iShopTotal;//可挂单数
        int iShopUsed; //已挂单数
        
        int iStoreTotal; //仓库容量
        int iStoreUsed; //当前资源

        int iNPCAmount;  //NPC数量

        int iReordZero=0;//检索到记录数为0的次数。
        //地图参数
        string[] sMapDirect={"0,1","0,-1","-1,0","1,0","-1,1","1,1","-1,-1","1,-1"}; //四个方向随机取
        string sLastMapID =""; //地图的最上角小块ID
        int iMapX = 0;//初始位置
        int iMapY = 0;
        bool bHeroBusy = false;
        bool bDocumentLoaded;//用于标记网页已经载入。由于webbrowser载入完成事件会激发两次，所以在这里增加一个标记。让打开一个网页仅仅激发一次。
        string sAccessConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\verify.mdb;"; //数据库链接
        string sAccessConnStr2 = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\user.mdb;"; //数据库链接
        string sSQLServerConnStr = "Provider=SQLOLEDB;user id=sa;password=comefirstfangrenfu;initial catalog=qlj;Server=60.190.19.121;Connect Timeout=30";
        string sCurrentConnStr = "";
        #endregion
        public mainForm()
        {
            InitializeComponent();            
        }
        private void btStartAuto_Click(object sender, EventArgs e)
        {
            try
            {
                if (btStartAuto.Text == "开始")
                {
                    
                    btStartAuto.Text = "结束";
                    //从几个建筑中挑选出2个等级最低的建筑
                    sBuilding1 = "建筑1,998"; //默认情况下建造这两个。
                    sBuilding2 = "建筑2,998";
                    alAction.Clear();
                    alAction.Add("注销");
                    alAction.Add("登录");
                    //自动基础任务,仅仅获取步骤小于9的记录
                    if (cbAutoMission.Checked)
                    {
                        WebOperate.getAccountList(ComboBoxAccount,dataGridView,ComboBoxServer.SelectedItem.ToString(),ComboBoxSection.SelectedItem.ToString(), sCurrentConnStr,(int)udMinStep.Value,"全部","0","99999999");
                        alAction.Add("任务");

                    } 
                    alAction.Add("检测"); //检测账号是否正常
                    //自动内政
                    if (cbAutoGov.Checked)
                    {
                        alAction.Add("内政");
                    }
                    //自动建造
                    if (cbAutoBuild.Checked)
                    {
                        alAction.Add("建造");
                    }
                    //自动挂单
                    if (cbAutoSell.Checked)
                    {
                        alAction.Add("挂单");
                    }


                    if (cbAutoHunt.Checked)
                    {
                        alAction.Add("出征");
                    }

                    alAction.Add("下个账户");
                    alAction.Add("结束");
                    if (ComboBoxAccount.Items.Count > 0)
                    {
                        timerOut.Enabled = true; //启动计数器
                        iOperateTimeOut = (int)udOperateTimeOut.Value;
                        iReadTimes = (int)udReadTimes.Value;
                        tbWorkLogs.Text += "第" + iAccountTimes.ToString() + "次，" + DateTime.Now.ToString() + "\r\n";
                        iActionIndex = 0;
                        timerAction.Enabled = true;
                    }
                }
                else
                {
                    btStartAuto.Text = "开始";
                    timerOut.Enabled = false;
                    timerAction.Enabled = false;
                    timerOperate.Enabled = false;
                    timerWait.Enabled = false;
                    iOperateTimeOut = 999999;
                    iActionIndex = alAction.Count - 1;
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "btStartAuto" + ex.Message + ex.Data + "\r\n";
            }
            
        }
        /// <summary>
        /// 进行下一个动作
        /// </summary>
        private void NextAction()
        {
            timerAction.Enabled = false;
            string sActionName = "";
            try
            {                
                //列表不存在或者超过访问，那就结束程序。
                if (alAction != null && iActionIndex < alAction.Count)
                    sActionName = alAction[iActionIndex].ToString();
                else
                    return;
                iActionIndex++;
                switch (sActionName)
                {
                    case "登录":
                        #region 登录账号
                        sUserName = ComboBoxAccount.SelectedItem.ToString().Split(',')[0];
                        sPassword = ComboBoxAccount.SelectedItem.ToString().Split(',')[1];
                        iUserOrder = ComboBoxAccount.SelectedIndex+1;
                        bHeroBusy = false;
                        workStatus.Text = "登录...";
                        alOperate.Clear();
                        alOperate.Add("打开|<#LOGIN>");//打开登入页面
                        alOperate.Add("暂停|1");
                        alOperate.Add("登录信息|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("登录确定|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("打开|<#GAMEINDEX>");
                        alOperate.Add("暂停|1");
                        #endregion
                        break;
                    case "检测":
                        #region 检测是否正常登录
                        workStatus.Text = "账号检测...";
                        alOperate.Clear();
                        alOperate.Add("暂停|1");
                        alOperate.Add("打开|<#SERVER>city/index.ql");
                        alOperate.Add("暂停|1");
                        alOperate.Add("挂封判断|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("读取信息|");;
                        alOperate.Add("暂停|1");
                        alOperate.Add("读取建筑|");
                        alOperate.Add("暂停|1");
                        #endregion
                        break;     
                    case "任务":
                        #region 完成基础任务
                        alOperate.Clear();
                        iMissionStep = WebOperate.getUserMissionStep(sUserName, serverInfo.ServerURL,sCurrentConnStr);
                        workStatus.Text = "任务：" + iMissionStep;
                        switch (iMissionStep)
                        {
                            case 0:
                                #region 建立随机用户名
                                alOperate.Add("暂停|1");
                                alOperate.Add("打开|<#SERVER>pick.ql");
                                alOperate.Add("暂停|1");
                                alOperate.Add("随机用户|");
                                alOperate.Add("暂停|2");
                                alOperate.Add("重名确认|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|1");
                                 #endregion
                                break;
                            case 1:
                                #region 建造农场
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#农场>");//打开建造农场页面，这样可以跳到第2步
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("升级建筑|农场,1");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|2");
                                #endregion
                                break;
                            case 2:
                                #region 建造伐木场，建造石料厂
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#伐木场>");//打开建造农场页面
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("升级建筑|伐木场,1");
                                alOperate.Add("暂停|2");
                                alOperate.Add("打开|<#SERVER><#石头矿>");//打开建造农场页面
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("升级建筑|石头矿,1");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|2");
                                #endregion
                                break;
                            case 3:
                                #region 建造水晶矿，建造酒馆
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#水晶矿>");//打开建造农场页面
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("升级建筑|水晶矿,1");
                                alOperate.Add("暂停|2");
                                alOperate.Add("打开|<#SERVER><#空地1>");//打开建造酒馆
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|酒馆");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|2");
                                #endregion
                                break;
                            case 4:
                                #region 建造仓库,建造军事指挥部
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#空地1>");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|仓库");
                                alOperate.Add("暂停|2");
                                alOperate.Add("打开|<#SERVER><#空地2>");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|军事指挥所");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|2");
                                #endregion
                                break;
                            case 5:
                                #region 建造市场，初级兵营
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#空地1>");//打开建造市场
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|市场");
                                alOperate.Add("暂停|2");
                                alOperate.Add("打开|<#SERVER><#空地2>");//打开初级兵营
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|初级兵营");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|2");
                                #endregion
                                break;
                            case 6:
                                #region 升级2级农场，建造宝物仓库
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#农场>");//打开建造农场页面
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("升级建筑|农场,2");
                                alOperate.Add("暂停|2");
                                alOperate.Add("打开|<#SERVER><#空地1>");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|宝物仓库");

                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|2");
                                #endregion
                                break;
                            case 7: //
                                #region 收税，召唤英雄
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#市政中心>");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("超链接|收税,1");
                                alOperate.Add("暂停|2");
                                alOperate.Add("打开|<#SERVER><#酒馆>");
                                alOperate.Add("暂停|2");
                                alOperate.Add("雇佣英雄|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("对话确定|");

                                alOperate.Add("暂停|1");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|1");
                                #endregion
                                break;
                            case 9://建造联盟会所
                                #region
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER><#空地1>");
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("建造|联盟会所");
                                #endregion
                                break;
                            default:
                                #region 基本对话


                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("读取建筑|");
                                alOperate.Add("打开|<#SERVER>city/index.ql");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|欢迎");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|A,0"); //A标签，第2个
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");

                                alOperate.Add("暂停|2");
                                alOperate.Add("任务对话|发展农田");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");

                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|发展矿产");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");

                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|游侠");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");

                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|英雄的契约");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");

                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|建造军事指挥所");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");


                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("打开|<#SERVER>map/index.ql"); //打开地图页面

                                alOperate.Add("暂停|2");
                                alOperate.Add("出征|打野");
                                alOperate.Add("暂停|1");
                                alOperate.Add("出征模式|打野");
                                alOperate.Add("暂停|1");
                                alOperate.Add("英雄选择|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("出征确定|");
                                alOperate.Add("暂停|1");

                                alOperate.Add("打开|<#SERVER>city/index.ql"); //打开游戏主页
                                alOperate.Add("暂停|2");
                                alOperate.Add("任务对话|还以颜色");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");


                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|储存资源");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1");

                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|税收");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1"); //INPUT标签,第1个

                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|宝物仓库");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1"); //INPUT标签,第1个

                                /*
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务对话|建造兵营");
                                alOperate.Add("暂停|1");
                                alOperate.Add("任务确定|INPUT,1"); //INPUT标签,第1个
                                */
                                alOperate.Add("暂停|1");
                                alOperate.Add("读取信息|");
                                alOperate.Add("暂停|1");
                                alOperate.Add("下个步骤|");
                                alOperate.Add("暂停|1");

                                #endregion
                                break;
                        }
                        #endregion
                        break;
                    case "建造":
                        #region 建造
                        workStatus.Text = "建造...";
                        alOperate.Clear();
                        alOperate.Add("暂停|1");
                        alOperate.Add("建造判断|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("打开|<#SERVER><#BUILD1>"); //打开英雄页面
                        alOperate.Add("暂停|1");
                        alOperate.Add("读取信息|");
                        alOperate.Add("升级确定|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("打开|<#SERVER><#BUILD2>"); //打开英雄页面
                        alOperate.Add("暂停|1");
                        alOperate.Add("读取信息|");
                        alOperate.Add("升级确定|");
                        alOperate.Add("暂停|1");

                        //使用礼券
                        if (cbUseDiamond.Checked)
                        {
                            alOperate.Add("打开|<#SERVER>city/index.ql"); //打开城市页面
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取信息|");
                            alOperate.Add("使用礼券|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("任务确定|INPUT,4");
                            alOperate.Add("暂停|3");
                            alOperate.Add("跳转|-7");
                            alOperate.Add("暂停|1");
                        }
                        #endregion
                        break;
                    case "挂单":
                        #region 挂单流程
                        workStatus.Text = "挂单...";
                        alOperate.Clear();
                        alOperate.Add("暂停|1");
                        alOperate.Add("读取信息|");
                        alOperate.Add("NPC判断|");
                        alOperate.Add("打开|<#SERVER>market/selltonpc.ql");
                        alOperate.Add("NPC资源|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("对话确定|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("打开|<#SERVER>market/mytraderes.ql");
                        alOperate.Add("暂停|1");
                        alOperate.Add("读取信息|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("挂单资源|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("对话确定|");
                        alOperate.Add("暂停|1");
                        alOperate.Add("对话确定|");
                        alOperate.Add("暂停|2");
                        alOperate.Add("跳转|-11"); //跳转到11步以前
                        alOperate.Add("暂停|2");
                        #endregion
                        break;
                    case "内政":
                        #region 英雄复活、自动加点
                        workStatus.Text = "内政...";
                        alOperate.Clear();
                        //收税
                        int iHours = DateTime.Now.Hour;
                        if (cbAutoTax.Checked && iHours < udTaxHours.Value) //如果收税打钩了，并且在收税时间。
                        {
                            alOperate.Add("暂停|1");
                            alOperate.Add("打开|<#SERVER>city/index.ql");
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取建筑|");
                            alOperate.Add("打开|<#SERVER><#市政中心>"); //打开英雄页面
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取信息|");
                            alOperate.Add("超链接|收税,1");
                            alOperate.Add("暂停|1");
                        }
                        if (cbAutoHero.Checked)
                        {
                            
                            alOperate.Add("打开|<#SERVER>hero/index.ql"); //打开英雄页面
                            alOperate.Add("暂停|1");
                            alOperate.Add("尝试复活|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("复活英雄|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("对话确定|");
                            alOperate.Add("跳转|4");
                            alOperate.Add("暂停|1");
                            alOperate.Add("升级英雄|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("属性加点|");
                            alOperate.Add("暂停|1");
                        }
                        if (cbFullBuildStore.Checked)
                        {
                            alOperate.Add("扩仓判断|");
                            alOperate.Add("打开|<#SERVER><#仓库>");
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取信息|");
                            alOperate.Add("升级建筑|仓库,20");
                            alOperate.Add("暂停|1");
                        }
                        if (cbBuyRes.Checked)
                        {
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取信息|");
                            alOperate.Add("购买判断|");
                            alOperate.Add("打开|<#SERVER>market/indexres.ql?by=unit_price&&resType=1&asc=1");
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取信息|");
                            alOperate.Add("购买检查|");
                            alOperate.Add("超链接|购买,1");
                            alOperate.Add("暂停|1");
                            alOperate.Add("对话确定|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("打开|<#SERVER>market/indexres.ql?by=unit_price&&resType=2&asc=1");
                            alOperate.Add("暂停|1");
                            alOperate.Add("读取信息|");
                            alOperate.Add("购买检查|");
                            alOperate.Add("超链接|购买,1");
                            alOperate.Add("暂停|1");
                            alOperate.Add("对话确定|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("跳转|-19");
                            alOperate.Add("暂停|1");

                        }
                        alOperate.Add("暂停|1");
                        #endregion
                        break;
                    case "出征":
                        #region 英雄出征
                        workStatus.Text = "出征...";
                        alOperate.Clear();  
                        if (cbWater.Checked)  //水车
                        {
                            alOperate.Add("英雄检查|");
                            alOperate.Add("打开|<#SERVER>map/index.ql"); //打开地图页面
                            alOperate.Add("暂停|2");
                            alOperate.Add("出征|水车");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征模式|水车");
                            alOperate.Add("暂停|1");
                            alOperate.Add("英雄选择|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征确定|");
                            alOperate.Add("暂停|1");
                        }
                        if (cbWind.Checked)
                        { 
                            alOperate.Add("英雄检查|"); //风车
                            alOperate.Add("打开|<#SERVER>map/index.ql"); //打开地图页面
                            alOperate.Add("暂停|2");
                            alOperate.Add("出征|风车");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征模式|风车");
                            alOperate.Add("暂停|1");
                            alOperate.Add("英雄选择|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征确定|");
                            alOperate.Add("暂停|1");
                        }
                        if (cbHell.Checked)
                        {
                            alOperate.Add("英雄检查|"); //恶魔城
                            alOperate.Add("打开|<#SERVER>map/index.ql"); //打开地图页面
                            alOperate.Add("暂停|2");
                            alOperate.Add("出征|恶魔城");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征模式|恶魔城");
                            alOperate.Add("暂停|1");
                            alOperate.Add("英雄选择|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征确定|");
                            alOperate.Add("暂停|1");
                        }
                        if (cbWild.Checked)
                        {
                            alOperate.Add("英雄检查|"); //打野
                            alOperate.Add("打开|<#SERVER>map/index.ql"); //打开地图页面
                            alOperate.Add("暂停|2");
                            alOperate.Add("出征|打野");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征模式|打野");
                            alOperate.Add("暂停|1");
                            alOperate.Add("英雄选择|");
                            alOperate.Add("暂停|1");
                            alOperate.Add("出征确定|");
                            alOperate.Add("暂停|1");
                        }
                        alOperate.Add("暂停|1");
                        #endregion
                        break;
                    case "注销":
                        #region 清空Cookies
                        workStatus.Text = "注销...";
                        buildingURL = null;
                        alOperate.Clear();
                        alOperate.Add("暂停|1");
                        alOperate.Add("浏览器重置|");
                        alOperate.Add("暂停|2");
                        #endregion
                        break;
                    case "注册":
                        #region 注册流程
                        alOperate.Clear();
                        alOperate.Add("打开|<#REGISTER>");;
                        alOperate.Add("基本信息|");
                        alOperate.Add("打开|<#HUMAN>");
                        alOperate.Add("暂停|1");
                        alOperate.Add("防沉迷|");
                        alOperate.Add("注册完成|");
                        alOperate.Add("暂停|5");
                        #endregion
                        break;
                    case "下个账户":
                        #region 下个账户
                        workStatus.Text = "下一个...";
                        loadSetting(); //一轮了以后再次载入  
                        if (ComboBoxAccount.SelectedIndex + 1 >= ComboBoxAccount.Items.Count)
                        {
                            ComboBoxAccount.SelectedIndex = 0;
                            iAccountTimes++;
                            if(cbAutoMission.Checked)
                                WebOperate.getAccountList(ComboBoxAccount, dataGridView, ComboBoxServer.SelectedItem.ToString(), ComboBoxSection.SelectedItem.ToString(), sCurrentConnStr, (int)udMinStep.Value,"全部","0","99999999");
                            else
                                WebOperate.getAccountList(ComboBoxAccount, dataGridView, ComboBoxServer.SelectedItem.ToString(), ComboBoxSection.SelectedItem.ToString(), sCurrentConnStr, 999, "全部", "0", "99999999");
                            if (ComboBoxAccount.Items.Count <=0)
                            {
                                timerOut.Enabled = false; //启动计数器
                                iOperateTimeOut = (int)udOperateTimeOut.Value;
                                iReadTimes = (int)udReadTimes.Value;
                                workStatus.Text = "所有账号任务完成。";
                                iActionIndex = 0;
                                timerAction.Enabled = false;
                                btStartAuto.Text = "开始";
                            }
                            tbWorkLogs.Text += "第" + iAccountTimes.ToString() + "次，" + DateTime.Now.ToString() + "\r\n";
                        }
                        else
                            ComboBoxAccount.SelectedIndex++;
                        //从几个建筑中挑选出2个等级最低的建筑
                        sBuilding1 = "建筑1,998"; //默认情况下建造这两个。
                        sBuilding2 = "建筑2,998";
                        iActionIndex = 0;
                        timerAction.Enabled = true;
                        #endregion
                        return;  //必须使用放回，否则会进入两个退出
                    case "结束":
                        #region 结束挂机
                        workStatus.Text = "结束...";
                        alOperate.Clear();
                        iOperateIndex = 0;
                        timerOut.Enabled = false;
                        #endregion
                        break;
                    default: break;
                }
                iOperateIndex = 0;
                timerOperate.Enabled = true;
                return;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += sActionName+ex.Message+ex.Data+"\r\n";
            }
        }
        /// <summary>
        /// 进行下一个步骤。
        /// </summary>
        private void NextOperate()
        {
            timerOperate.Enabled = false;
            string sCurrentOperate = "";
            try
            {
                if (iOperateIndex >= alOperate.Count)
                {
                    timerAction.Enabled = true;
                    return;
                }
                iOperateTimeOut = (int)udOperateTimeOut.Value;
                sCurrentOperate = alOperate[iOperateIndex].ToString();
                string sOperateName = sCurrentOperate.Split('|')[0];
                string sParamString = sCurrentOperate.Split('|')[1];
                iOperateIndex++;
                switch (sOperateName)
                {
                    case "基本信息":
                        #region 填写注册基本信息
                        try
                        {
                            bDocumentLoaded = false;
                            switch (serverInfo.ServerProvider)
                            {
                                case "uuplay.com":
                                    Bitmap image = (Bitmap)WebOperate.getVerifyCodePic(webBrowser, "", "/ValidateCode", "");
                                    sRegVerifyCode = WebOperate.getVerifyCode(image, 1, sAccessConnStr);
                                    image.Dispose();
                                    sRegEmail = WebOperate.getRandomEmailAddress(5, 6);
                                    sRegPassword = WebOperate.getRandomString(6,7,2);
                                    WebOperate.putInData(webBrowser, "email", sRegEmail);
                                    WebOperate.putInData(webBrowser, "nickName", WebOperate.getRandomString(4, 5, 2));
                                    WebOperate.putInData(webBrowser, "password", sRegPassword);
                                    WebOperate.putInData(webBrowser, "password2", sRegPassword);
                                    WebOperate.putInData(webBrowser, "validcode", sRegVerifyCode);
                                    WebOperate.ButtonClick(webBrowser, "value", "立即注册", 0);
                                    iOperateIndex+=2; //跳过打开防沉迷和暂停步骤
                                    break;
                                case "qlj2.ewtang.com":
                                    sRegEmail = WebOperate.getRandomString(6,7,2);
                                    sRegPassword = WebOperate.getRandomString(6,7,2);
                                    WebOperate.putInData(webBrowser, "account", 0, sRegEmail);
                                    WebOperate.putInData(webBrowser, "passwd", 0, sRegPassword);
                                    WebOperate.putInData(webBrowser, "confirmPasswd", 0, sRegPassword);
                                    WebOperate.putInData(webBrowser, "email", 0, WebOperate.getRandomEmailAddress(5, 6));
                                    WebOperate.putInData(webBrowser, "verifyNumber", 0, "");
                                    break;
                                case "ly.kunlun.com":
                                    sRegEmail = WebOperate.getRandomString(6, 7, 2);
                                    sRegPassword = WebOperate.getRandomString(7, 9, 2);
                                    WebOperate.putInData(webBrowser, "username", 0, sRegEmail);
                                    WebOperate.putInData(webBrowser, "nickname",0, WebOperate.getRandomString(6, 8, 2));
                                    WebOperate.putInData(webBrowser, "password", 0, sRegPassword);
                                    WebOperate.putInData(webBrowser, "repassword", 0, sRegPassword);
                                    WebOperate.putInData(webBrowser, "email", 0, WebOperate.getRandomEmailAddress(5, 6));
                                    WebOperate.putInData(webBrowser, "verify", 0,"");
                                    break;
                                case "qilongji2.ourgame.com":
                                    sRegEmail = WebOperate.getRandomString(6, 7, 2);
                                    sRegPassword = WebOperate.getRandomString(9, 10, 2);
                                    regUser = WebOperate.getRegUser(sCurrentConnStr);
                                    WebOperate.putInData(webBrowser, "username", sRegEmail);
                                    WebOperate.putInData(webBrowser, "password", sRegPassword);
                                    WebOperate.putInData(webBrowser, "cpassword", sRegPassword);
                                    WebOperate.putInData(webBrowser, "personid", regUser.UserID);
                                    WebOperate.putInData(webBrowser, "nickname2", regUser.UserName);
                                    WebOperate.putInData(webBrowser, "uemail", WebOperate.getRandomEmailAddress(5, 6));
                                    webBrowser.Document.GetElementById("xieyi").InvokeMember("click");
                                    WebOperate.putInData(webBrowser, "rndnumber", ""); 
                                    bDocumentLoaded=true;
                                    iOperateIndex++; //跳过打开防沉迷步骤
                                    timerOperate.Enabled = true;
                                    break;
                                case "qlj2.uusee.com":
                                    sRegEmail = WebOperate.getRandomEmailAddress(6,7);
                                    sRegPassword = WebOperate.getRandomString(6, 7, 2);
                                    regUser = WebOperate.getRegUser(sCurrentConnStr);
                                    WebOperate.putInData(webBrowser, "email", sRegEmail);
                                    WebOperate.putInData(webBrowser, "username", regUser.UserName);
                                    WebOperate.putInData(webBrowser, "password", sRegPassword);
                                    WebOperate.putInData(webBrowser, "chkpassword", sRegPassword);
                                    webBrowser.Document.GetElementById("ppdoul").InvokeMember("click");
                                    iOperateIndex += 3;
                                    break;
                                case "qlj2.game5.cn":
                                    Bitmap image2 = (Bitmap)WebOperate.getVerifyCodePic(webBrowser,"valid_src","","");
                                    sRegVerifyCode = WebOperate.getVerifyCode(image2,4, sAccessConnStr);
                                    image2.Dispose();
                                    if (sRegVerifyCode.IndexOf("*") >= 0||sRegVerifyCode.Length!=4)
                                    {
                                        iOperateIndex -= 2;
                                        timerOperate.Enabled = true;
                                        break;
                                    }
                                    sRegEmail = WebOperate.getRandomString(5, 6, 2);
                                    string sUserMail = WebOperate.getRandomEmailAddress(6, 7);
                                    sRegPassword = WebOperate.getRandomString(6, 7,2);
                                    regUser = WebOperate.getRegUser(sCurrentConnStr);
                                    WebOperate.putInData(webBrowser, "user_code", sRegEmail);
                                    WebOperate.putInData(webBrowser, "user_password", sRegPassword);
                                    WebOperate.putInData(webBrowser, "user_password_confirm", sRegPassword);
                                    WebOperate.putInData(webBrowser, "user_email",sUserMail);
                                    WebOperate.putInData(webBrowser, "verifyCode", sRegVerifyCode);
                                    webBrowser.Document.GetElementById("reg_buttom").InvokeMember("click");
                                    iOperateIndex += 3;
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "防沉迷":
                        #region 填写身份证信息
                        try
                        {
                            //检查是否打开了信息输入界面。
                            if (webBrowser.Url.ToString()!=serverInfo.RegID)
                            {
                                iOperateIndex = 0; //返回第一步基本信息页面
                                timerOperate.Enabled = true;
                                return;
                            }
                            else
                            {
                                bDocumentLoaded = false;
                                regUser = WebOperate.getRegUser(sCurrentConnStr);
                                switch (serverInfo.ServerProvider)
                                {
                                    case "uuplay.com":
                                        WebOperate.putInData(webBrowser, "realName", regUser.UserName);
                                        WebOperate.putInData(webBrowser, "nickName", regUser.UserID);
                                        WebOperate.ButtonClick(webBrowser, "value", "验证", 0);
                                        break;
                                    case "qlj2.ewtang.com":
                                        WebOperate.putInData(webBrowser, "nickname", 0, WebOperate.getRandomString(6, 8, 2));
                                        WebOperate.putInData(webBrowser, "realname", 0, regUser.UserName);
                                        WebOperate.putInData(webBrowser, "idcard", 0, regUser.UserID);
                                        WebOperate.ButtonClick(webBrowser, "alt", "确认提交", 0);
                                        break;
                                    case "ly.kunlun.com":
                                        WebOperate.putInData(webBrowser, "truename", 0, regUser.UserName);
                                        WebOperate.putInData(webBrowser, "idcode", 0, regUser.UserID);
                                        WebOperate.ButtonClick(webBrowser, "value", "提 交", 0);
                                        break;
                                    case "qilongji2.ourgame.com":
                                        //不需要做任何操作
                                        break;
                                    case "qlj2.uusee.com":
                                        break;
                                    default:
                                        break;
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "注册完成":
                        #region 注册完成进行下一步
                        try
                        {
                            if (webBrowser.Url.ToString() != serverInfo.RegDone && serverInfo.ServerProvider != "qlj2.game5.cn")
                            {
                                iOperateIndex -=2; //返回上一步
                                timerOperate.Enabled = true;
                                return;
                            }
                            else
                            {
                                int iRow = WebOperate.addUser(serverInfo.ServerURL, regUser.UserName, regUser.UserID, sRegEmail, sRegPassword, sCurrentConnStr);
                                if (iRow == 1)
                                {
                                    //注册数减1
                                    tbRegCount.Text = (Convert.ToInt32(tbRegCount.Text) - 1).ToString();
                                }

                                if (Convert.ToInt32(tbRegCount.Text) > 0)
                                {
                                    //注销一下。
                                    
                                    iOperateIndex = 0;
                                    if (serverInfo.LogOut.Trim() != "") //如果地址不是空的，那就执行注销操作。否者进入第一步
                                    {
                                        bDocumentLoaded = false;
                                        webBrowser.Navigate(serverInfo.LogOut);
                                    }
                                    else
                                    {
                                        timerOperate.Enabled = true;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "浏览器重置":
                        #region 浏览器重置
                        //收回所有的资源
                        try
                        {
                            workStatus.Text = "浏览器重置";
                            if (serverInfo.LogOut.Trim() != "") //如果地址不是空的，那就执行注销操作。否者进入第一步
                            {
                                bDocumentLoaded = false;
                                webBrowser.Navigate(serverInfo.LogOut);
                            }
                            else
                            {
                                timerOperate.Enabled = true;
                            }

                            WiniNet.InternetSetOption(0,42, IntPtr.Zero, 0);
                            return;

                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                       // break;
                    case "打开":
                        #region 打开某个页面
                        try
                        {
                            bDocumentLoaded = false;
                            sCurrentURL = sParamString.Replace("<#SERVER>", serverInfo.ServerURL); //服务器地址
                            sCurrentURL = sCurrentURL.Replace("<#LOGIN>", serverInfo.Login);//登录页面
                            sCurrentURL = sCurrentURL.Replace("<#GAMEINDEX>", serverInfo.GameIndex);//游戏主页面
                            sCurrentURL = sCurrentURL.Replace("<#REGISTER>", serverInfo.RegStart); //注册开始页面
                            sCurrentURL = sCurrentURL.Replace("<#HUMAN>", serverInfo.RegID); //防沉迷信息页面
                            sCurrentURL = sCurrentURL.Replace("<#USERNAME>", sUserName); //用户名
                            sCurrentURL = sCurrentURL.Replace("<#PASSWORD>", sPassword); //密码
                            if (buildingURL != null)
                            {

                                sCurrentURL = sCurrentURL.Replace("<#BUILD1>",sBuilding1.Split(',')[0]);
                                sCurrentURL = sCurrentURL.Replace("<#BUILD2>", sBuilding2.Split(',')[0]);
                                sCurrentURL = sCurrentURL.Replace("<#农场>", buildingURL.Food != null ? buildingURL.Food.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#伐木场>", buildingURL.Wood != null ? buildingURL.Wood.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#石头矿>", buildingURL.Stone != null ? buildingURL.Stone.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#水晶矿>", buildingURL.Crystal != null ? buildingURL.Crystal.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#仓库>", buildingURL.Store != null ? buildingURL.Store.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#市场>", buildingURL.Shop != null ? buildingURL.Shop.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#空地1>", buildingURL.Blank != null ? buildingURL.Blank[0].Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#空地2>", buildingURL.Blank != null ? buildingURL.Blank[1].Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#市政中心>", buildingURL.Gov != null ? buildingURL.Gov.Split(',')[0] : "");
                                sCurrentURL = sCurrentURL.Replace("<#酒馆>", buildingURL.Bar != null ? buildingURL.Bar.Split(',')[0] : "");

                                if(sCurrentURL.IndexOf("建筑")>0) //如果包含建筑两个字，表示没有找到下个升级的DD，直接退出本步骤
                                {
                                    iOperateIndex = alOperate.Count - 1;
                                    timerOperate.Enabled = true;
                                    return;
                                }
                            }
                            webBrowser.Navigate(sCurrentURL);
                            // tbWorkLogs.Text += sCurrentURL + "\r\n";
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "登录信息":
                        #region 登录信息
                        try
                        {
                            bDocumentLoaded = false;
                            workStatus.Text = "登录";
                            timesStatus.Text = "第" + iAccountTimes.ToString() + "遍，第" + (ComboBoxAccount.SelectedIndex + 1) + "个，共" + ComboBoxAccount.Items.Count + "个";
                            HtmlElement heUUplay = webBrowser.Document.GetElementById("validcode"); //uuplay.com
                            HtmlElement heKunLun = webBrowser.Document.GetElementById("verifypic"); //kunlun.com
                            HtmlElement heOurGame = webBrowser.Document.GetElementById("username"); //ourgame.com
                            HtmlElement heUsee = webBrowser.Document.GetElementById("pplogion"); //usee.com
                            HtmlElement heGame5 = webBrowser.Document.GetElementById("user_code"); //usee.com
                            HtmlElementCollection hecEwtang = webBrowser.Document.All.GetElementsByName("account"); //etang.com
                            if (heUUplay == null &&heKunLun==null&& hecEwtang.Count == 0&&heOurGame==null&&heUsee==null&&heGame5==null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，必须是退2步才能到暂停。
                                    timerOperate.Enabled = true;
                                    return;
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "登录查找信息输入框" + udReadTimes.Value.ToString() + "次。\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                }
                                break;
                            }
                            else
                            {
                              //  if (iReadTimes != (int)udReadTimes.Value)
                              //      tbWorkLogs.Text += "登录信息需要" + ((int)udReadTimes.Value - iReadTimes).ToString() + "次。\r\n";
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                switch (serverInfo.ServerProvider)
                                {
                                    case "uuplay.com":
                                        WebOperate.putInData(webBrowser, "email", sUserName);
                                        WebOperate.putInData(webBrowser, "password", sPassword);
                                        //输入验证码
                                        Bitmap image = (Bitmap)WebOperate.getVerifyCodePic(webBrowser, "", "/ValidateCode", "");
                                        sRegVerifyCode = WebOperate.getVerifyCode(image, 1, sAccessConnStr);
                                        image.Dispose();
                                        if (sRegVerifyCode.IndexOf('*') >= 0) //有识别不出来的代码的话，跳回第一步
                                        {
                                            iOperateIndex = 0;
                                            timerOperate.Enabled = true;
                                            return;
                                        }
                                        WebOperate.putInData(webBrowser, "validcode", sRegVerifyCode);
                                        WebOperate.ButtonClick(webBrowser, "name", "submit_login", 0);
                                        iOperateIndex=alOperate.Count-1; //跳到最后一步
                                        break;
                                    case "qlj2.ewtang.com":
                                        WebOperate.putInData(webBrowser, "account", 0, sUserName);
                                        WebOperate.putInData(webBrowser, "passwd", 0, sPassword);
                                        WebOperate.ButtonClick(webBrowser, "alt", "登录", 0);
                                        iOperateIndex = alOperate.Count - 1; //跳到最后一步
                                        break;
                                    case "ly.kunlun.com":
                                        WebOperate.putInData(webBrowser, "loginName", sUserName);
                                        WebOperate.putInData(webBrowser, "loginPass", sPassword);

                                        Bitmap image2 = (Bitmap)WebOperate.getVerifyCodePic(webBrowser, "", "http://user.ly.kunlun.com/Verify/login", "");
                                        sRegVerifyCode = WebOperate.getVerifyCode(image2, 2, sAccessConnStr);
                                        //输入验证码
                                        image2.Dispose();
                                        if (sRegVerifyCode.IndexOf('*') >= 0) //有识别不出来的代码的话，跳回第一步
                                        {
                                            iOperateIndex = 0;
                                            timerOperate.Enabled = true;
                                            return;
                                        }
                                        WebOperate.putInData(webBrowser, "validate", sRegVerifyCode);
                                        WebOperate.ButtonClick(webBrowser, "name", "submit_login", 0); //需要地址转向

                                        break;
                                    case "qilongji2.ourgame.com":
                                        WebOperate.putInData(webBrowser, "username", sUserName);
                                        WebOperate.putInData(webBrowser, "password", sPassword);
                                        //输入验证码
                                        Bitmap image3 = (Bitmap)WebOperate.getVerifyCodePic(webBrowser, "imgValidate", "", "");
                                        sRegVerifyCode = WebOperate.getVerifyCode(image3, 3, sAccessConnStr);
                                        image3.Dispose();
                                        if (sRegVerifyCode.IndexOf('*') >= 0) //有识别不出来的代码的话，跳回第一步
                                        {
                                            iOperateIndex = 0;
                                            timerOperate.Enabled = true;
                                            return;
                                        }
                                        WebOperate.putInData(webBrowser, "rndnumber", sRegVerifyCode);
                                        webBrowser.Document.GetElementById("imgUserLogin").InvokeMember("click");
                                        timerOperate.Enabled = true;
                                        break;
                                    case "qlj2.uusee.com":
                                       // WebOperate.putInData(webBrowser, "pemail", sUserName);
                                       // WebOperate.putInData(webBrowser, "password", sPassword);
                                       // WebOperate.ButtonClick(webBrowser, "name", "Submit3", 0); //需要地址转向

                                        break;
                                    case "qlj2.game5.cn":
                                        WebOperate.putInData(webBrowser, "user_code", sUserName);
                                        WebOperate.putInData(webBrowser, "user_password", sPassword);
                                        WebOperate.ButtonClick(webBrowser, "classname", "rinput", 0); //需要地址转向
                                        break;
                                    default:
                                        break;
                                }
                                timerOperate.Enabled = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "登录确定":
                        #region 登录确定
                        try
                        {
                            switch (serverInfo.ServerProvider)
                            {
                                case "uuplay.com":
                                    //不用做任何事
                                    timerOperate.Enabled = true;
                                    break;
                                case "qlj2.ewtang.com":
                                    //不用做任何事
                                    timerOperate.Enabled = true;
                                    break;
                                case "ly.kunlun.com":
                                    //不用做任何事
                                    timerOperate.Enabled = true;
                                    break;
                                case "qilongji2.ourgame.com":
                                    //网页内容中显示出账号了，表示登录成功了。还没有显示的话，继续等待
                                    if (webBrowser.Document.Body.InnerText.IndexOf("游戏账号：" + sUserName) < 0)
                                    {
                                        if (iReadTimes > 0)
                                        {
                                            iReadTimes--;
                                            iOperateIndex = iOperateIndex - 2; //返回上一步，需要退两步
                                            timerOperate.Enabled = true;
                                        }
                                        else
                                        {
                                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                            tbWorkLogs.Text += "登录确定" + userInfo.UserName + "超过" + udReadTimes.Value.ToString() + "次。\r\n";
                                            iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                            break; //跳出这里了。
                                        }
                                    }
                                    else
                                    {
                                      //  if (iReadTimes != (int)udReadTimes.Value)
                                       //     tbWorkLogs.Text += "登录确认需要" + ((int)udReadTimes.Value + 1 - iReadTimes).ToString() + "次。\r\n";
                                        iReadTimes = (int)udReadTimes.Value;
                                        timerOperate.Enabled = true;
                                        
                                    }
                                    //根据网页内容检查是否登录成功 
                                    break;
                                case "qlj2.uusee.com":
                                    if (webBrowser.Document.Body.InnerText.IndexOf("邮箱：" + sUserName) < 0)
                                    {
                                        if (iReadTimes > 0)
                                        {
                                            iReadTimes--;
                                            iOperateIndex = iOperateIndex - 2; //返回上一步，需要退两步
                                            timerOperate.Enabled = true;
                                        }
                                        else
                                        {
                                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                            tbWorkLogs.Text += "登录确定" + userInfo.UserName + "超过" + udReadTimes.Value.ToString() + "次。\r\n";
                                            iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                            break; //跳出这里了。
                                        }
                                    }
                                    else
                                    {
                                        //  if (iReadTimes != (int)udReadTimes.Value)
                                        //     tbWorkLogs.Text += "登录确认需要" + ((int)udReadTimes.Value + 1 - iReadTimes).ToString() + "次。\r\n";
                                        iReadTimes = (int)udReadTimes.Value;
                                        timerOperate.Enabled = true;

                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "随机用户":
                        #region 第一次登录时候的随机用户名
                        try
                        {

                            if (webBrowser.Url.AbsolutePath.ToString() != "/pick.ql")
                            {
                                WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 1, sCurrentConnStr);
                                iActionIndex--; //跳到上一步，重新开始任务。
                                timerAction.Enabled = true;
                                break;
                            }
                            HtmlElement he = webBrowser.Document.GetElementById("name_tit");
                            if (he == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，必须是退2步才能到暂停。
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "随机用户" + sUserName + "超过" + udReadTimes.Value.ToString() + "次。\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    break;
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value;
                                WebOperate.putInData(webBrowser, "userName",0, WebOperate.getNickName(sCurrentConnStr));
                                WebOperate.ButtonClick(webBrowser, "name", "pick", 0);
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                         catch (Exception ex)
                         {
                             tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                             tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                             throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                         }
                        #endregion
                       // break;
                    case "重名确认":
                        #region 重名确认
                        try
                        {
                            userInfo = null;
                            userInfo = WebOperate.getUserInfo(webBrowser, sUserName);
                            if (userInfo.Gold == null || userInfo.Gold == "")
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 5;//返回到英雄选择页面。 
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "重名确认" + userInfo.UserName + "信息超过" + udReadTimes.Value.ToString() + "次。\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    break; //跳出这里了。
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value;
                                getUserInfo(); 
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                       // break;
                    case "暂停":
                        #region 暂停
                        try
                        {   Random rnd=new Random();
                            timerWait.Interval = Convert.ToInt32(sParamString)*rnd.Next(500,1000);
                            timerWait.Enabled = true;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "读取信息":
                        #region 读取账号信息
                        try
                        {
                            workStatus.Text = "读取信息";
                            userInfo = null;
                            userInfo = WebOperate.getUserInfo(webBrowser, sUserName);
                            if (userInfo.Gold == null || userInfo.Gold == "")
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，必须是退2步才能到暂停。
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "读取账号" + userInfo.UserName + "信息超过" + udReadTimes.Value.ToString() + "次。\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    break; //跳出这里了。
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value;
                                getUserInfo();
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //  break;
                    case "读取建筑":
                        #region 读取建筑信息
                        try
                        {
                            workStatus.Text = "读取建筑";

                            HtmlElement he = webBrowser.Document.GetElementById("simWinPop");
                            if (he==null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，必须是退2步才能到暂停。
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "读取建筑" + userInfo.UserName + "信息超过" + udReadTimes.Value.ToString() + "次。\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    break; //跳出这里了。
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value;
                                buildingURL = null;
                                buildingURL = WebOperate.getBuildingURL(webBrowser); //务必写在读取信息后面，保证信息的正常读取。

                                WebOperate.getCityPlace(webBrowser, dataGridView, serverInfo.ServerURL, sUserName, sSQLServerConnStr);

                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                      //  break;
                    case "使用礼券":
                        #region 使用礼券
                        try
                        {
                            HtmlElementCollection hec = webBrowser.Document.GetElementsByTagName("LI");
                            bool bBuild = false;
                            for (int i = 0; i < hec.Count; i++)
                            {
                                if (hec[i].GetAttribute("classname") == "build")
                                {
                                    string sBuildText = hec[i].InnerText;
                                    int iBuildLevel = Convert.ToInt32(sBuildText.Substring(sBuildText.IndexOf('(') + 1, sBuildText.IndexOf(')') - sBuildText.IndexOf('(') - 1).Trim().Replace("LV.", ""));
                                    //从仅仅石头矿、伐木场、仓库使用礼券改为只要有礼券就用。
                                    // if ((sBuildText.IndexOf("石头矿") >= 0 || sBuildText.IndexOf("伐木场") >= 0 || sBuildText.IndexOf("仓库") >= 0) && iBuildLevel >= udUseDiamond.Value&&Convert.ToInt32(userInfo.Diamond.Split('/')[1])>0)
                                    if (iBuildLevel >= udUseDiamond.Value && Convert.ToInt32(userInfo.Diamond.Split('/')[1]) > 0)
                                    {
                                        hec[i].GetElementsByTagName("A")[1].InvokeMember("click");
                                        bBuild = true;
                                        break;
                                    }
                                }
                            }
                            if (bBuild == false)
                            {
                                iOperateIndex = iOperateIndex + 4;
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                       // break;
                    case "挂封判断":
                        #region 挂封判断
                        try
                        {
                            workStatus.Text = "外挂判断";
                            string sCurrentURL = webBrowser.Url.AbsolutePath;
                            if (sCurrentURL == "/checkplayer.ql")  //如果进入了外挂检测页面了，就进入下一个用户
                            {
                                WebOperate.setLockStatus(serverInfo.ServerURL,sUserName, "1", sCurrentConnStr);
                                iActionIndex = alAction.Count - 2;
                                iOperateIndex = 0;
                                timerAction.Enabled = true;
                                break;
                            }

                            if (webBrowser.DocumentText.IndexOf("任何形式的一机多号，包括多人同机操作、账号他人托管，都将是禁止的") > 0 || webBrowser.DocumentTitle == "《七龙纪II》Age of Seventh DragonII 官方网站-英雄养成&战争策略 网页游戏") //地址栏里面包含了登陆几个字母，很可能被封了。
                            {
                                WebOperate.setLockStatus(serverInfo.ServerURL, sUserName, "2", sCurrentConnStr);
                                iActionIndex = alAction.Count - 2;
                                iOperateIndex = 0;
                                timerAction.Enabled = true;
                                break;
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion 
                       // break;
                    case "扩仓判断":
                        #region 扩仓判断
                        try
                        {   //还有大于5000的容量，就不扩仓了。
                            workStatus.Text = "扩仓判断";
                            if (Convert.ToInt32(userInfo.Store.Split('/')[0]) + Convert.ToInt32(tbStoreRest.Text) < Convert.ToInt32(userInfo.Store.Split('/')[1]))
                            {
                                iOperateIndex = iOperateIndex + 4;
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                          catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion 
                     //   break;
                    case "购买判断":
                        #region 购买资源判断
                        try
                        {   //如果仓库剩余容量小于5000，或者金币小于指定数额，或者时间不在10点到23点之前就不购买了,或者可购买的木头石头都为0.
                            workStatus.Text = "购买判断";
                            int iGoldAmount = Convert.ToInt32(userInfo.Gold.Split('/')[0]); //金币数
                            int iStoreCurrent = Convert.ToInt32(userInfo.Store.Split('/')[0]);
                            int iStoreMax = Convert.ToInt32(userInfo.Store.Split('/')[1]);
                            int iHours = DateTime.Now.Hour;
                            if (iStoreCurrent+5000 > iStoreMax||iGoldAmount<Convert.ToInt32(tbBuyResGold.Text)||iHours<10||iHours>=23||iReordZero>=1)
                            {
                                iOperateIndex = iOperateIndex + 17;
                            }
                            iReordZero = 0; //重置记录条数为0的次数
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //break;
                    case "购买检查":
                        #region 购买前检查
                        try
                        {   
                            //检查检索出来的记录条数
                            if (userInfo.RecordCount != null && userInfo.RecordCount != "") //记录条数检索的时候
                            {
                                int iRecordCount = Convert.ToInt32(userInfo.RecordCount.Split('/')[0]);
                                if (iRecordCount <= 0)
                                {
                                    iOperateIndex += 3;
                                    iReordZero++;
                                    workStatus.Text = "资源数为0";
                                }
                            }
                            if (webBrowser.Document.Body.InnerText.IndexOf("该城市尚未建造市场") >= 0 || webBrowser.Document.Body.InnerText.IndexOf("英雄达到10级之前，不能使用市场功能") >= 0)
                            {
                                iOperateIndex += 3;
                                iReordZero++;
                                workStatus.Text = "市场未建或英雄等级不足。";
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                      //  break;
                    case "建造判断":
                        #region 建造判断
                        try
                        {
                            //比较各种建筑等级。
                            if (buildingURL != null)
                            {
                                //与已知建筑不同，并且没有达到建筑目标
                                sBuilding1 = Convert.ToInt32(buildingURL.Stone.Split(',')[1]) < udStoneLevel.Value && Convert.ToInt32(buildingURL.Stone.Split(',')[1]) < Convert.ToInt32(sBuilding1.Split(',')[1]) ? buildingURL.Stone : sBuilding1;
                                sBuilding1 = Convert.ToInt32(buildingURL.Wood.Split(',')[1]) < udWoodLevel.Value && Convert.ToInt32(buildingURL.Wood.Split(',')[1]) < Convert.ToInt32(sBuilding1.Split(',')[1]) ? buildingURL.Wood : sBuilding1;
                                sBuilding1 = Convert.ToInt32(buildingURL.Store.Split(',')[1]) < udStoreLevel.Value && Convert.ToInt32(buildingURL.Store.Split(',')[1]) -3 < Convert.ToInt32(sBuilding1.Split(',')[1]) ? buildingURL.Store : sBuilding1;
                                sBuilding1 = Convert.ToInt32(buildingURL.Food.Split(',')[1]) < udFoodLevel.Value && Convert.ToInt32(buildingURL.Food.Split(',')[1]) + 2 < Convert.ToInt32(sBuilding1.Split(',')[1]) ? buildingURL.Food : sBuilding1;
                                sBuilding1 = Convert.ToInt32(buildingURL.Crystal.Split(',')[1]) < udCrystalLevel.Value && Convert.ToInt32(buildingURL.Crystal.Split(',')[1]) + 3  < Convert.ToInt32(sBuilding1.Split(',')[1]) ? buildingURL.Crystal : sBuilding1;
                                sBuilding1 = Convert.ToInt32(buildingURL.Shop.Split(',')[1]) < udShopLevel.Value && Convert.ToInt32(buildingURL.Shop.Split(',')[1]) + 3 < Convert.ToInt32(sBuilding1.Split(',')[1]) ? buildingURL.Shop : sBuilding1;

                                sBuilding2 = buildingURL.Stone != sBuilding1 && Convert.ToInt32(buildingURL.Stone.Split(',')[1]) < udStoneLevel.Value && Convert.ToInt32(buildingURL.Stone.Split(',')[1]) < Convert.ToInt32(sBuilding2.Split(',')[1]) ? buildingURL.Stone : sBuilding2;
                                sBuilding2 = buildingURL.Wood != sBuilding1 && Convert.ToInt32(buildingURL.Wood.Split(',')[1]) < udWoodLevel.Value && Convert.ToInt32(buildingURL.Wood.Split(',')[1]) < Convert.ToInt32(sBuilding2.Split(',')[1]) ? buildingURL.Wood : sBuilding2;
                                sBuilding2 = buildingURL.Store != sBuilding1 && Convert.ToInt32(buildingURL.Store.Split(',')[1]) < udStoreLevel.Value && Convert.ToInt32(buildingURL.Store.Split(',')[1])-3 < Convert.ToInt32(sBuilding2.Split(',')[1]) ? buildingURL.Store : sBuilding2;
                                sBuilding2 = buildingURL.Food != sBuilding1 && Convert.ToInt32(buildingURL.Food.Split(',')[1]) < udFoodLevel.Value && Convert.ToInt32(buildingURL.Food.Split(',')[1]) +2 < Convert.ToInt32(sBuilding2.Split(',')[1]) ? buildingURL.Food : sBuilding2;
                                sBuilding2 = buildingURL.Crystal != sBuilding1 && Convert.ToInt32(buildingURL.Crystal.Split(',')[1]) < udCrystalLevel.Value && Convert.ToInt32(buildingURL.Crystal.Split(',')[1]) + 3 < Convert.ToInt32(sBuilding2.Split(',')[1]) ? buildingURL.Crystal : sBuilding2;
                                sBuilding2 = buildingURL.Shop != sBuilding1 && Convert.ToInt32(buildingURL.Shop.Split(',')[1]) < udShopLevel.Value && Convert.ToInt32(buildingURL.Shop.Split(',')[1]) + 3 < Convert.ToInt32(sBuilding2.Split(',')[1]) ? buildingURL.Shop : sBuilding2;

                                sBuilding1 = sBuilding1.Split(',')[0] + "," + (Convert.ToInt32(sBuilding1.Split(',')[1]) + 1).ToString();
                                sBuilding2 = sBuilding2.Split(',')[0] + "," + (Convert.ToInt32(sBuilding2.Split(',')[1]) + 1).ToString();
                            }
                            else
                            {
                                iOperateIndex = alOperate.Count - 1;
                            }
                            timerOperate.Enabled = true;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            tbWorkLogs.Text += buildingURL.Stone + "\r\n";
                            tbWorkLogs.Text += buildingURL.Wood + "\r\n";
                            tbWorkLogs.Text += buildingURL.Crystal + "\r\n";
                            tbWorkLogs.Text += buildingURL.Food + "\r\n";
                            tbWorkLogs.Text += buildingURL.Shop + "\r\n";
                            tbWorkLogs.Text += buildingURL.Store + "\r\n";
                            tbWorkLogs.Text += sBuilding1 + "\r\n";
                            tbWorkLogs.Text += sBuilding2 + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        break;
                    case "NPC判断":
                        #region NPC判断
                        try
                        {
                            int iStoreMax = Convert.ToInt32(userInfo.Store.Split('/')[1]);//最大仓库容量
                            int iStoreNow = Convert.ToInt32(userInfo.Store.Split('/')[0]); //当前仓库容量
                            int iHours = DateTime.Now.Hour; //当前的小时数
                            int iGoldAmount = Convert.ToInt32(userInfo.Gold.Split('/')[0]); //金币数
                            //选中了清仓，并且在清仓时段里面
                            if (cbClearStore.Checked&&iHours >=udClearStoreTime.Value)
                            {
                                iNPCAmount = 100000;
                            }
                            else
                            {//没有选中清仓，但是资源满了或者没钱了
                                if (iStoreMax < iStoreNow + 2500 ||iGoldAmount < 300)
                                {
                                    iNPCAmount = 50000; //卖掉部分商品
                                }
                                else
                                {
                                    iOperateIndex += 5; 
                                }
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                       // break;
                    case "挂单资源":
                        #region 挂单资源
                        try
                        {
                            workStatus.Text = "挂单资源";
                            if (userInfo.Shop.Trim() == "") //如果没有找到挂机单子的信息，直接进入下一个动作,这里可不能再用iActionIndex++了，用了就到结束去了。
                            {
                                iOperateIndex = 0;
                                timerAction.Enabled = true;
                                return;
                            }
                            else
                            {
                                iShopTotal = Convert.ToInt32(userInfo.Shop.Split('/')[0]);
                                iShopUsed = Convert.ToInt32(userInfo.Shop.Split('/')[1]);
                                iStoreTotal = Convert.ToInt32(userInfo.Store.Split('/')[0]);
                                iStoreUsed = Convert.ToInt32(userInfo.Store.Split('/')[1]);

                                if (iShopTotal <= iShopUsed)  //单子已经挂满了
                                {
                                    workStatus.Text = "单子挂满了！";
                                    iOperateIndex = 0;
                                    timerAction.Enabled = true;
                                    return;
                                }
                                else  //没有挂满
                                {
                                    int iHour = DateTime.Now.Hour;
                                    if ((iHour < 7 || iHour >= 23) && cbStopNight.Checked) //如果当前时间小于7时或者大于等于23时，不再挂单
                                    {
                                        workStatus.Text = "半夜休息。";
                                        iOperateIndex = alOperate.Count - 1;
                                        timerOperate.Enabled = true;
                                        return;
                                    }
                                    else //时间也符合要求
                                    {
                                        //挂单小时数
                                        Random rnd = new Random();
                                        int iKeepHours = Convert.ToInt32(udKeepHours.Text)-1+rnd.Next(0,6);
                                        iKeepHours = iKeepHours >= 0 ? iKeepHours : 0; //如果序号小于0了，设置为0
                                        //挑选选定的并且资源最多的那种资源
                                        int resIndex = -1;
                                        int[] resAmount = new int[4];
                                        resAmount[0] = Convert.ToInt32(userInfo.Wood.Split('/')[0]);
                                        resAmount[1] = Convert.ToInt32(userInfo.Stone.Split('/')[0]);
                                        resAmount[2] = Convert.ToInt32(userInfo.Food.Split('/')[0]);
                                        resAmount[3] = Convert.ToInt32(userInfo.Crystal.Split('/')[0]);
                                        //获取选择挂单的情况
                                        bool[] resChecked = new bool[4];
                                        resChecked[0] = cbWoodSell.Checked;
                                        resChecked[1] = cbStoneSell.Checked;
                                        resChecked[2] = cbFoodSell.Checked;
                                        resChecked[3] = cbCrystalSell.Checked;

                                        //最低数量2500
                                        int maxAmount = 2500;
                                        for (int i = 0; i < 4; i++)
                                        {  //大于2500并且选中了的
                                            if (resAmount[i] > maxAmount && resChecked[i])
                                            {
                                                maxAmount = resAmount[i];
                                                resIndex = i;
                                            }

                                        }

                                        if (resIndex >= 0)
                                        {
                                            //获取价格并且出售
                                            string[] resPrice = new string[4];

                                            //先把价格保存下来
                                            resPrice[0] = udWoodPrice.Value.ToString();
                                            resPrice[1] = udStonePrice.Value.ToString();
                                            resPrice[2] = udFoodPrice.Value.ToString();
                                            resPrice[3] = udCrystalPrice.Value.ToString();

                                            string[] resNpcPrice = WebOperate.getNpcPrice(webBrowser);
                                            //自动调整调整Npc价格，如果当前售出价低于等于 Npc 价格，调整为Npc价格+1,当前售价大于Npc价格的三倍时，调整为Npc价格的三倍。
                                            udWoodPrice.Value = Convert.ToInt32(udWoodPrice.Value) <= Convert.ToInt32(resNpcPrice[0]) ? Convert.ToInt32(resNpcPrice[0]) + 1 : udWoodPrice.Value;
                                            udWoodPrice.Value = Convert.ToInt32(udWoodPrice.Value) > Convert.ToInt32(resNpcPrice[0]) * 3 ? Convert.ToInt32(resNpcPrice[0]) * 3 : udWoodPrice.Value;

                                            udStonePrice.Value = Convert.ToInt32(udStonePrice.Value) <= Convert.ToInt32(resNpcPrice[1]) ? Convert.ToInt32(resNpcPrice[1]) + 1 : udStonePrice.Value;
                                            udStonePrice.Value = Convert.ToInt32(udStonePrice.Value) > Convert.ToInt32(resNpcPrice[1]) * 3 ? Convert.ToInt32(resNpcPrice[1]) * 3 : udStonePrice.Value;

                                            udFoodPrice.Value = Convert.ToInt32(udFoodPrice.Value) <= Convert.ToInt32(resNpcPrice[2]) ? Convert.ToInt32(resNpcPrice[2]) + 1 : udFoodPrice.Value;
                                            udFoodPrice.Value = Convert.ToInt32(udFoodPrice.Value) > Convert.ToInt32(resNpcPrice[2]) * 3 ? Convert.ToInt32(resNpcPrice[2]) * 3 : udFoodPrice.Value;


                                            udCrystalPrice.Value = Convert.ToInt32(udCrystalPrice.Value) <= Convert.ToInt32(resNpcPrice[3]) ? Convert.ToInt32(resNpcPrice[3]) + 1 : udCrystalPrice.Value;
                                            udCrystalPrice.Value = Convert.ToInt32(udCrystalPrice.Value) > Convert.ToInt32(resNpcPrice[3]) * 3 ? Convert.ToInt32(resNpcPrice[3]) * 3 : udCrystalPrice.Value;
                                            //如果有调整过的，再保存一次。
                                            if (resPrice[0] != udWoodPrice.Value.ToString() || resPrice[1] != udStonePrice.Value.ToString() || resPrice[2] != udFoodPrice.Value.ToString()||resPrice[3] != udCrystalPrice.Value.ToString())
                                            {
                                                resPrice[0] = udWoodPrice.Value.ToString();
                                                resPrice[1] = udStonePrice.Value.ToString();
                                                resPrice[2] = udFoodPrice.Value.ToString();
                                                resPrice[3] = udCrystalPrice.Value.ToString();
                                                btSaveSetting_Click(null, null);
                                            }

                                            WebOperate.putInData(webBrowser, "unitPrice", resIndex, resPrice[resIndex]);
                                            WebOperate.putInData(webBrowser, "keepHours", resIndex, iKeepHours);
                                            WebOperate.ButtonClick(webBrowser, "value", "确定", resIndex);
                                            timerOperate.Enabled = true;
                                            return;
                                        }
                                        else
                                        {
                                            workStatus.Text = "资源卖空了！";
                                            iOperateIndex = 0;
                                            timerAction.Enabled = true;
                                            return;
                                        } //end of sell resource
                                    }//end of 满足挂机时间要求
                                }//end of 单子没有挂满
                            }//end of else 获得了正确的挂单信息。
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "NPC资源":
                        #region 卖资源给NPC
                        try
                        {
                            workStatus.Text = "清空资源";
                            int iHours = DateTime.Now.Hour; //当前的小时数
                            if (iHours >= udClearStoreTime.Value)
                            {
                                int iNPCAmount0 = Convert.ToInt32(userInfo.Wood.Split('/')[0]) > 10000 ? Convert.ToInt32(userInfo.Wood.Split('/')[0]) - 10000 : 0;
                                int iNPCAmount1 = Convert.ToInt32(userInfo.Stone.Split('/')[0]) > 10000 ? Convert.ToInt32(userInfo.Stone.Split('/')[0]) - 10000 : 0;
                                int iNPCAmount2 = Convert.ToInt32(userInfo.Food.Split('/')[0]) > 10000 ? Convert.ToInt32(userInfo.Food.Split('/')[0]) - 10000 : 0;
                                int iNPCAmount3 = Convert.ToInt32(userInfo.Crystal.Split('/')[0]) > 10000 ? Convert.ToInt32(userInfo.Crystal.Split('/')[0]) - 10000 : 0;
                                WebOperate.putInData(webBrowser, "resNum", 0, iNPCAmount0.ToString());
                                WebOperate.putInData(webBrowser, "resNum", 1, iNPCAmount1.ToString());
                                WebOperate.putInData(webBrowser, "resNum", 2, iNPCAmount2.ToString());
                                WebOperate.putInData(webBrowser, "resNum", 3, iNPCAmount3.ToString());
                            }
                            else
                            {
                                WebOperate.putInData(webBrowser, "resNum", 0, iNPCAmount.ToString());
                                WebOperate.putInData(webBrowser, "resNum", 1, iNPCAmount.ToString());
                                WebOperate.putInData(webBrowser, "resNum", 2, iNPCAmount.ToString());
                                WebOperate.putInData(webBrowser, "resNum", 3, iNPCAmount.ToString());
                            }
                            WebOperate.ButtonClick(webBrowser, "value", "出售", 0);
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "跳转":
                        #region 跳转到某个步骤
                        try
                        {
                            if (sParamString == "LAST")
                                iOperateIndex = alOperate.Count - 1;
                            else
                            {
                                iOperateIndex = iOperateIndex+Convert.ToInt32(sParamString);
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "超链接":
                        #region 超链接
                        try
                        {
                            string sHrefText = sParamString.Split(',')[0];
                            string sHrefOrder = sParamString.Split(',')[1];
                            WebOperate.herfClick(webBrowser.Document, sHrefText, Convert.ToInt32(sHrefOrder));
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //    break;
                    case "雇佣英雄":
                        #region 雇佣英雄
                        try
                        {
                            workStatus.Text = "雇佣英雄";
                            int iHeroOrder = WebOperate.getGoodHeroOrder(webBrowser);
                            if (WebOperate.herfClick(webBrowser.Document, "雇佣", iHeroOrder) == false) //如果没有点到雇佣按钮，就不需要对话框确定了。
                            {
                                workStatus.Text = "雇佣失败";
                                iOperateIndex += 2;
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "尝试复活":
                        #region 尝试复活
                        try
                        {
                            workStatus.Text = "尝试复活";
                            HtmlElement heHeroInfo = webBrowser.Document.GetElementById("hrInf");
                            if (heHeroInfo == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "尝试复活页面打开超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                HtmlElementCollection hec = heHeroInfo.GetElementsByTagName("A");
                                bool bIsDead = false; //英雄死亡状态
                                for (int i = 0; i < hec.Count; i++)
                                {
                                    if (hec[i].InnerText == "[复活]")
                                    {
                                        hec[i].InvokeMember("click");
                                        bIsDead = true;
                                        break;
                                    }
                                }
                                if (!bIsDead) //如果没有死的话，后面的对话就不需要了。
                                {
                                    iOperateIndex +=5;
                                }
                                hec = null;
                            }
                            heHeroInfo = null;
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    // break;
                    case "复活英雄":
                        #region 复活英雄
                        try
                        {
                            workStatus.Text = "复活英雄";
                            HtmlElement he = webBrowser.Document.GetElementById("scrInner");
                            if (he == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "复活英雄页面打开超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                HtmlElementCollection hec = webBrowser.Document.GetElementById("scrInner").GetElementsByTagName("A");
                                for (int i = 0; i < hec.Count; i++)
                                {
                                    if (hec[i].InnerText == "复活")
                                    {
                                        hec[i].InvokeMember("click");
                                        bHeroBusy = true;
                                        break;
                                    }
                                }
                                if (!bHeroBusy) //如果没有死的话，后面的对话就不需要了。
                                {
                                    iOperateIndex += 4;
                                }
                                hec = null;
                            }
                            he = null;
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    // break;
                    case "升级英雄":
                        #region 升级英雄
                        try
                        {
                            workStatus.Text = "升级英雄";
                            HtmlElement heHeroInfo = webBrowser.Document.GetElementById("hrInf");
                            if (heHeroInfo == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "升级英雄页面打开超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                WebOperate.getHeroLevel(webBrowser, dataGridView,serverInfo.ServerURL, sUserName, sSQLServerConnStr);
                                HtmlElementCollection hec = heHeroInfo.GetElementsByTagName("A");
                                for (int i = 0; i < hec.Count; i++)
                                {
                                    if (hec[i].GetElementsByTagName("IMG").Count > 0 && hec[i].GetElementsByTagName("IMG")[0].GetAttribute("title") == "升级")
                                    {
                                        hec[i].InvokeMember("click");
                                        break;
                                    }
                                }
                                hec = null;                                
                            }
                            heHeroInfo = null;
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                       // break;
                    case "属性加点":
                        #region 属性加点
                        try
                        {
                            workStatus.Text = "属性加点";
                            HtmlElement heHeroInfo = webBrowser.Document.GetElementById("hrInf");
                            if (heHeroInfo == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "属性加点页面打开超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                HtmlElementCollection hec = heHeroInfo.GetElementsByTagName("SPAN");

                                for (int i = 0; i < hec.Count; i++)
                                {   //如果类型为right，并且span内的ID属性为
                                    if (hec[i].GetAttribute("id") == "strengthPlus")
                                    {
                                        if (hec[i].Parent.GetElementsByTagName("A").Count != 0) ///有超链接，肯定能够升级
                                            hec[i].Parent.GetElementsByTagName("A")[0].InvokeMember("click");
                                        break; //退出循环
                                    }
                                }
                                hec = null;
                            }
                            heHeroInfo = null;
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "任务对话":
                        #region 任务对话
                        try
                        {
                            workStatus.Text = "任务对话"+sParamString;
                            HtmlElement he = webBrowser.Document.GetElementById("taskingContainer");
                            if (he != null && he.GetElementsByTagName("A").Count > 0 && he.GetElementsByTagName("A")[0].InnerText.Trim()==sParamString)
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                
                                if (he.GetElementsByTagName("A")[0].GetElementsByTagName("IMG")[0].GetAttribute("TITLE").ToString() == "未完成"&&sParamString!="欢迎")
                                {
                                    
                                    switch (sParamString)
                                    {
                                        case "发展农田":
                                            WebOperate.setUserMissionStep(sUserName,serverInfo.ServerURL, 1, sCurrentConnStr);
                                            break;
                                        case "发展矿产":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 2, sCurrentConnStr);
                                            break;
                                        case "游侠":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 3, sCurrentConnStr);
                                            break;
                                        case "英雄的契约":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 7, sCurrentConnStr);
                                            break;
                                        case "建造军事指挥所":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 4, sCurrentConnStr);
                                            break;
                                        case "储存资源":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 4, sCurrentConnStr);
                                            break;
                                        case "税收":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 7, sCurrentConnStr);
                                            break;
                                        case "宝物仓库":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 6, sCurrentConnStr);
                                            break;
                                        case "建造兵营":
                                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, 5, sCurrentConnStr);
                                            break;
                                        default:
                                            break;
                                    }
                                    workStatus.Text = sParamString+"任务未完成。重置步骤";
                                    tbWorkLogs.Text += sParamString + "任务未完成。重置步骤\r\n";
                                    iOperateIndex = alOperate.Count - 1; //跳到最后一步

                                }
                                else 
                                {
                                    he.GetElementsByTagName("A")[0].InvokeMember("click");
                                }

                            }
                            else
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes -= 4;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    workStatus.Text = "任务对话" + sParamString + "打不开";
                                    if (sParamString == "欢迎")
                                        iOperateIndex += 4;
                                    else
                                        iOperateIndex += 2;
                                }
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                      //  break;
                    case "对话确定":
                        #region 对话框点确定，一般使用
                        try
                        {
                           
                            HtmlElement he = webBrowser.Document.GetElementById("simWinPop");
                            if (he == null || he.GetElementsByTagName("INPUT").Count == 0)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "对话确定查找确定按钮超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                                                 
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                he.GetElementsByTagName("INPUT")[0].InvokeMember("click");
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //    break;
                    case "任务确定":
                        #region 任务确定
                        try
                        {
                            workStatus.Text = "任务确定";
                            string sTagName = sParamString.Split(',')[0];
                            int iButtonOrder = Convert.ToInt32(sParamString.Split(',')[1]);
                            HtmlElement he = webBrowser.Document.GetElementById("simWinPop");
                            if (he == null || he.Document.Window.Frames.Count < 2 || he.Document.Window.Frames[1].Document.Body==null||he.Document.Window.Frames[1].Document.Body.GetElementsByTagName(sTagName).Count < iButtonOrder + 1)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "任务确定超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    iOperateIndex += 2;
                                    workStatus.Text = "任务确定失败";
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                he.Document.Window.Frames[1].Document.Body.GetElementsByTagName(sTagName)[iButtonOrder].InvokeMember("click");
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "建造":
                        #region 建造
                        try
                        {
                            workStatus.Text = "建造"+sParamString;
                            HtmlElementCollection hecTemp = WebOperate.getBuildButton(webBrowser, sParamString);
                            if (hecTemp != null)
                            {
                                if (hecTemp.Count > 0)
                                    hecTemp[0].InvokeMember("click");
                                else //没有确定按钮
                                {
                                    iOperateIndex = alOperate.Count - 1; //到最后一个
                                    WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, iMissionStep - 1, sCurrentConnStr);
                                    workStatus.Text = sParamString + "建设条件不足";
                                    tbWorkLogs.Text += sParamString + "建设条件不足,返回上一步。\r\n";
                                }
                            }
                            else
                            { //列表中找不到这个建筑
                                tbWorkLogs.Text += sParamString + "已经建设\r\n";
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                     //   break;
                    case "升级建筑":
                        #region 升级建筑,需要再次检查建筑的等级是否达到了目标
                        try
                        {
                            //获取建筑的等级
                            workStatus.Text = "升级"+sParamString;
                            string sBuildingstring = "";
                            string sBuildingName = sParamString.Split(',')[0];//建筑的URL地址
                            string sBuildingLevel = sParamString.Split(',')[1]; //要建筑的目标等级
                            //根据名称获取建筑的URL
                            sBuildingstring = sBuildingName == "农场" ? buildingURL.Food : sBuildingstring;
                            sBuildingstring = sBuildingName == "伐木场" ? buildingURL.Wood : sBuildingstring;
                            sBuildingstring = sBuildingName == "水晶矿" ? buildingURL.Crystal : sBuildingstring;
                            sBuildingstring = sBuildingName == "石头矿" ? buildingURL.Stone : sBuildingstring;
                            sBuildingstring = sBuildingName == "市场" ? buildingURL.Shop : sBuildingstring;
                            sBuildingstring = sBuildingName == "仓库" ? buildingURL.Store : sBuildingstring;

                            if (sBuildingstring != null && sBuildingstring != "")
                            {
                                string sBuildingCurrentLevel = sBuildingstring.Split(',')[1];
                                if (sBuildingCurrentLevel == "" || Convert.ToInt32(sBuildingCurrentLevel) < Convert.ToInt32(sBuildingLevel))
                                {
                                    WebOperate.ButtonClick(webBrowser, "value", "建造", 0);
                                }
                                else
                                {
                                    workStatus.Text = sBuildingName + "已经达到" + sBuildingLevel + "级";
                                }
                            }
                            else
                            {
                                workStatus.Text = sBuildingName + "没有在地图上找到。";
                                tbWorkLogs.Text += sUserName + sBuildingName + "没有在地图上找到。\r\n";
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //    break;
                    case "升级确定":
                        #region 升级建筑,不检查是否达到目标，直接按建造
                        try
                        {
                            WebOperate.ButtonClick(webBrowser, "value", "建造", 0);   
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //    break;
                    case "英雄检查":
                        #region 英雄检查
                        try
                        {
                            if (bHeroBusy == true) //英雄正在忙着呢
                            {
                                iOperateIndex = alOperate.Count - 1;
                                workStatus.Text = "英雄繁忙";
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                      //  break;
                    case "出征":
                        #region 英雄出征
                        try
                        {
                            workStatus.Text = "出征"+sParamString;
                            Random rnd = new Random();
                            HtmlElementCollection hec = webBrowser.Document.GetElementById("mapHot").GetElementsByTagName("AREA");
                            //热区个数不等于81或者地图还没有更新
                            if (hec.Count != 81 || webBrowser.Document.GetElementById("mapArs").GetElementsByTagName("DIV")[0].GetAttribute("id") == sLastMapID || iReadTimes < 0)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {

                                 //   tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                 //   tbWorkLogs.Text += "出征尝试" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    iMapX = 0;//重置起始坐标
                                    iMapY = 0;
                                    iOperateIndex += 6;//跳到本项出征的最后
                                    workStatus.Text = "出征"+sParamString+"失败";
                                }
                            }
                            else
                            {
                                //查找是否有出征对象
                                sLastMapID = webBrowser.Document.GetElementById("mapArs").GetElementsByTagName("DIV")[0].GetAttribute("id"); //标记下地图的最上角位置
                                string[] sAllPosition = WebOperate.getVisitPosition(webBrowser, sParamString);
                                if ((iMapX == 0 && iMapY == 0) || sAllPosition == null) //没有找到对象，或者还在1屏,就开始探索地图
                                {
                                    HtmlElementCollection hecDirect = webBrowser.Document.All.GetElementsByName("drtGo")[0].GetElementsByTagName("AREA");// 获得8个方向按键
                                    bool bFind = false;
                                    int iDirectIndex; //生成的随机方向的序号
                                    int MoveX;//X位移
                                    int MoveY;//Y位移
                                    double AttackRange;
                                    while (!bFind)
                                    {
                                        iDirectIndex = rnd.Next(0, sMapDirect.Length);
                                        MoveX = Convert.ToInt32(sMapDirect[iDirectIndex].Split(',')[0]);//X坐标移动
                                        MoveY = Convert.ToInt32(sMapDirect[iDirectIndex].Split(',')[1]);//Y坐标移动
                                        AttackRange = Math.Sqrt(Math.Pow(iMapX + MoveX, 2) + Math.Pow(iMapY + MoveY, 2));
                                        if (AttackRange < (double)udAttackRange.Value && AttackRange != 0)
                                        {
                                            bFind = true;
                                            iMapX = iMapX + MoveX;
                                            iMapY = iMapY + MoveY;
                                            iReadTimes-=5;
                                            workStatus.Text = "查找方向" + sMapDirect[iDirectIndex] + ",还有" + iReadTimes + "次";
                                            iOperateIndex -= 2;
                                            hecDirect[iDirectIndex].InvokeMember("click");
                                        }
                                    }
                                    hecDirect = null;
                                }
                                else //如果找到一个目标，就点击它。
                                {
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    int iIndex = rnd.Next(0, sAllPosition.Length);
                                    HtmlElementCollection hecAttack = webBrowser.Document.GetElementById("mapHot").GetElementsByTagName("AREA");
                                    for (int i = 0; i < hec.Count; i++)
                                    {
                                        if (hecAttack[i].GetAttribute("y").ToString() == sAllPosition[iIndex].ToString().Split(',')[0] && hecAttack[i].GetAttribute("x").ToString() == sAllPosition[iIndex].ToString().Split(',')[1])
                                        {
                                            iMapX = 0;//重置起始坐标
                                            iMapY = 0;
                                            hecAttack[i].InvokeMember("click"); //点击攻击
                                            break;
                                        }
                                    }
                                    hecAttack = null;
                                }//end of 打开地图后的目标搜索
                            }//end of 正常打开地图的操作
                            hec = null;
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    //   break;
                    case "出征模式":
                        #region 出征模式
                        try
                        {
                            HtmlElement he = webBrowser.Document.GetElementById("simWinPop");
                            if (he == null || he.Document.Window.Frames.Count < 2 || he.Document.Window.Frames[1].Document.Body == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "任务确定超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    iOperateIndex += 2;
                                    workStatus.Text = "任务确定失败";
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                string sHerfText = "出军";//超链接默认为出军，恶魔城
                                sHerfText = sParamString == "打野" ? "狩猎" : sHerfText;
                                sHerfText = sParamString == "水车" ? "访问" : sHerfText;
                                sHerfText = sParamString == "风车" ? "访问" : sHerfText;
                                WebOperate.herfClick(he.Document.Window.Frames[1].Document, sHerfText, 1);
                               
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                         
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                       // break;
                    case "英雄选择":
                        #region 英雄选择
                        try
                        {
                            HtmlElement he = webBrowser.Document.GetElementById("simWinPop");
                            if (he == null || he.Document.Window.Frames.Count < 2 || he.Document.Window.Frames[1].Document.Body == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "英雄选择超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    iOperateIndex = alOperate.Count - 1;
                                    workStatus.Text = "英雄选择失败";
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                string sNoHeroTip="出军(调兵)\r\n城内没有英雄，您不能出兵!";
                                string sBodyText=he.Document.Window.Frames[1].Document.Body.InnerText.Trim();
                                if (sBodyText == sNoHeroTip) //如果出现没有英雄的提示，就退出来
                                {
                                    iOperateIndex += 2;
                                    bHeroBusy = true;
                                }
                                else
                                {
                                    HtmlElementCollection hec = he.Document.Window.Frames[1].Document.Body.GetElementsByTagName("INPUT");
                                    //查找英雄选择框
                                    for (int i = 0; i < hec.Count; i++)
                                    {
                                        if (hec[i].GetAttribute("type") == "checkbox" && hec[i].GetAttribute("name") == "heroId")
                                        {
                                            hec[i].InvokeMember("click");
                                            break;
                                        }
                                    }
                                    //查找确定按钮
                                    for (int i = 0; i < hec.Count; i++)
                                    {
                                        if (hec[i].GetAttribute("type") == "submit" && hec[i].GetAttribute("value") == "确定")
                                        {
                                            hec[i].InvokeMember("click");
                                            break;
                                        }
                                    }
                                }
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        // break;
                    case "出征确定":
                        #region 英雄选择
                        try
                        {
                            HtmlElement he = webBrowser.Document.GetElementById("simWinPop");
                            if (he == null || he.Document.Window.Frames.Count < 2 || he.Document.Window.Frames[1].Document.Body == null)
                            {
                                if (iReadTimes > 0)
                                {
                                    iReadTimes--;
                                    iOperateIndex = iOperateIndex - 2;//返回上一步再次暂停，
                                }
                                else
                                {
                                    tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                                    tbWorkLogs.Text += "出征确定超过" + udReadTimes.Value.ToString() + "次.\r\n";
                                    iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                    iOperateIndex += 2;
                                    workStatus.Text = "出征确定失败";
                                }
                            }
                            else
                            {
                                iReadTimes = (int)udReadTimes.Value; //再次初始化数值备用
                                HtmlElementCollection hec = he.Document.Window.Frames[1].Document.Body.GetElementsByTagName("INPUT");
                                //查找确定按钮
                                for (int i = 0; i < hec.Count; i++)
                                {
                                    if (hec[i].GetAttribute("type") == "submit" && hec[i].GetAttribute("value") == "确定")
                                    {
                                        hec[i].InvokeMember("click");
                                        bHeroBusy = true; //英雄已经在忙或者挂掉了
                                        break;
                                    }
                                }
                            }
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                        // break;
                    case "下个步骤":
                        #region 设置下个步骤的序号
                        try
                        {
                            WebOperate.setUserMissionStep(sUserName, serverInfo.ServerURL, iMissionStep + 1, sCurrentConnStr);
                            timerOperate.Enabled = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                            tbWorkLogs.Text += sUserName + sCurrentOperate + ex.Message + ex.Data + "\r\n";
                            throw new Exception(sUserName + sCurrentOperate + ex.Message + ex.Data);
                        }
                        #endregion
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += DateTime.Now.ToString() + "\r\n";
                tbWorkLogs.Text += sCurrentOperate + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 浏览器加载完成的时候
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                if (bDocumentLoaded == false)
                {
                    bDocumentLoaded = true;
                    timerOperate.Enabled = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "DocumentCompleted" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 基础计时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                timerWait.Enabled = false;
                timerOperate.Enabled = true;
                return;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "timer_Tick" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btRegister_Click(object sender, EventArgs e)
        {
            try
            {
                udOperateTimeOut.Value = 10; //设置超时为10秒
                alAction.Clear();
                alAction.Add("注册");
                iActionIndex = 0;

                timerAction.Enabled = true;
                return;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "btRegister" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 窗口加载的时候
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainForm_Load(object sender, EventArgs e)
        {
            sCurrentConnStr = sSQLServerConnStr;
            WebOperate.getServerList(ComboBoxServer, sCurrentConnStr); //获得服务器列表
            ComboBoxAutoStyle.SelectedIndex = 0;
            ComboBoxSearch.SelectedIndex = 0;
            iAccountTimes = 1; //当前开始的
            this.Text = this.Text + sVersion;
        }
        /// <summary>
        /// 服务器列表选择的时候激发
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                WebOperate.getSectionList(ComboBoxSection, ComboBoxServer.SelectedItem.ToString(), sCurrentConnStr);
                serverInfo = WebOperate.getAccountList(ComboBoxAccount,dataGridView, ComboBoxServer.SelectedItem.ToString(),ComboBoxSection.SelectedItem.ToString(), sCurrentConnStr,999,"全部","0","99999999"); //获取所有用户名
                loadSetting();//载入设置
                this.Text = ComboBoxServer.SelectedItem.ToString() + "-" + ComboBoxSection.SelectedItem.ToString()+sVersion;
              
            }
            
            catch (Exception ex)
            {
                tbWorkLogs.Text += "ComboBoxServerSelectedIndexChanged" + ex.Message + ex.Data + "\r\n";
            }
          
        }
        /// <summary>
        /// 进入用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btEnterUser_Click(object sender, EventArgs e)
        {
            try
            {
                btStartAuto.Text = "开始";
                alAction.Clear();
                alAction.Add("注销");
                alAction.Add("登录");
                alAction.Add("结束");
                iActionIndex = 0;
                timerOut.Enabled = true; //启动计数器
                iOperateTimeOut = (int)udOperateTimeOut.Value;
                iReadTimes = (int)udReadTimes.Value;
                timerAction.Enabled = true;
                return;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "EnterUser" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 下一个用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btNextUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (ComboBoxAccount.SelectedIndex + 1 >= ComboBoxAccount.Items.Count)
                {
                    ComboBoxAccount.SelectedIndex = 0;
                    iAccountTimes++;
                }
                else
                    ComboBoxAccount.SelectedIndex++;
                btEnterUser_Click(null, null);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "NextUser" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 上一个用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btLastUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (ComboBoxAccount.SelectedIndex <= 0)
                {
                    ComboBoxAccount.SelectedIndex = ComboBoxAccount.Items.Count - 1;
                    iAccountTimes++;
                }
                else
                    ComboBoxAccount.SelectedIndex--;

                btEnterUser_Click(null, null);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "LastUser" + ex.Message + ex.Data + "\r\n";
            }
        }
        #region 设置快捷键（热键，F1下一个）
        //窗口激活的时候
        private void mainForm_Activated(object sender, EventArgs e)
        {
            try
            {
                HotKey.RegisterHotKey(Handle, 100, HotKey.KeyModifiers.None, Keys.F1); //F1下一个账户
                HotKey.RegisterHotKey(Handle, 101, HotKey.KeyModifiers.None, Keys.F2); //F2开放账户
                HotKey.RegisterHotKey(Handle, 102, HotKey.KeyModifiers.None, Keys.F3); //注册
                HotKey.RegisterHotKey(Handle, 103, HotKey.KeyModifiers.Shift, Keys.Enter); //回车下一步
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "mainForm_Activated" + ex.Message + ex.Data + "\r\n";
            }
        }
        //窗口不再激活的时候
        private void mainForm_Deactivate(object sender, EventArgs e)
        {
            try
            {
                HotKey.UnregisterHotKey(Handle, 100);
                HotKey.UnregisterHotKey(Handle, 101);
                HotKey.UnregisterHotKey(Handle, 102);
                HotKey.UnregisterHotKey(Handle, 103);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "mainForm_Deactivate" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// 重载WndProc方法，用于实现热键响应
        protected override void WndProc(ref Message m)
        {
            try
            {
                const int WM_HOTKEY = 0x0312;//如果m.Msg的值为0x0312那么表示用户按下了热键
                //按快捷键 
                switch (m.Msg)
                {
                    case WM_HOTKEY:
                        switch (m.WParam.ToInt32())
                        {
                            case 100:    //按下的是F1 //下一个账户
                                btNextUser_Click(null, null);
                                break;
                            case 101://按下的是F2开锁
                                btUnLockUser_Click(null, null);
                                break;
                            case 102:
                                btRegister_Click(null, null);
                                break;
                            case 103:
                                regNext();
                                break;
                            default:
                                break;
                        }
                        break;
                }
                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "WndProc" + ex.Message + ex.Data + "\r\n";
            }
        }
        //注册的时候点下一步
        private void regNext()
        {
            if (webBrowser.Url.ToString() == "http://register.ourgame.com/regist/register_webgame.asp?from=MMOG6027") //联众注册页面
            {
                webBrowser.Document.InvokeScript("register_onsubmit");
            }
        }
        #endregion
        private void timerOut_Tick(object sender, EventArgs e)
        {
            try
            {
                iOperateTimeOut--;
                timeOutStatus.Text = iOperateTimeOut + "秒后超时，";
                if (iOperateTimeOut <= 0)
                {
                    //如果超时了，重置动作。进入下一个用户
                    iActionIndex = 0;
                    loadSetting();
                    if (ComboBoxAccount.SelectedIndex + 1 >= ComboBoxAccount.Items.Count)
                    {
                        ComboBoxAccount.SelectedIndex = 0;
                        iAccountTimes++;
                        tbWorkLogs.Text += "第" + iAccountTimes.ToString() + "次，" + DateTime.Now.ToString() + "\r\n";
                    }
                    else
                        ComboBoxAccount.SelectedIndex++;
                    timerAction.Enabled = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "timerOut_Tick" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 清空信息列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btClearWorkLogs_Click(object sender, EventArgs e)
        {
            tbWorkLogs.Clear();
        }
        /// <summary>
        /// 获取用户信息并且写入数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btGetUserInfo_Click(object sender, EventArgs e)
        {
            try
            {
                userInfo = WebOperate.getUserInfo(webBrowser, sUserName);
                getUserInfo();
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "btGetUserInfo" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 获取用户信息
        /// </summary>
        private void getUserInfo()
        {
            try
            {
                resStatus.Text = "账号：" + userInfo.UserName + "，金币：" + userInfo.Gold + "，木头：" + userInfo.Wood + "，石头：" + userInfo.Stone + "，水晶：" + userInfo.Crystal + "，粮食：" + userInfo.Food + "，仓库：" + userInfo.Store + "，挂单数：" + userInfo.Shop;
                //如果能够读取到金币等信息的数量，就更新。
                if (userInfo.Gold != "")
                {
                    int iRowCount = dataGridView.Rows.Count;
                    bool isExist = false;
                    for (int i = 0; i < iRowCount - 1; i++)
                    {
                        if (dataGridView.Rows[i].Cells["账户"].Value.ToString() == userInfo.UserName)
                        {  //如果已经存在账户信息了，就更新。
                            isExist = true;
                            dataGridView.Rows[i].Cells["金币"].Value = Convert.ToInt32(userInfo.Gold.Split('/')[0]);
                            dataGridView.Rows[i].Cells["钻石"].Value = userInfo.Diamond;
                            dataGridView.Rows[i].Cells["仓库"].Value = userInfo.Store;
                            dataGridView.Rows[i].Cells["木材"].Value = userInfo.Wood;
                            dataGridView.Rows[i].Cells["石头"].Value = userInfo.Stone;
                            dataGridView.Rows[i].Cells["水晶"].Value = userInfo.Crystal;
                            dataGridView.Rows[i].Cells["食物"].Value = userInfo.Food;
                            dataGridView.Rows[i].Cells["任务"].Value = iMissionStep;
                            dataGridView.Rows[i].Cells["挂单"].Value = userInfo.Shop;

                            dataGridView.Rows[i].Cells["时间"].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

                            WebOperate.updateUserInfo(userInfo, sSQLServerConnStr, serverInfo.ServerURL);
                            break;
                        }
                    }
                    if (!isExist)
                    {  //如果不存在，就添加一条信息
                        dataGridView.Rows.Add(iUserOrder, userInfo.UserName, 0, userInfo.Shop, iMissionStep, Convert.ToInt32(userInfo.Gold.Split('/')[0]), userInfo.Store, userInfo.Wood, userInfo.Stone, userInfo.Crystal, userInfo.Food, DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                    }                    
                    iRowCount = dataGridView.Rows.Count;
                    //计算金币总数
                    int goldTotal = 0;
                    for (int i = 0; i < iRowCount - 1; i++)
                    {
                        goldTotal += Convert.ToInt32(dataGridView.Rows[i].Cells["金币"].Value.ToString().Split('/')[0]);
                    }
                    goldStatus.Text = "总金币：" + goldTotal.ToString() + "(" + (dataGridView.Rows.Count - 1).ToString() + ")";
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "getUserInfo" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btDeleteAccount_Click(object sender, EventArgs e)
        {
            try
            {

                string sUserToDelete = ComboBoxAccount.SelectedItem.ToString().Split(',')[0];
                if (MessageBox.Show("将账号" + sUserToDelete + "设置为封号么？", "设置确认", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    ComboBoxAccount.SelectedIndex = 0;
                    WebOperate.setLockStatus(serverInfo.ServerURL, sUserName, "2", sCurrentConnStr);   
                }
            }

            catch (Exception ex)
            {
                tbWorkLogs.Text += "DeleteAccount" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 数据表格单元单击的逝世后
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //如果单击某一行中的数据，就登录这个账号。
            try
            {
                string sSelectedUser = dataGridView.Rows[e.RowIndex].Cells["账户"].Value.ToString();

                for (int i = 0; i < ComboBoxAccount.Items.Count; i++)
                {
                    if (sSelectedUser == ComboBoxAccount.Items[i].ToString().Split(',')[0])
                    {
                        ComboBoxAccount.SelectedIndex = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                 tbWorkLogs.Text+="CellDoubleClick" + ex.Message + ex.Data+"\r\n";
            }
        }
        /// <summary>
        /// 操作的计时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerOperate_Tick(object sender, EventArgs e)
        {
            NextOperate();
        }
        /// <summary>
        /// 动作的计时
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerAction_Tick(object sender, EventArgs e)
        {
            NextAction();
        }
        /// <summary>
        /// 用正则表达式获取代理列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btGetProxy_Click(object sender, EventArgs e)
        {

            tbProxyList.Clear();
            HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

            string sHtml = doc.documentElement.outerHTML;
            string[] sProxyString=WebOperate.getStringsByReg(sHtml, tbProxyReg.Text, tbProxyPortSplit.Text, tbProxyProtocolSplit.Text);
            if (sProxyString != null)
            {
                for (int i = 0; i < sProxyString.Length; i++)
                    tbProxyList.Text += sProxyString[i] + "\r\n";
            }
        }
        /// <summary>
        /// 打开指定网页
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btProxyWeb_Click(object sender, EventArgs e)
        {
            webBrowser.Navigate(tbProxyURL.Text);
        }
        /// <summary>
        /// 复制所有代理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btProxyCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetData(DataFormats.Text, tbProxyList.Text);
        }
        /// <summary>
        /// 区域选择变化时候
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                WebOperate.getAccountList(ComboBoxAccount, dataGridView, ComboBoxServer.SelectedItem.ToString(), ComboBoxSection.SelectedItem.ToString(), sCurrentConnStr, 999, "全部", "0", "99999999"); //获取所有用户名
                this.Text = ComboBoxServer.SelectedItem.ToString() + "-" + ComboBoxSection.SelectedItem.ToString() + sVersion;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "ComboBoxSectionSelectedIndexChanged" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 全部增加价格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btAddPrice_Click(object sender, EventArgs e)
        {
            try
            {
                udStonePrice.Value = (udStonePrice.Value + 1) < udStonePrice.Maximum ? udStonePrice.Value + 1 : udStonePrice.Maximum;
                udFoodPrice.Value = (udFoodPrice.Value + 1) < udFoodPrice.Maximum ? udFoodPrice.Value + 1 : udFoodPrice.Maximum;
                udWoodPrice.Value = (udWoodPrice.Value + 1) < udWoodPrice.Maximum ? udWoodPrice.Value + 1 : udWoodPrice.Maximum;
                udCrystalPrice.Value = (udCrystalPrice.Value + 1) < udCrystalPrice.Maximum ? udCrystalPrice.Value + 1 : udCrystalPrice.Maximum;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "btAddPrice" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 全部减少价格
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSubPrice_Click(object sender, EventArgs e)
        {
            try
            {
                udStonePrice.Value = (udStonePrice.Value - 1) > udStonePrice.Minimum ? udStonePrice.Value - 1 : udStonePrice.Minimum;
                udFoodPrice.Value = (udFoodPrice.Value - 1) > udFoodPrice.Minimum ? udFoodPrice.Value - 1 : udFoodPrice.Minimum;
                udWoodPrice.Value = (udWoodPrice.Value - 1) > udWoodPrice.Minimum ? udWoodPrice.Value - 1 : udWoodPrice.Minimum;
                udCrystalPrice.Value = (udCrystalPrice.Value - 1) > udCrystalPrice.Minimum ? udCrystalPrice.Value - 1 : udCrystalPrice.Minimum;
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "btSubPrice" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 本地数据库选择
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbLocalDB_Click(object sender, EventArgs e)
        {
            try
            {
                sCurrentConnStr = sAccessConnStr;
                ComboBoxAccount.Items.Clear();
                ComboBoxSection.Items.Clear();
                dataGridView.Rows.Clear();
                WebOperate.getServerList(ComboBoxServer, sCurrentConnStr); //获得服务器列表
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "rbLocalDB" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 更改链接的数据库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rbRemoteDB_Click(object sender, EventArgs e)
        {
            try
            {
                sCurrentConnStr = sSQLServerConnStr;
                ComboBoxAccount.Items.Clear();
                ComboBoxSection.Items.Clear();
                dataGridView.Rows.Clear();
                WebOperate.getServerList(ComboBoxServer, sCurrentConnStr); //获得服务器列表
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "rbRemoteDB" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 清空仓库
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btClearStore_Click(object sender, EventArgs e)
        {
            try
            {
                string sNpcAmount = "1000000";
                WebOperate.putInData(webBrowser, "resNum", 0, sNpcAmount);
                WebOperate.putInData(webBrowser, "resNum", 1, sNpcAmount);
                WebOperate.putInData(webBrowser, "resNum", 2, sNpcAmount);
                WebOperate.putInData(webBrowser, "resNum", 3, sNpcAmount);
                WebOperate.ButtonClick(webBrowser, "value", "出售", 0);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "btClearStore" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 载入服务器配置
        /// </summary>
        public void loadSetting()
        {
            try
            {
                if (ComboBoxServer.SelectedIndex >= 0)
                {
                    ServerSetting serverSetting = WebOperate.getServerSetting(ComboBoxServer.SelectedItem.ToString(),sSQLServerConnStr);
                    udCrystalLevel.Value = Convert.ToDecimal(serverSetting.CrystalLevel);
                    udCrystalPrice.Value = Convert.ToDecimal(serverSetting.CrystalPrice);
                    udFoodLevel.Value = Convert.ToDecimal(serverSetting.FoodLevel);
                    udFoodPrice.Value = Convert.ToDecimal(serverSetting.FoodPrice);
                    udStoneLevel.Value = Convert.ToDecimal(serverSetting.StoneLevel);
                    udStonePrice.Value = Convert.ToDecimal(serverSetting.StonePrice);
                    udWoodLevel.Value = Convert.ToDecimal(serverSetting.WoodLevel);
                    udWoodPrice.Value = Convert.ToDecimal(serverSetting.WoodPrice);
                    udStoreLevel.Value = Convert.ToDecimal(serverSetting.StoreLevel);
                    udShopLevel.Value = Convert.ToDecimal(serverSetting.ShopLevel);
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "loadSetting" + ex.Message + ex.Data + "\r\n";
            }

        }
        /// <summary>
        /// 保存设置到服务器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btSaveSetting_Click(object sender, EventArgs e)
        {
            try
            {
                ServerSetting serverSetting = new ServerSetting();
                serverSetting.CrystalLevel = udCrystalLevel.Value.ToString();
                serverSetting.CrystalPrice = udCrystalPrice.Value.ToString();
                serverSetting.FoodLevel = udFoodLevel.Value.ToString();
                serverSetting.FoodPrice = udFoodPrice.Value.ToString();
                serverSetting.StoneLevel = udStoneLevel.Value.ToString();
                serverSetting.StonePrice = udStonePrice.Value.ToString();
                serverSetting.WoodLevel = udWoodLevel.Value.ToString();
                serverSetting.WoodPrice = udWoodPrice.Value.ToString();
                serverSetting.StoreLevel = udStoreLevel.Value.ToString();
                serverSetting.ShopLevel = udShopLevel.Value.ToString();
                WebOperate.saveSetting(ComboBoxServer.SelectedItem.ToString(), serverSetting, sSQLServerConnStr);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "loadSetting" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 挂机模式更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxAutoStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (ComboBoxAutoStyle.SelectedItem.ToString())
                {
                    case "任务":
                        cbAutoMission.Checked = true; //自动任务 
                        cbAutoGov.Checked = false; //内政
                        cbAutoBuild.Checked = false;  //建造
                        cbAutoSell.Checked = false; //挂单
                        cbAutoHunt.Checked = false; //出征
                        break;
                    case "发展":
                        cbAutoMission.Checked = false; //自动任务

                        cbAutoGov.Checked = true; //内政
                        cbAutoHero.Checked = true;//英雄
                        cbAutoTax.Checked = true;//税收
                        cbFullBuildStore.Checked = true; //扩建胀库
                        cbBuyRes.Checked = true; //购买资源

                        cbAutoBuild.Checked = true; //建造
                        cbUseDiamond.Checked = true; //使用礼券
                        cbWoodUpdate.Checked = true; //木头
                        cbStoneUpdate.Checked = true; //石头
                        cbCrystalUpdate.Checked = true;//水晶
                        cbFoodUpdate.Checked = true;//食物
                        cbStoreUpdate.Checked = true;//仓库
                        cbShopUpdate.Checked = true;//市场

                        cbAutoSell.Checked = false; //挂单

                        cbAutoHunt.Checked = true;//出征
                        cbHell.Checked = false; //恶魔城
                        cbWind.Checked = false;  //风车
                        cbWater.Checked = false;  //水车
                        cbWild.Checked = true; //打野
                        break;
                    case "建造":
                        cbAutoMission.Checked = false; //自动任务

                        cbAutoGov.Checked = true; //内政
                        cbAutoHero.Checked = false;//英雄
                        cbAutoTax.Checked = true;//税收
                        cbFullBuildStore.Checked = true; //扩建胀库
                        cbBuyRes.Checked = true; //购买资源

                        cbAutoBuild.Checked = true; //建造
                        cbUseDiamond.Checked = true; //使用礼券
                        cbWoodUpdate.Checked = true; //木头
                        cbStoneUpdate.Checked = true; //石头
                        cbCrystalUpdate.Checked = true;//水晶
                        cbFoodUpdate.Checked = true;//食物
                        cbStoreUpdate.Checked = true;//仓库
                        cbShopUpdate.Checked = true;//市场

                        cbAutoSell.Checked = false; //挂单

                        cbAutoHunt.Checked = false;//出征                     
                        break;
                    case "挂单":
                        cbAutoMission.Checked = false; //自动任务

                        cbAutoGov.Checked = false; //内政

                        cbAutoBuild.Checked = false; //建造

                        cbAutoSell.Checked = true; //挂单
                        cbClearStore.Checked = true;//清粮食、水晶
                        cbStoneSell.Checked = true;//石头
                        cbWoodSell.Checked = true;//木头
                        cbCrystalSell.Checked = true;//水晶
                        cbFoodSell.Checked = true;//食物

                        cbAutoHunt.Checked = false;//出征
                        break;
                    case "常规":
                        cbAutoMission.Checked = false;//任务

                        cbAutoGov.Checked = true; //内政
                        cbAutoHero.Checked = true;//英雄
                        cbAutoTax.Checked = true;//税收
                        cbFullBuildStore.Checked = true; //扩建仓库
                        cbBuyRes.Checked = false; //购买资源

                        cbAutoBuild.Checked = true; //建造
                        cbUseDiamond.Checked = true; //使用礼券
                        cbWoodUpdate.Checked = true; //木头
                        cbStoneUpdate.Checked = true; //石头
                        cbCrystalUpdate.Checked = true;//水晶
                        cbFoodUpdate.Checked = true;//食物
                        cbStoreUpdate.Checked = true;//仓库
                        cbShopUpdate.Checked = true;//市场

                        cbAutoSell.Checked = true; //挂单

                        cbClearStore.Checked = true;//清粮食、水晶
                        cbStoneSell.Checked = true;//石头
                        cbWoodSell.Checked = true;//木头
                        cbCrystalSell.Checked = true;//水晶
                        cbFoodSell.Checked = true;//食物
                       
                        cbAutoHunt.Checked = true;//出征
                        cbHell.Checked = false; //恶魔城
                        cbWind.Checked = true;  //风车
                        cbWater.Checked = true;  //水车
                        cbWild.Checked = false; //打野
                        break;
                    default:
                        break;
                }
                //如果当前正在运行的话，点击两次重置
                if (btStartAuto.Text == "结束")
                {
                    btStartAuto_Click(null, null);
                    btStartAuto_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "StyleChange" + ex.Message + ex.Data + "\r\n";
            }
        }
        /// <summary>
        /// 已经开锁了，设置属性
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        private void btUnLockUser_Click(object sender, EventArgs e)
        {
            WebOperate.setLockStatus(serverInfo.ServerURL,sUserName, "0", sCurrentConnStr);                  
        }

        private void webBrowser_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            progressBar.Value = Convert.ToInt32(100*(e.CurrentProgress / e.MaximumProgress));
        }

        private void tbWorkLogs_TextChanged(object sender, EventArgs e)
        {
            tbWorkLogs.SelectionStart = tbWorkLogs.Text.Length;
            tbWorkLogs.ScrollToCaret();
        }

        private void brRemoteDatabase_Click(object sender, EventArgs e)
        {
            sCurrentConnStr = sSQLServerConnStr;
        }

        private void rbLocalDatabase_Click(object sender, EventArgs e)
        {
            sCurrentConnStr = sAccessConnStr2;
        }

        private void btSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string sSearchType = ComboBoxSearch.SelectedItem.ToString();
                string sGoldMin = cbGoldMin.Checked ? tbGoldMin.Text : "0";
                string sGoldMax = cbGoldMax.Checked ? tbGoldMax.Text : "99999999";
                WebOperate.getAccountList(ComboBoxAccount, dataGridView, ComboBoxServer.SelectedItem.ToString(),"", sCurrentConnStr, 999, sSearchType, sGoldMin, sGoldMax);
            }
            catch (Exception ex)
            {
                tbWorkLogs.Text += "DeleteAccount" + ex.Message + ex.Data + "\r\n";
            }
        }
        //用以替代newWindow函数
        private void webBrowser_BeforeNewWindow(object sender, EventArgs e)
        {
            WebBrowserExtendedNavigatingEventArgs eventArgs = e as WebBrowserExtendedNavigatingEventArgs;
            webBrowser.Navigate(eventArgs.Url);
            eventArgs.Cancel = true;
        }

        private void btDownLoad_Click(object sender, EventArgs e)
        {
            try
            {
                string sUpdateURL = "http://60.190.19.121/down/qlj.rar";
                workStatus.Text = "准备下载...";//label框提示下载文件
                progressBar.Value = 0;
                WebClient ws = new WebClient();
                ws.DownloadProgressChanged += new DownloadProgressChangedEventHandler(OnDownloadProgressChanged);
                //绑定下载事件，以便于显示当前进度
                ws.DownloadFileCompleted += new AsyncCompletedEventHandler(OnDownloadFileCompleted);
                //绑定下载完成事件，以便于计算总进度
                ws.DownloadFileAsync(new Uri(sUpdateURL), Path.Combine(Application.StartupPath, "qlj.rar"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// 
/// 下载进程变更事件
        private void OnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            
            progressBar.Value = e.ProgressPercentage;
            string total, received;

            received = e.BytesReceived / 1024 + "KB";
            total = e.TotalBytesToReceive / 1024 + "KB";

            workStatus.Text = "已下载" + received + "/总计" + total;
        }
        private void OnDownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show("下载完成");//计算总下载进度，因为我在服务端XML文件里可以得到文件大小，所以我直接用服务端数据，我回头再看看有没有什么好办法
        }





    }
}
