﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using mshtml;
using System.Windows.Forms;
namespace 七龙纪II
{
    class img
    {
        public Image getimg(WebBrowser webBrowser1, string url)//从网页中获得验证码
        {

            int imgnum = -1;
            for (int i = 0; i < webBrowser1.Document.Images.Count; i++)　//获取所有的Image元素
            {
                IHTMLImgElement img = (IHTMLImgElement)webBrowser1.Document.Images[i].DomElement;

                if (img.src.Contains(url))
                    imgnum = i;
            }
            HTMLDocument doc = (HTMLDocument)webBrowser1.Document.DomDocument;
            HTMLBody body = (HTMLBody)doc.body;
            IHTMLControlRange rang = (IHTMLControlRange)body.createControlRange();
            IHTMLControlElement tImg = (IHTMLControlElement)webBrowser1.Document.Images[imgnum].DomElement;
            rang.add(tImg);
            rang.execCommand("Copy", false, null);
            Image RegImg = Clipboard.GetImage();
            Clipboard.Clear();
            return RegImg;
        
        }
        public Image graylevel(Image img, int x)//反馈去掉浅色
        {
            Bitmap Img = new Bitmap(img);
            for (int w = 0; w <= Img.Width - 1; w++)
            {
                for (int h = 0; h <= Img.Height - 1; h++)
                {
                    //如果当前颜色是深色的话，就转化成黑色
                    //否则就转化成白色

                    Color c = Img.GetPixel(w, h);
                    if (c.ToArgb() > x)
                        Img.SetPixel(w, h, Color.White);
                }
            }
            return Img;
        }
        public Image graylevel1(Image img, int x)//反馈去掉浅色  再把深色涂黑
        {
            Bitmap Img = new Bitmap(img);
            for (int w = 0; w <= Img.Width - 1; w++)
            {
                for (int h = 0; h <= Img.Height - 1; h++)
                {
                    //如果当前颜色是深色的话，就转化成黑色
                    //否则就转化成白色

                    Color c = Img.GetPixel(w, h);
                    if (c.ToArgb() > x)
                        Img.SetPixel(w, h, Color.White);
                    else
                        Img.SetPixel(w, h, Color.Black);
                }
            }
            return Img;
        }
        public Image huaxian(Image img, int x1)//  对图像画线
        { Bitmap Img = new Bitmap(img);
        for (int w = 0; w < Img.Height; w++)
        {
          
                //如果当前颜色是深色的话，就转化成黑色
                //否则就转化成白色
            Img.SetPixel(x1, w, Color.Red);
            
        }
        return Img;
        
        }
        public void savepic(Image img, string picname)//保存图像到数据库 picname,pic,x,y
        {
            Bitmap Img = new Bitmap(img);
            string pic = "";
            for (int j = 0; j < Img.Height; j++)
            {
                for (int i = 0; i < Img.Width; i++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy < 55)
                    {
                        pic = pic + "1";
                    }
                    else
                        pic = pic + "0";
                }
                pic += "x";
            }
            try
            {
                string strConnection = "Provider=Microsoft.Jet.OleDb.4.0;";
                strConnection += @"Data Source=tk.mdb";
                //把文件流填充到数组 
                OleDbConnection connection = new OleDbConnection(strConnection);
                // string strSQL = "select   *   from   pic";//   where   picname   = '" + picname + "'";
                //   OleDbCommand myCmd = new OleDbCommand(strSQL, connection);
                try
                {
                    connection.Open();
                    //  OleDbDataReader datareader = myCmd.ExecuteReader();
                    OleDbCommand cmd = new OleDbCommand("insert into   pic(picname,pic,x,y) values ('" + picname + "','" + pic + "'," + Img.Width + "," + Img.Height + ")", connection);

                    cmd.ExecuteNonQuery();
                    connection.Close();

                }
                catch
                {

                    connection.Close();

                }
                finally
                {


                }
            }
            catch { };



        }
        public int shibie(string a, string b, int ierror)//比对字符串 A B， 
        {
            int error = 0;

            for (int i = 0; i < b.Length - 2; i++)
            {
                if (a.Substring(i, 1) != b.Substring(i, 1))
                    error++;
                if (ierror < error)
                    return error + 1;
            }
            return error;
        }
        public string shibie(Image img, int error)  //识别图像，错误数限制 error
        {
            if (img != null)
            {
                Bitmap Img = new Bitmap(img);
                string pic = "";
                for (int j = 0; j < Img.Height; j++)
                {
                    for (int i = 0; i < Img.Width; i++)
                    {
                        Color c = Img.GetPixel(i, j);
                        int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                        if (yy < 55)
                        {
                            pic = pic + "1";
                        }
                        else
                            pic = pic + "0";
                    }
                    pic += "x";
                }
                //  textBox25.Text = pic.Replace("x", "\r\n"); ;

                string strConnection = "Provider=Microsoft.Jet.OleDb.4.0;";
                strConnection += @"Data Source=tk.mdb";
                //把文件流填充到数组 

                OleDbConnection connection = new OleDbConnection(strConnection);
                string strSQL = "select   *   from   pic  where   x= " + (Img.Width) + " and y   = " + (Img.Height) + "";

                OleDbCommand myCmd = new OleDbCommand(strSQL, connection);
                connection.Open();
                OleDbDataReader datareader = myCmd.ExecuteReader();
                string pic2 = "";
                int temperror = 1000;
                string tempvalue = "没找到";
                int temperror1;
                while (datareader.Read())
                {
                    pic2 = datareader["pic"].ToString().Trim();
                    temperror1 = shibie(pic2, pic, error);
                    if (temperror >= temperror1)
                    {

                        temperror = temperror1;
                        tempvalue = datareader["picname"].ToString().Trim();

                    }

                }
                //  textBox30.Text = pic2.Replace("x","\r\n");
                connection.Close();
                if (temperror <= error)
                    return tempvalue;
                else
                    return "没找到";


            }
            return "没找到";
        }

        public void xtouying(Image img, out int x1, out int x2, out int x3, out int x4, out int x5, out int x6, out int x7, out int x8)//图像在X轴投影，输出8个参数，每个字母的最左和左右，
        {
              x1 = 0;
              x2 = 0;
              x3 = 0;
              x4 = 0;
              x5 = 0;
              x6 = 0;
              x7 = 0;
              x8 = 0;
            Bitmap Img = new Bitmap(img);
            int[] red=new int[Img.Width];
            for (int w = 0; w < Img.Width; w++)
            {
                int red1 = 0;
                for (int h = 0; h < Img.Height; h++)
                {
                    //如果当前颜色是深色的话，就转化成黑色
                    //否则就转化成白色
                    Color c = Img.GetPixel(w, h);
                    if (c.ToArgb() <-3150000)
                    {
                        red1 = 1;

                    }
                    else
                        red1 = 0;

                    if (red1 == 1)
                        break;
                }
                red[w] = red1;
            }
            for (int w = 0; w < Img.Width; w++)
            {

                if (red[w] == 1)
                { 
                    x1 = w;
                break;
                }
            }
            for (int w = x1; w < Img.Width; w++)
            {

                if (red[w] == 0)
                {
                    x2 = w;
                    break;
                }
            }
            for (int w = x2; w < Img.Width; w++)
            {

                if (red[w] == 1)
                {
                    x3 = w;
                    break;
                }
            }
            for (int w = x3; w < Img.Width; w++)
            {

                if (red[w] == 0)
                {
                    x4 = w;
                    break;
                }
            }
            for (int w = x4; w < Img.Width; w++)
            {

                if (red[w] == 1)
                {
                    x5 = w;
                    break;
                }
            }
            for (int w = x5; w < Img.Width; w++)
            {

                if (red[w] == 0)
                {
                    x6 = w;
                    break;
                }
            }
            for (int w = x6; w < Img.Width; w++)
            {

                if (red[w] == 1)
                {
                    x7 = w;
                    break;
                }
            }
            for (int w = x7; w < Img.Width; w++)
            {

                if (red[w] == 0)
                {
                    x8 = w;
                    break;
                }
            }

        }
        public Image cutpic(Image img, int x1, int x2)//分割，把原图x1,x2间的复制出来
        {
            Bitmap Img = new Bitmap(img);
            Bitmap img1 = new Bitmap(x2 - x1, Img.Height);
            for (int w = x1; w < x2; w++)
            {
                for (int h = 0; h < img1.Height; h++)
                {
                    //如果当前颜色是深色的话，就转化成黑色
                    //否则就转化成白色
                    img1.SetPixel(w - x1, h, Img.GetPixel(w, h));
                }
            }
            return img1;
        }
        public void getgray(Image img, out int x1, out int x2, out int x3)//返回前图像中三个最大的灰度值（除了白色外）
        {
            int[] x;
            x = new int[256];
            int[] y;
            y = new int[256];
            for (int i = 0; i < 256; i++)
            {
                x[i] = 0;
            }
            Bitmap Img = new Bitmap(img);
            for (int i = 0; i <= Img.Width - 1; i++)
            {
                for (int j = 0; j <= Img.Height - 1; j++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    x[yy]++;
                }
            }  //存灰度数组 
            int temp;
            for (int i = 0; i < 256; i++)
            {
                y[i] = x[i];
            }//复制数组

            for (int i = 1; i < 256; i++)
            {
                for (int j = 1; j < 256; j++)
                {
                    if (y[j] >= y[j - 1])
                    {
                        temp = y[j - 1];
                        y[j - 1] = y[j];
                        y[j] = temp;
                    }
                }
            }//排序
            x1 = 0; x2 = 0; x3 = 0;
            for (int i = 0; i < 256; i++)
            {
                if (y[1] == x[i])
                    x1 = i;
                if (y[2] == x[i])
                    x2 = i;
                if (y[3] == x[i])
                    x3 = i;
            }
            if (getgoodpic(getgraypic(Img, x1), 0, Img.Width - 1).Width > 28)//| getgoodpic(getgraypic(Img, x1), 0, Img.Width - 1).Height > 2)//2
            {
                x1 = x2;
                if (getgoodpic(getgraypic(Img, x1), 0, Img.Width - 1).Width > 28)//|| getgoodpic(getgraypic(Img, x1), 0, Img.Width - 1).Height > 22)//
                    x1 = x3;
            }
        }
        public Image getgoodpic(Image img, int start, int end)//从x=start 到end 获取  最适合的图像（去掉多余的左右和上下）
        {
            int x1 = 999, x2 = 999, y1 = 999, y2 = 999;
            Bitmap Img = new Bitmap(img);
            Bitmap g2 = new Bitmap(img);
            if (Img.GetPixel(1, 1).ToArgb() != Color.Red.ToArgb())
            {
                Bitmap Img1 = new Bitmap(img.Width, img.Height);
                for (int i = start; i <= end; i++)
                {
                    for (int j = 0; j <= Img.Height - 1; j++)
                    {
                        Color c = Img.GetPixel(i, j);
                        int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                        if (yy < 55)
                        {
                            x1 = i;
                            break;
                        }
                    }
                    if (x1 != 999)
                        break;
                }
                for (int i = end; i >= start; i--)
                {
                    for (int j = 0; j <= Img.Height - 1; j++)
                    {
                        Color c = Img.GetPixel(i, j);
                        int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                        if (yy < 55)
                        {
                            x2 = i;
                            break;


                        }
                    }
                    if (x2 != 999)
                        break;
                }
                for (int j = 0; j < Img.Height - 1; j++)
                {
                    for (int i = start; i < end; i++)
                    {
                        Color c = Img.GetPixel(i, j);
                        int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                        if (yy < 55)
                        {
                            y1 = j;
                            break;


                        }
                    }
                    if (y1 != 999)
                        break;
                }
                for (int j = Img.Height - 1; j > 0; j--)
                {
                    for (int i = start; i < end; i++)
                    {
                        Color c = Img.GetPixel(i, j);
                        int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                        if (yy < 55)
                        {
                            y2 = j;
                            break;


                        }
                    }
                    if (y2 != 999)
                        break;
                }
                if (x2 > x1 && y2 > y1)
                {
                    g2 = new Bitmap(x2 - x1 + 1, y2 - y1 + 1);
                    for (int i = x1; i < x2 + 1; i++)
                    {
                        for (int j = y1; j < y2 + 1; j++)
                        {
                            g2.SetPixel(i - x1, j - y1, Img.GetPixel(i, j));
                        }
                    }
                }

                return g2;
            }
            else
                return null;
        }
        public Image getgraypic(Image img)//取得灰度图像  55
        {
            Bitmap Img = new Bitmap(img);
            for (int i = 0; i < Img.Width - 1; i++)
            {
                for (int j = 0; j < Img.Height - 1; j++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy < 55)
                        Img.SetPixel(i, j, Color.Black);
                    else
                        Img.SetPixel(i, j, Color.White);
                }
            }
            return Img;
        }
        public Image getgraypic(Image img, int graylevel)//取得灰度图像  graylevel,
        {
            Bitmap Img = new Bitmap(img);
           
            int start = 999, end = 999;
            for (int i = 1; i <= Img.Width - 2; i++)
            {
                for (int j = 1; j <= Img.Height - 2; j++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy < 55)
                    {
                        start = i;
                        break;
                    }
                }
                if (start != 999)
                    break;
            }
            for (int i = Img.Width - 2; i >= 1; i--)
            {
                for (int j = 1; j <= Img.Height - 2; j++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy < 55)
                    {
                        end = i;
                        break;
                    }
                }
                if (end != 999)
                    break;
            }
            int start1 = 999, end1 = 999;

            for (int j = 1; j <= Img.Height - 2; j++)
            {
                for (int i = 1; i <= Img.Width - 2; i++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy < 55)
                    {
                        start1 = j;
                        break;
                    }
                }
                if (start1 != 999)
                    break;
            }


            for (int j = Img.Height - 2; j > 1; j--)
            {
                for (int i = 1; i <= Img.Width - 2; i++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy < 55)
                    {
                        end1 = j;
                        break;
                    }
                }
                if (end1 != 999)
                    break;
            }
            for (int i = 0; i < Img.Width; i++)
            {
                for (int j = 0; j < Img.Height; j++)
                {
                    Color c = Img.GetPixel(i, j);
                    int yy = (int)(c.R * .299 + c.G * .587 + c.B * .114);
                    if (yy != graylevel)
                    {
                        Img.SetPixel(i, j, Color.White);
                    }
                    else
                        Img.SetPixel(i, j, Color.Black);
                }
            }

            return Img;


        }
  
    }
}
