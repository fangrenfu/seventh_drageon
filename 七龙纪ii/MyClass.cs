﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace 七龙纪II
{
    //资源类型
    public enum resType{wood=0,stone=1,crystal=2,food=3}
    public class ValidCode
    {
        public string x, y;
        public ValidCode(string a, string b)
        {
            x = a;
            y = b;
        }
    }
    public class ErrorCode
    {
        public string x;
        public int y;
        public ErrorCode(string a, int b)
        {
            x = a;
            y = b;
        }
    }
    class ClipImage
    {
        /// <summary>
        /// 切割预览
        /// </summary>
        public Bitmap ImagePreView
        {
            get;
            set;
        }
        /// <summary>
        /// 切割结果
        /// </summary>
        public Bitmap[] ClipImages
        {
            get;
            set;
        }
    }
    class VerifyResult
    {
        public double Likeness
        {
            get;
            set;
        }
        public string Code
        {
            get;
            set;
        }
    }
    /// <summary>
    /// 服务器信息
    /// </summary>
    public class ServerInfo
    {
        /// <summary>
        /// 服务器地址
        /// </summary>
        public string ServerURL
        {
            get;
            set;
        }
        /// <summary>
        /// 登录页面
        /// </summary>
        public string Login
        {
            get;
            set;
        }
        /// <summary>
        /// 游戏默认页面
        /// </summary>
        public string GameIndex
        {
            get;
            set;
        }
        /// <summary>
        /// 游戏提供者
        /// </summary>
        public string ServerProvider
        {
            get;
            set;
        }
        /// <summary>
        /// 注册开始页面
        /// </summary>
        public string RegStart
        {
            get;
            set;
        }
        /// <summary>
        /// 防沉沉迷页面
        /// </summary>
        public string RegID
        {
            get;
            set;
        }
        /// <summary>
        /// 注册完成页面
        /// </summary>
        public string RegDone
        {
            get;
            set;
        }
        /// <summary>
        /// 注销页面
        /// </summary>
        public string LogOut
        {
            get;
            set;
        }
    }
    /// <summary>
    /// 用户信息
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// 账户名
        /// </summary>
        public string UserName
        {
            get;set;
        }
        /// <summary>
        /// 金币
        /// </summary>
        public string Gold
        {
            get;
            set;
        }
        /// <summary>
        /// 石头
        /// </summary>
        public string Stone
        {
            get;
            set;
        }
        /// <summary>
        /// 木头
        /// </summary>
        public string Wood
        {
            get;
            set;
        }
        /// <summary>
        /// 食物
        /// </summary>
        public string Food
        {
            get;
            set;
        }
        /// <summary>
        /// 水晶
        /// </summary>
        public string Crystal
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库库存
        /// </summary>
        public string Store
        {
            get;
            set;
        }
        /// <summary>
        /// 市场
        /// </summary>
        public string Shop
        {
            get;
            set;
        }
        //钻石，礼券
        public string Diamond
        {
            get;
            set;
        }
        public string RecordCount
        {
            get;
            set;
        }

    }
    /// <summary>
    /// 注册信息
    /// </summary>
    public class RegUser
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string UserName
        {
            get;
            set;
        }
        /// <summary>
        /// 身份号
        /// </summary>
        public string UserID
        {
            get;
            set;
        }
    }
    /// <summary>
    /// 建筑信息
    /// </summary>
    public class BuildingURL
    {
        /// <summary>
        /// 石头矿
        /// </summary>
        public string Stone
        {
            get;
            set;
        }
        /// <summary>
        /// 伐木场
        /// </summary>
        public string Wood
        {
            get;
            set;
        }
        /// <summary>
        /// 农场
        /// </summary>
        public string Food
        {
            get;
            set;
        }
        /// <summary>
        /// 水晶矿
        /// </summary>
        public string Crystal
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库
        /// </summary>
        public string Store
        {
            get;
            set;
        }
        /// <summary>
        /// 市场
        /// </summary>
        public string Shop
        {
            get;
            set;
        }
        /// <summary>
        /// 空地
        /// </summary>
        public string[] Blank
        {
            get;
            set;
        }
        /// <summary>
        /// 酒馆
        /// </summary>
        public string Bar
        {
            get;
            set;
        }
        /// <summary>
        /// 市政中心
        /// </summary>
        public string Gov
        {
            get;
            set;
        }
        /// <summary>
        /// 初级兵营
        /// </summary>
        public string Soldier
        {
            get;
            set;
        }
        /// <summary>
        /// 军事指挥所
        /// </summary>
        public string Army
        {
            get;
            set;
        }
    }
    /// <summary>
    /// 服务器设置
    /// </summary>
    public class ServerSetting
    {
       /// <summary>
       /// 木头价格
       /// </summary>
        public string WoodPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 石头价格
        /// </summary>
        public string StonePrice
        {
            get;
            set;
        }
        /// <summary>
        /// 粮食价格
        /// </summary>
        public string FoodPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 水晶价格
        /// </summary>
        public string CrystalPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 伐木场等级
        /// </summary>
        public string WoodLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 石头矿等级
        /// </summary>
        public string StoneLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 农场等级
        /// </summary>
        public string FoodLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 水晶矿等级
        /// </summary>
        public string CrystalLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 市场等级
        /// </summary>
        public string ShopLevel
        {
            get;
            set;
        }
        /// <summary>
        /// 仓库等级
        /// </summary>
        public string StoreLevel
        {
            get;
            set;
        }
    }

    public class SearchResult
    {
        /// <summary>
        /// 图像结果
        /// </summary>
        public Bitmap Image
        {
            get;
            set;
        }
        /// <summary>
        /// 开始横坐标
        /// </summary>
        public int XStart
        {
            get;
            set;
        }
        /// <summary>
        /// 开始纵坐标
        /// </summary>
        public int YStart
        {
            get;
            set;
        }
        /// <summary>
        /// 结束横坐标
        /// </summary>
        public int XEnd
        {
            get;
            set;
        }
        /// <summary>
        /// 结束纵坐标
        /// </summary>
        public int YEnd
        {
            get;
            set;
        }
        //当前处理的X坐标
        public int XCurrent
        {
            get;
            set;
        }
        //当前处理的Y坐标
        public int YCurrent
        {
            get;
            set;
        }
        /// <summary>
        /// 像素总点数
        /// </summary>
        public int pointCount
        {
            get;
            set;
        }

    }
}
