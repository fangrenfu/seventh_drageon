﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data.OleDb;
using System.Data.Sql;
using mshtml;
namespace 七龙纪II
{
    class WebOperate
    {
        public static string getVerifyCode(Bitmap imageSrc,int iVerifyType,string sConnStr)
        {
            int iGreyThreshold; //灰度阈值
            string sVerifyURL; //验证码地址
            //去噪参数，参考宽度以及阈值
            int iPointWidth;
            int iBlackClearThreshold;
            //
            int iMinWidth; //字符的最小宽度
            int iSplitThreshold; //认为是字符区域的投影点数阈值
            int iBorderWidth; //边框宽度
            int iUsedPercent;//有效的图形占总像素的比
            string sResult = "";
            ClipImage clipImage;
            switch (iVerifyType)
            {
                //光辉登录注册验证码
                case 1:
                    sVerifyURL = "http://www.uuplay.com/ValidateCode";
                    iGreyThreshold = 160;
                    iPointWidth = 1;
                    iBlackClearThreshold = 8;
                    iSplitThreshold = 1;
                    iMinWidth = 3;

                    //二值化
                    imageSrc = VerifyCode.toGrey(imageSrc, iGreyThreshold);
                    //黑白去噪
                    imageSrc = VerifyCode.blackClear(imageSrc, iPointWidth, iBlackClearThreshold);
                    clipImage = VerifyCode.imageSplit(imageSrc, iMinWidth, iSplitThreshold);
                    break;
                //昆仑登录验证码
                case 2:
                    sVerifyURL = "http://user.ly.kunlun.com/Verify/login";
                    iGreyThreshold = 112;
                    iPointWidth = 1;
                    iBlackClearThreshold = 8;
                    iSplitThreshold = 1;
                    iMinWidth = 8;

                    //二值化
                    imageSrc = VerifyCode.toGrey(imageSrc, iGreyThreshold);
                    //黑白去噪
                    imageSrc = VerifyCode.blackClear(imageSrc, iPointWidth, iBlackClearThreshold);
                    clipImage = VerifyCode.imageSplit(imageSrc, iMinWidth, iSplitThreshold);
                    break;
                case 3:
                    sVerifyURL = "http://loginserver.ourgame.com/login/OGContorl/heart.ashx";
                    iGreyThreshold = 240;
                    iBorderWidth = 1;
                    iUsedPercent = 1;
                    imageSrc = VerifyCode.toGrey(imageSrc, iGreyThreshold);
                    imageSrc = VerifyCode.deleteBorder(imageSrc, iBorderWidth);
                    clipImage = VerifyCode.imageSplit(imageSrc, iUsedPercent);
                    break;
                case 4:
                    sVerifyURL = "http://www.game5.com/validn/index/814";
                    iGreyThreshold = 240;
                    iBorderWidth = 1;
                    iUsedPercent = 1;
                    imageSrc =(Bitmap) VerifyCode.graylevel(imageSrc, -6150000);
                    imageSrc = VerifyCode.toGrey(imageSrc, iGreyThreshold);
                    imageSrc = VerifyCode.deleteBorder(imageSrc, iBorderWidth);
                    clipImage = VerifyCode.imageSplit(imageSrc, iUsedPercent);
                    break;
                default:
                    sResult = "****";
                    return sResult;
            }
            
            //切割完成
           
            for (int i = 0; i < 8; i++)
            {
                if (clipImage.ClipImages[i] != null)
                {
                    clipImage.ClipImages[i] = VerifyCode.reSizeImage(clipImage.ClipImages[i]);
                    sResult += VerifyCode.searchCode(clipImage.ClipImages[i], sConnStr, sVerifyURL).Code;
                }
            }
            return sResult;
        }
        /// <summary>
        /// 获得验证码图片
        /// </summary>
        /// <param name="wbMail">浏览器</param>
        /// <param name="ImgName">图片名称</param>
        /// <param name="Src">Src字段</param>
        /// <param name="Alt">Alt字段</param>
        /// <returns></returns>
        public static Image getVerifyCodePic(WebBrowser wbMail, string ImgName, string Src, string Alt)
        {
            try
            {
                HTMLDocument doc = (HTMLDocument)wbMail.Document.DomDocument;

                HTMLBody body = (HTMLBody)doc.body;
                IHTMLControlRange rang = (IHTMLControlRange)body.createControlRange();
                IHTMLControlElement Img;

                if (ImgName == "") //如果没有图片的名字,通过Src或Alt中的关键字来取
                {
                    int ImgNum = getPicIndex(wbMail, Src, Alt);
                    if (ImgNum == -1) return null;
                    Img = (IHTMLControlElement)wbMail.Document.Images[ImgNum].DomElement;

                }
                else
                    Img = (IHTMLControlElement)wbMail.Document.All[ImgName].DomElement;

                rang.add(Img);
                rang.execCommand("Copy", false, null);
                Image RegImg = Clipboard.GetImage();
                Clipboard.Clear();
                return RegImg;
            }
            catch (Exception ex)
            {
                throw new Exception("GetRegCodePic" + ex.Message + ex.Data + "\r\n");
            }
        }
        /// <summary>
        /// 判断验证码图片的序号
        /// </summary>
        /// <param name="wbMail">浏览器</param>
        /// <param name="Src">Src字段</param>
        /// <param name="Alt">Alt字段</param>
        /// <returns>图片序号</returns>
        private static int getPicIndex(WebBrowser wbMail, string Src, string Alt)
        {
            try
            {
                int imgnum = -1;
                for (int i = 0; i < wbMail.Document.Images.Count; i++)　//获取所有的Image元素
                {
                    IHTMLImgElement img = (IHTMLImgElement)wbMail.Document.Images[i].DomElement;
                    if (Alt == "")
                    {
                        if (img.src.Contains(Src)) return i;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(img.alt))
                        {
                            if (img.alt.Contains(Alt)) return i;
                        }
                    }
                }
                return imgnum;
            }
            catch (Exception ex)
            {
                throw new Exception("GetPicIndex" + ex.Message + ex.Data + "\r\n");
            }
        }
        /// <summary>
        /// 从网页中获取用户资源信息
        /// </summary>
        /// <param name="webBrowser">浏览器</param>
        /// <param name="sUserName">用户名</param>
        /// <returns>信息类</returns>
        public static UserInfo getUserInfo(WebBrowser webBrowser,string sUserName)
        {
            try
            {
                string sGoldReg = "<DIV id=parentGold>([0-9]*)</DIV>[^<>]*<DIV><IMG[^<>]*>([0-9]*)</DIV>";
                string sStoneReg = "id=resStone>([0-9]*)</DIV>[^<>]*<DIV>([^<>]*)</DIV>";
                string sWoodReg = "id=resWood>([0-9]*)</DIV>[^<>]*<DIV>([^<>]*)</DIV>";
                string sFoodReg = "id=resFood>([0-9]*)</DIV>[^<>]*<DIV>([^<>]*)</DIV>";
                string sCrystalReg = "id=resCrystal>([0-9]*)</DIV>[^<>]*<DIV>([^<>]*)</DIV>";
                string sStoreReg = "<DIV id=resTotal>([0-9]*)/([0-9]*)</DIV>";
                string sShopReg = "<P>最大单数 ([0-9]*) &nbsp;&nbsp; 当前单数 ([0-9]*)</P>";
                string sRecordReg = "<DIV class=pages>[^<>]*<DIV class=count>共 ([0-9]*) 条 </DIV>[^<>]*<DIV class=page>";
                string sDiamondReg = "<LI class=dmd>([0-9]*) </LI>[^<>]*<LI class=df><IMG[^<>]*>[^<>]*</LI>[^<>]*<LI class=dmf>([0-9]*) </LI></UL>";
                UserInfo userInfo = new UserInfo();
                userInfo.UserName = sUserName;
                HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;
              
                string sHtml = doc.documentElement.outerHTML;

                userInfo.Gold = getStringByReg(sHtml, sGoldReg, "/");
                userInfo.Stone = getStringByReg(sHtml, sStoneReg, "/");
                userInfo.Wood = getStringByReg(sHtml, sWoodReg, "/");
                userInfo.Food = getStringByReg(sHtml, sFoodReg, "/");
                userInfo.Crystal = getStringByReg(sHtml, sCrystalReg, "/");
                userInfo.Store = getStringByReg(sHtml, sStoreReg, "/");
                userInfo.Shop = getStringByReg(sHtml, sShopReg, "/");
                userInfo.Diamond = getStringByReg(sHtml, sDiamondReg, "/");
                userInfo.RecordCount = getStringByReg(sHtml, sRecordReg, "/");
                doc = null;
                return userInfo;
            }
            catch (Exception ex)
            {
                throw new Exception("GetUserInfo" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获得建筑URL列表
        /// </summary>
        /// <param name="webBrowser">浏览器</param>
        /// <returns></returns>
        public static BuildingURL getBuildingURL(WebBrowser webBrowser)
        {

            try
            {
                string sWoodReg = "href=\"/(building1.ql\\?bid=[0-9]*)\"[^<>]*msg=\"伐木场 \\( 等级 :([^\"]*)\\)\"";
                string sStoneReg = "href=\"/(building2.ql\\?bid=[0-9]*)\"[^<>]*msg=\"石头矿 \\( 等级 :([^\"]*)\\)\"";
                string sFoodReg = "href=\"/(building3.ql\\?bid=[0-9]*)\"[^<>]*msg=\"农场 \\( 等级 :([^\"]*)\\)\"";
                string sCrystalReg = "href=\"/(building4.ql\\?bid=[0-9]*)\"[^<>]*msg=\"水晶矿 \\( 等级 :([^\"]*)\\)\"";
                string sGovReg = "href=\"/(building5.ql\\?bid=[0-9]*)\"[^<>]*msg=\"市政中心 \\( 等级 :([^\"]*)\\)\"";
                string sBarReg = "href=\"/(building6.ql\\?bid=[0-9]*)\"[^<>]*msg=\"酒馆 \\( 等级 :([^\"]*)\\)\"";
                string sStoreReg = "href=\"/(building7.ql\\?bid=[0-9]*)\"[^<>]*msg=\"仓库 \\( 等级 :([^\"]*)\\)\"";
                string sShopReg = "href=\"/(building8.ql\\?bid=[0-9]*)\"[^<>]*msg=\"市场 \\( 等级 :([^\"]*)\\)\"";
                string sSoldierReg = "href=\"/(building10.ql\\?bid=[0-9]*)\"[^<>]*msg=\"初级兵营 \\( 等级 :([^\"]*)\\)\"";
                string sArmyReg = "href=\"/(building12.ql\\?bid=[0-9]*)\"[^<>]*msg=\"军事指挥所 \\( 等级 :([^\"]*)\\)\"";
                string sBlankReg = "href=\"/(building.ql\\?pid=[0-9]*)\"[^<>]*msg=\"空地\"";
                BuildingURL buildingURL = new BuildingURL();
                HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

                string sHtml = doc.documentElement.outerHTML;
                buildingURL.Stone = getStringByReg(sHtml, sStoneReg, ",");
                buildingURL.Wood = getStringByReg(sHtml, sWoodReg, ",");
                buildingURL.Crystal = getStringByReg(sHtml, sCrystalReg, ",");
                buildingURL.Food = getStringByReg(sHtml, sFoodReg, ",");
                buildingURL.Gov = getStringByReg(sHtml, sGovReg, ",");
                buildingURL.Bar = getStringByReg(sHtml, sBarReg, ",");
                buildingURL.Store = getStringByReg(sHtml, sStoreReg, ",");
                buildingURL.Shop = getStringByReg(sHtml, sShopReg, ",");
                buildingURL.Soldier = getStringByReg(sHtml, sSoldierReg, ",");
                buildingURL.Army = getStringByReg(sHtml, sArmyReg, ",");
                buildingURL.Blank = getStringsByReg(sHtml, sBlankReg);
                
                doc = null;
                return buildingURL;
            }
            catch (Exception ex)
            {
                throw new Exception("GetBuildingURL" + ex.Message + ex.Data);
            }
        }
        #region 正则表达式应用函数
        /// <summary>
        /// 根据正则表达式获取信息
        /// </summary>
        /// <param name="sHtml">内容文档</param>
        /// <param name="sRegString">正则表达式</param>
        /// <param name="sSplit">间隔符号</param>
        /// <returns>返回值</returns>
        private static string getStringByReg(string sHtml, string sRegString,string sSplit)
        {
            try
            {
                Match match = Regex.Match(sHtml, sRegString, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                string sReturnValue = "";
                if (match.Success)
                {
                    sReturnValue = match.Groups[1].Value.Trim();
                    sReturnValue += sSplit + match.Groups[2].Value.Trim();
                }
                match = null;
                return sReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("GetStringByReg" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 根据正则表达式获取信息组成
        /// </summary>
        /// <param name="sHtml">内容文档</param>
        /// <param name="sRegString">正则表达式</param>
        /// <returns>返回值</returns>
        public  static string[] getStringsByReg(string sHtml, string sRegString)
        {
            try
            {
                string[] sBlank = new string[100];
                int iOrder = 0;
                Match match = Regex.Match(sHtml, sRegString, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                while (match.Success && iOrder < sBlank.Length)
                {
                    sBlank[iOrder] = match.Groups[1].Value.Trim();
                    iOrder++;
                    match = match.NextMatch();
                }
                match = null;

                if (iOrder > 0)
                {
                    string[] sReturnValue = new string[iOrder];
                    for (int i = 0; i < iOrder; i++)
                        sReturnValue[i] = sBlank[i];
                    return sReturnValue;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetStringsByReg" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 根据表达式获取信息数组
        /// </summary>
        /// <param name="sHtml">内容</param>
        /// <param name="sRegString">正则表达式</param>
        /// <param name="sSplit">分隔符</param>
        /// <returns></returns>
        public static string[] getStringsByReg(string sHtml, string sRegString,string sSplit)
        {
            try
            {
                string[] sBlank = new string[100];
                int iOrder = 0;
                Match match = Regex.Match(sHtml, sRegString, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                while (match.Success && iOrder < sBlank.Length)
                {
                    sBlank[iOrder] = match.Groups[1].Value.Trim() + sSplit + match.Groups[2].Value.Trim();
                    iOrder++;
                    match = match.NextMatch();
                }
                match = null;
                if (iOrder > 0)
                {
                    string[] sReturnValue = new string[iOrder];
                    for (int i = 0; i < iOrder; i++)
                        sReturnValue[i] = sBlank[i];
                    return sReturnValue;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetStringsByReg2" + ex.Message + ex.Data);
            }
        }
        public static string[] getStringsByReg(string sHtml, string sRegString, string sSplit,string sSplit2)
        {
            try
            {
                string[] sBlank = new string[500];
                int iOrder = 0;
                Match match = Regex.Match(sHtml, sRegString, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                while (match.Success && iOrder < sBlank.Length)
                {
                    sBlank[iOrder] = match.Groups[1].Value.Trim() + sSplit + match.Groups[2].Value.Trim() + sSplit2 + match.Groups[3].Value.Trim();
                    iOrder++;
                    match = match.NextMatch();
                }
                match = null;
                if (iOrder > 0)
                {
                    string[] sReturnValue = new string[iOrder];
                    for (int i = 0; i < iOrder; i++)
                        sReturnValue[i] = sBlank[i];
                    return sReturnValue;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("GetStringsByReg3" + ex.Message + ex.Data);
            }
        }
        #endregion
        /// <summary>
        /// 点击网页中某个链接
        /// </summary>
        /// <param name="sURL">链接地址</param>
        /// <param name="webBrowser">浏览器</param>
        public static bool herfClick( HtmlDocument htmlDocument,string sHerfText,int iOrder)
        {
            try
            {
                bool bReturnValue=false;
                int order = 1;
                for (int i = 0; i < htmlDocument.All.Count; i++)
                {
                    if (htmlDocument.All[i].TagName == "A" &&htmlDocument.All[i].InnerText!=null&&htmlDocument.All[i].InnerText.Trim() == sHerfText)
                    {
                        if (iOrder == order)
                        {
                            bReturnValue = true;
                            htmlDocument.All[i].InvokeMember("click");//引发”CLICK”事件
                            break;
                        }
                        else
                        {
                            order++;
                        }
                    }
                }
                return bReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("HerfClick" + ex.Message + ex.Data);
            }

        }
        /// <summary>
        /// 点击某个按钮
        /// </summary>
        /// <param name="ssButtonValue">按钮数值</param>
        /// <param name="iOrder">按钮次序</param>
        /// <param name="webBrowser">浏览器</param>
        public static int ButtonClick(WebBrowser webBrowser, string sAttributeName, string sButtonValue, int iOrder)
        {
            try
            {
                int iCurrent = 0;
                for (int i = 0; i < webBrowser.Document.All.Count; i++)
                {
                    if (webBrowser.Document.All[i].TagName == "INPUT" && webBrowser.Document.All[i].GetAttribute(sAttributeName).ToString().Trim() == sButtonValue)
                    {

                        if (iCurrent == iOrder)
                        {
                            webBrowser.Document.All[i].InvokeMember("click");//引发”CLICK”事件
                            break;
                        }
                        iCurrent++;
                    }

                }
                return iCurrent;
            }
            catch (Exception ex)
            {
                throw new Exception("ButtonClick" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 在网页中填写数据
        /// </summary>
        /// <param name="webBrowser">浏览器窗口</param>
        /// <param name="sID">ID号</param>
        /// <param name="sDataValue">数据值</param>
        /// <returns>-1,参数有问题，1正常，0，没有找到控件</returns>
        public static int putInData(WebBrowser webBrowser,string sID,string sDataValue)
        {
            try
            {
                int iReturnValue = -1;
                HtmlElement heID = webBrowser.Document.GetElementById(sID);

                if (heID != null)
                {

                    try
                    {
                        heID.Focus();
                    }
                    catch (Exception )
                    {

                    }
                    heID.SetAttribute("value", sDataValue);
                    iReturnValue = 1;
                }
                else
                    iReturnValue = 0;

                heID = null;
                return iReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("PutInData" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 输入数据
        /// </summary>
        /// <param name="webBrowser">网页浏览器</param>
        /// <param name="sName">名称</param>
        /// <param name="iOrder">序号，从0开始</param>
        /// <param name="sDataValue">数值</param>
        /// <returns></returns>
        public static int putInData(WebBrowser webBrowser,string sName,int iOrder,string sDataValue)
        {
            try
            {
                int iReturnValue = -1;

                HtmlElementCollection hecName = webBrowser.Document.All.GetElementsByName(sName);

                if (hecName[iOrder] != null)
                {
                    hecName[iOrder].Focus();
                    hecName[iOrder].SetAttribute("value", sDataValue);
                    iReturnValue = 1;
                }
                else
                    iReturnValue = 0;

                return iReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("PutInData2" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 输入数据，用户下拉菜单选择
        /// </summary>
        /// <param name="webBrowser">浏览器控件</param>
        /// <param name="sName">下拉菜单名称</param>
        /// <param name="iOrder">下拉菜单序号，从0开始</param>
        /// <param name="iSelectIndex">选择的ID序号</param>
        /// <returns></returns>
        public static int putInData(WebBrowser webBrowser, string sName, int iOrder, int iSelectIndex)
        {
            try
            {
                int iReturnValue = -1;

                HtmlElementCollection hecName = webBrowser.Document.All.GetElementsByName(sName);
                if (hecName[iOrder] != null)
                {
                    hecName[iOrder].SetAttribute("selectedIndex", iSelectIndex.ToString());
                    iReturnValue = 1;
                }
                else
                    iReturnValue = 0;

                return iReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("PutInData3" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 生成一个随机的邮件地址
        /// </summary>
        /// <param name="iMinLength">最小长度</param>
        /// <param name="iMaxLength">最大长度</param>
        /// <returns></returns>
        public static string getRandomEmailAddress(int iMinLength,int iMaxLength)
        {
            try
            {
                //邮件服务器列表
                string sEmailHost = "@qq.com|@126.com|@163.com|@china.com|@netease.com|@sohu.com|@yahoo.com";
                //列表数组
                string[] sEmailArray = sEmailHost.Split('|');
                //总数
                int iEmailCount = sEmailArray.Length;
                //随机选择一个服务器
                Random rnd = new Random();
                string sEmailServer = sEmailArray[rnd.Next(iEmailCount)];
                //随机生成一串数字
                string sUserName = getRandomString(iMinLength, iMaxLength,0);
                //放回地址
                return sUserName + sEmailServer;
            }
            catch (Exception ex)
            {
                throw new Exception("getRandomEmailAddress" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 生成一个随机的字符串
        /// </summary>
        /// <param name="iMinLength">最少长度</param>
        /// <param name="iMaxLength">最大长度</param>
        /// <param name="iType">0：所有字符，1：数字，2：字母</param>
        /// <returns></returns>
        public static string getRandomString(int iMinLength,int iMaxLength,int iType)
        {
            try
            {
                //随机数
                Random rnd = new Random();
                //返回值
                string sReturnValue = "";
                string sCurrentSet="";
                //字符串内容组成
                string sAllNumber = "1|2|3|4|5|6|7|8|9|0";
                string sAllChar = "a|b|c|d|e|f|g|h|i|j|k|l|m|n|p|q|r|s|t|u|v|w|x|y|z";
                string sAll = sAllChar + "|" + sAllNumber;
                switch (iType)
                {
                    case 0:
                        sCurrentSet = sAll;
                        break;
                    case 1:
                        sCurrentSet = sAllNumber;
                        break;
                    case 2:
                        sCurrentSet = sAllChar;
                        break;
                    default:
                        sCurrentSet = sAll;
                        break;
                }
                //转换为数组
                string[] saCharArray = sCurrentSet.Split('|');
                //数组长度
                int iCharArrayLength = saCharArray.Length;
                //字符串长度
                int iStringLength = rnd.Next(iMinLength, iMaxLength + 1);
                //生成一个字符串
                for (int i = 0; i < iStringLength; i++)
                {
                    sReturnValue += saCharArray[rnd.Next(iCharArrayLength)];
                }
                //去掉最前面的0
                char[] trimstart = { '0' };
                return sReturnValue.TrimStart(trimstart);
            }
            catch (Exception ex)
            {
                throw new Exception("getRandomString" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获取一个用户名
        /// </summary>
        /// <param name="sConnStr"></param>
        /// <returns></returns>  
        public static RegUser getRegUser(string sConnStr)
        {
            try
            {
                RegUser regUser = new RegUser();
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                OleDbDataReader odrReader;

                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "select top 1 UserName,UserID from RegUser order by newid()";
                odrReader = odCommand.ExecuteReader();
                odrReader.Read();
                regUser.UserName = odrReader["UserName"].ToString();
                regUser.UserID = odrReader["UserID"].ToString();
                odrReader.Close();
                //释放资源
                odcConnection.Dispose();
                odrReader.Dispose();
                odCommand.Dispose();
                return regUser;
            }
            catch (Exception ex)
            {
                throw new Exception("getRegUser" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 增加刚刚注册好的用户到数据库
        /// </summary>
        /// <param name="sUserName">用户名</param>
        /// <param name="sUserID">身份证号</param>
        /// <param name="sEmail">电子邮箱</param>
        /// <param name="sPassword">密码</param>
        /// <returns></returns>
        public static int addUser(string sServerURL,string sUserName, string sUserID, string sEmail, string sPassword,string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "insert into UserList([Server],[RealName],[UserID],[Email],[Password]) values('" + sServerURL + "','" + sUserName + "','" + sUserID + "','" + sEmail + "','" + sPassword + "')";
                int iReturnValue = odCommand.ExecuteNonQuery();

                odcConnection.Dispose();
                odCommand.Dispose();
                return iReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("addUser" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获得服务器列表
        /// </summary>
        /// <param name="ComboBoxServer">服务器列表存储</param>
        /// <param name="sConnStr">链接</param>
        public static void getServerList(ToolStripComboBox ComboBoxServer, string sConnStr)
        {
            try
            {
                //清空
                ComboBoxServer.Items.Clear();
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                OleDbDataReader odrReader;

                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "select ServerName from ServerList order by RecNo";
                odrReader = odCommand.ExecuteReader();
                while (odrReader.Read())
                {
                    ComboBoxServer.Items.Add(odrReader["ServerName"].ToString());
                }
                odrReader.Close();
                //释放资源
                odcConnection.Dispose();
                odrReader.Dispose();
                odCommand.Dispose();
                return;
            }
            catch (Exception ex)
            {
                throw new Exception("getServerList" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获取分段列表
        /// </summary>
        /// <param name="ComboBoxSection">分段</param>
        /// <param name="sServerName">服务器名称</param>
        /// <param name="sConnStr">链接</param>
        public static void getSectionList(ToolStripComboBox ComboBoxSection, string sServerName, string sConnStr)
        {
            ComboBoxSection.Items.Clear();
            OleDbConnection odcConnection = new OleDbConnection(sConnStr);
            OleDbDataReader odrReader;

            odcConnection.Open();
            OleDbCommand odCommand = odcConnection.CreateCommand();
            odCommand.CommandText = "select distinct [Section] from ServerList inner  join UserList on ServerList.ServerURL=UserList.Server where ServerList.ServerName='" + sServerName + "'";
            odrReader = odCommand.ExecuteReader();
            while (odrReader.Read())
            {
                ComboBoxSection.Items.Add(odrReader["Section"].ToString());
            }
            odrReader.Close();
            //选择第一条
            if (ComboBoxSection.Items.Count > 0)
                    ComboBoxSection.SelectedIndex = 0;
            //释放资源
            odcConnection.Dispose();
            odrReader.Dispose();
            odCommand.Dispose();
            
        }
        /// <summary>
        /// 获取账户列表
        /// </summary>
        /// <param name="ComboBoxAccount">账户控件</param>
        /// <param name="dataGridView">账户列表格</param>
        /// <param name="sServerURL">服务器地址</param>
        /// <param name="sSectionName">区段名称，如果是全部，填空白</param>
        /// <param name="sConnStr">连接字符串</param>
        /// <param name="iMinStep">最小任务步骤，如果是全部，填999</param>
        /// <param name="sSearchType">检索类型，全部，正常，锁定，封号</param>
        /// <param name="sGoldMin">最小金币数</param>
        /// <param name="sGoldMax">最大金币数</param>
        /// <returns></returns>
        public static ServerInfo getAccountList(ToolStripComboBox ComboBoxAccount,DataGridView dataGridView,string sServerName,string sSectionName, string sConnStr,int iMinStep,string sSearchType,string sGoldMin,string sGoldMax)
        {
            try
            {
                int iUserOrder = 1;
                string sUserName;
                string sPassword;
                int iGold;
                string sDiamond;
                int iHeroLevel;
                int iMissionStep;
                string sStore;
                string sStone;
                string sWood;
                string sCrystal;
                string sFood;
                string sDateTime;
                string sPlace;//城市坐标
                ServerInfo serverInfo = new ServerInfo();
                int iIndexTemp = ComboBoxAccount.SelectedIndex; //保存当前选择的序号
                ComboBoxAccount.Items.Clear(); //清空所有用户
                dataGridView.Rows.Clear(); //清空所有资料
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                OleDbDataReader odrReader;

                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                string SqlString = "";
                switch (sSearchType)
                {
                    case "全部":
                        break;
                    case "正常":
                        SqlString = " and Lock=0 ";
                        break;
                    case "锁定":
                        SqlString = " and Lock=1 ";
                        break;
                    case "封号":
                        SqlString = " and Lock=2";
                        break;
                    default:
                        break;
                }
                if (sSectionName != "")
                    SqlString += " and [Section]='"+sSectionName+"' ";
                odCommand.CommandText = "select * from UserList inner join ServerList on ServerList.ServerURL=UserList.Server  where ServerList.ServerName='"+sServerName+"' "+SqlString+" and Step< " + iMinStep + " and Gold< "+sGoldMax+" and Gold >= "+sGoldMin+" ";
                odrReader = odCommand.ExecuteReader();
                while (odrReader.Read())
                {
                    sUserName = odrReader["Email"].ToString();
                    sPassword = odrReader["Password"].ToString();
                    iGold = Convert.ToInt32(odrReader["Gold"].ToString());
                    sPlace = odrReader["Place"].ToString();
                    iMissionStep= Convert.ToInt32(odrReader["Step"].ToString());
                    iHeroLevel = Convert.ToInt32(odrReader["HeroLevel"].ToString());
                    sStore = odrReader["Store"].ToString();
                    sStone = odrReader["Stone"].ToString();
                    sWood = odrReader["Wood"].ToString();
                    sCrystal = odrReader["Crystal"].ToString();
                    sFood = odrReader["Food"].ToString();
                    sDiamond = odrReader["Diamond"].ToString();
                    sDateTime=Convert.ToDateTime(odrReader["DateTime"]).ToString("yyyy-MM-dd HH:mm");
                    ComboBoxAccount.Items.Add(sUserName + "," + sPassword);
                    dataGridView.Rows.Add(iUserOrder,sUserName,sPlace,iHeroLevel,"0/0",iMissionStep,iGold,sDiamond,sStore, sWood, sStone, sCrystal, sFood,sDateTime);
                    iUserOrder++;
                    serverInfo.ServerURL= odrReader["ServerURL"].ToString();
                    serverInfo.Login = odrReader["Login"].ToString();
                    serverInfo.GameIndex = odrReader["GameIndex"].ToString();
                    serverInfo.RegStart = odrReader["RegStart"].ToString();
                    serverInfo.RegID = odrReader["RegID"].ToString();
                    serverInfo.RegDone = odrReader["RegDone"].ToString();
                    serverInfo.LogOut = odrReader["LogOut"].ToString();
                    serverInfo.ServerProvider = serverInfo.ServerURL.Substring(serverInfo.ServerURL.IndexOf('.') + 1, serverInfo.ServerURL.Length - serverInfo.ServerURL.IndexOf('.') - 2); //获取服务器提供商
                }
                odrReader.Close();
                if (ComboBoxAccount.Items.Count > 0)
                {
                    if (iIndexTemp < 0 || iIndexTemp > ComboBoxAccount.Items.Count-1)
                    {
                        ComboBoxAccount.SelectedIndex = 0;
                    }
                    else
                    {
                        ComboBoxAccount.SelectedIndex = iIndexTemp;
                    }
                }
                //释放资源
                odcConnection.Dispose();
                odrReader.Dispose();
                odCommand.Dispose();
                return serverInfo;
            }
            catch (Exception ex)
            {
                throw new Exception("getAccountList" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 更新用户信息数据库
        /// </summary>
        /// <param name="userInfo">用户信息类</param>
        /// <param name="sConnStr">连接</param>
        /// <param name="sServerURL">所在服务器</param>
        public static void updateUserInfo(UserInfo userInfo, string sConnStr, string sServerURL)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "update UserList set Gold=" + userInfo.Gold.Split('/')[0] + ",Diamond='"+userInfo.Diamond+"',Store='" + userInfo.Store + "',Stone='" + userInfo.Stone + "',Wood='" + userInfo.Wood + "',Crystal='" + userInfo.Crystal + "',Food='" + userInfo.Food + "',DateTime=getdate()";
                odCommand.CommandText += " where Server='" + sServerURL + "' and Email='" + userInfo.UserName + "'";
                odCommand.ExecuteNonQuery();
                odcConnection.Dispose();
                odCommand.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("updateUserInfo" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 删除指定账号
        /// </summary>
        /// <param name="sUserName">账号名</param>
        /// <param name="sConnStr">连接</param>
        /// <param name="sServerURL">服务器地址</param>
        public static void deleteUser(string sUserName, string sConnStr,string sServerURL)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "update UserList set Section='遭封' from  UserList where Server='" + sServerURL + "' and Email='" + sUserName + "'";
                odCommand.ExecuteNonQuery();
                odcConnection.Dispose();
                odCommand.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("deleteUser" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获取当前账户基本任务完成情况
        /// </summary>
        /// <param name="sUserName">账户名</param>
        /// <param name="sConnStr">连接字符串</param>
        /// <returns></returns>
        public static int getUserMissionStep(string sUserName,string sServerURL,string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "select Step from  UserList where Email='" + sUserName + "' and Server='"+sServerURL+"'";
                int iReturnValue = Convert.ToInt32(odCommand.ExecuteScalar().ToString());
                odCommand.Dispose();
                odcConnection.Dispose();
                return iReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("getUserMissionStep" + ex.Message + ex.Data);
            }
        }
       /// <summary>
       /// 设置用户的当前步骤
       /// </summary>
       /// <param name="sUserName">用户名</param>
       /// <param name="sStep">步骤</param>
       /// <param name="sConnStr">连接</param>
        public static void setUserMissionStep(string sUserName,string sServerURL,int iMissionStep, string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "update UserList set Step=" + iMissionStep.ToString() + " where Email='" + sUserName + "' and Server ='"+sServerURL+"'";
                odCommand.ExecuteNonQuery();
                odCommand.Dispose();
                odcConnection.Dispose();
                return;
            }
            catch (Exception ex)
            {
                throw new Exception("setUserMissionStep" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获取指定建筑的建造按钮
        /// </summary>
        /// <param name="webBrower"></param>
        /// <param name="sBuildingName"></param>
        /// <returns></returns>
        public static HtmlElementCollection getBuildButton(WebBrowser webBrowser, string sBuildingName)
        {
            try
            {
                bool bFind = false;
                HtmlElementCollection hec = webBrowser.Document.GetElementsByTagName("DIV");
                HtmlElementCollection hec2;
                HtmlElementCollection hecReturn=null;
                for (int i = 0; i < hec.Count; i++)
                {    //首先查找类别属性为b_block的物件
                    if (hec[i].GetAttribute("classname") == "b_block") //获得class属性必须使用classname
                    {
                        hec2 = hec[i].GetElementsByTagName("li"); //找到以后取下面的li节
                        for (int j = 0; j < hec2.Count; j++)
                        {    //如果存在某个li节的类为t2_bgc,并且内部名称为建筑名称的，那就表示在列表中存在
                            if (hec2[j].GetAttribute("classname") == "t2_bgc" && hec2[j].InnerText == sBuildingName)
                            {
                                bFind = true;
                                break;
                            }
                        }
                        //继续找确定按钮。
                        if (bFind == true)
                        {
                            hecReturn = hec[i].GetElementsByTagName("input");
                            break; //退出循环
                        }
                        else
                        {
                            hecReturn = null;
                        }

                    }
                }
                return hecReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("getBuildButton" + ex.Message + ex.Data);
            }
        }
        public static int getGoodHeroOrder(WebBrowser webBrowser)
        {
            try
            {
                string sHeroLifeReg = "<LI>生命值：([0-9]*)";
                string[] sHeroLife=new string[10];
                int iReturnValue=1;
                HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

                string sHtml = doc.documentElement.outerHTML;
                sHeroLife = getStringsByReg(sHtml,sHeroLifeReg);
                if(sHeroLife[1]!=null) //如果有两个英雄，就比较一下。
                    iReturnValue=Convert.ToInt32(sHeroLife[0]) > Convert.ToInt32(sHeroLife[1])?1:2;
                return iReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("getGoodHeroOrder" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 从数据库中获取一个中文名字
        /// </summary>
        /// <returns></returns>
        public static string getNickName(string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "select top 1 NickName from  NickList order by newid()";
                string sReturnValue = odCommand.ExecuteScalar().ToString();
                odCommand.Dispose();
                odcConnection.Dispose();
                return sReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception("getUserMissionStep" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 根据访问的名称获取位置数组
        /// </summary>
        /// <param name="webBrowser"></param>
        /// <param name="sVisitType">访问类型风车、水车，打野，恶魔城</param>
        /// <returns></returns>
        public static string[] getVisitPosition(WebBrowser webBrowser, string sVisitType)
        {
            try
            {
                string sHellReg = "y=\"([0-9\\-]*)\" x=\"([0-9\\-]*)\"><IMG src=\"http://[^/]*/images/map/area_17_0.gif"; //恶魔城
                string sWindReg = "y=\"([0-9\\-]*)\" x=\"([0-9\\-]*)\"><IMG src=\"http://[^/]*/images/map/area_71_0.gif"; //风车
                string sWaterReg = "y=\"([0-9\\-]*)\" x=\"([0-9\\-]*)\"><IMG src=\"http://[^/]*/images/map/area_72_0.gif";//水车
                string sWildReg = "y=\"([0-9\\-]*)\" x=\"([0-9\\-]*)\"><IMG src=\"http://[^/]*/images/map/area_2[1-2]*_0.gif";//森林1，石头山2，丘陵3，沼泽4

                string sVisitReg = sHellReg;
                sVisitReg = sVisitType == "风车" ? sWindReg : sVisitReg;
                sVisitReg = sVisitType == "水车" ? sWaterReg : sVisitReg;
                sVisitReg = sVisitType == "打野" ? sWildReg : sVisitReg;

                HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

                string sHtml = doc.documentElement.outerHTML;

                return WebOperate.getStringsByReg(sHtml, sVisitReg, ",");
            }
            catch (Exception ex)
            {
                throw new Exception("getVisitPosition" + ex.Message + ex.Data);
            }
            
        }
        /// <summary>
        /// 获取英雄等级
        /// </summary>
        /// <param name="webBrowser">浏览器</param>
        /// <param name="dataGridView">显示表</param>
        /// <param name="sConnStr">数据库连接</param>
        /// <returns></returns>
        public static string[] getHeroLevel(WebBrowser webBrowser,DataGridView dataGridView,string sServerURL, string sUserName,string sConnStr)
        {
            try
            {
                HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

                string sHtml = doc.documentElement.outerHTML;
                string sHeroReg = "class=lv>LV.([0-9]*)</SPAN> <SPAN class=heroName[0-9]*>([^<>]*)</SPAN>";
                string[] sHeroInfos = WebOperate.getStringsByReg(sHtml, sHeroReg, ",");

                //更新数据库中英雄等级
                string sHeroLevel="0";
                for (int i = 0; i < sHeroInfos.Length; i++)
                {
                    sHeroLevel=Convert.ToInt32(sHeroLevel)>Convert.ToInt32(sHeroInfos[i].Split(',')[0])?sHeroLevel:sHeroInfos[i].Split(',')[0];
                }
                if (sHeroLevel!= "0")
                {
                    OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                    odcConnection.Open();
                    OleDbCommand odCommand = odcConnection.CreateCommand();
                    odCommand.CommandText = "update UserList set HeroLevel=" + sHeroLevel + " where Email='" + sUserName + "' and Server='"+sServerURL+"'";
                    odCommand.ExecuteNonQuery();
                    odCommand.Dispose();
                    odcConnection.Dispose();
                    int iRowCount = dataGridView.Rows.Count;
                    for (int i = 0; i < iRowCount - 1; i++)
                    {
                        if (dataGridView.Rows[i].Cells["账户"].Value.ToString() == sUserName)
                        {  //如果已经存在账户信息了，就更新。
                            dataGridView.Rows[i].Cells["等级"].Value = Convert.ToInt32(sHeroLevel);
                            break;
                        }
                    }
                }
                return sHeroInfos;
            }
            catch (Exception ex)
            {
                throw new Exception("getHeroLevel" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获取城市坐标
        /// </summary>
        /// <param name="webBrowser"></param>
        /// <param name="dataGridView"></param>
        /// <param name="sServerURL"></param>
        /// <param name="sUserName"></param>
        /// <param name="sConnStr"></param>
        /// <returns></returns>
        public static string getCityPlace(WebBrowser webBrowser, DataGridView dataGridView, string sServerURL,string sUserName, string sConnStr)
        {
            try
            {
                HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

                string sHtml = doc.documentElement.outerHTML;
                // <A class=city title="新的城市 (-267 | -5)" href="/city/index.ql?cityId=47334995"
                string sCityPlaceReg = "<A class=city title=\"[^\v]*\\(([^\\)]*)\\)\" href=\"/city/index.ql\\?cityId=([0-9]*)";
                string sCityPlace = WebOperate.getStringByReg(sHtml, sCityPlaceReg, ",");
                if (sCityPlace.Trim() != "")
                {
                    int iRowCount = dataGridView.Rows.Count;
                    sCityPlace=sCityPlace.Replace(" ","");
                    for (int i = 0; i < iRowCount - 1; i++)
                    {
                        if (dataGridView.Rows[i].Cells["账户"].Value.ToString() == sUserName)
                        {  //如果已经存在账户信息了，就更新。
                            if (dataGridView.Rows[i].Cells["坐标"].Value.ToString() == "")
                            {
                                dataGridView.Rows[i].Cells["坐标"].Value = sCityPlace.Split(',')[0].Trim();
                                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                                odcConnection.Open();
                                OleDbCommand odCommand = odcConnection.CreateCommand();
                                odCommand.CommandText = "update UserList set Place='" + sCityPlace.Split(',')[0].Trim() +"' where Email='" + sUserName + "' and Server='" + sServerURL + "'";
                                odCommand.ExecuteNonQuery();
                                odCommand.Dispose();
                                odcConnection.Dispose();
                            }
                            break;
                        }
                    }
                }
                return sCityPlace;
            }
            catch (Exception ex)
            {
                throw new Exception("getHeroLevel" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 获取服务器设置
        /// </summary>
        /// <param name="ServerName">服务器名称</param>
        /// <param name="sConnStr">数据库连接</param>
        /// <returns></returns>
        public static ServerSetting getServerSetting(string ServerName, string sConnStr)
        {
            try
            {
                ServerSetting serverSetting = new ServerSetting();
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                OleDbDataReader odrReader;
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "select top 1 * from Setting inner join ServerList on ServerList.ServerURL=Setting.Server where ServerList.ServerName='"+ServerName+"' ";
                odrReader = odCommand.ExecuteReader();
                while (odrReader.Read())
                {
                    serverSetting.WoodPrice = odrReader["WoodPrice"].ToString();
                    serverSetting.WoodLevel = odrReader["WoodLevel"].ToString();
                    serverSetting.FoodPrice = odrReader["FoodPrice"].ToString();
                    serverSetting.FoodLevel = odrReader["FoodLevel"].ToString();
                    serverSetting.StonePrice = odrReader["StonePrice"].ToString();
                    serverSetting.StoneLevel = odrReader["StoneLevel"].ToString();
                    serverSetting.CrystalPrice = odrReader["CrystalPrice"].ToString();
                    serverSetting.CrystalLevel = odrReader["CrystalLevel"].ToString();
                    serverSetting.ShopLevel = odrReader["ShopLevel"].ToString();
                    serverSetting.StoreLevel = odrReader["StoreLevel"].ToString();
                }
                odrReader.Close();
                odrReader.Dispose();
                odCommand.Dispose();
                odcConnection.Dispose();
                return serverSetting;
            }
            catch (Exception ex)
            {
                throw new Exception("getServerSetting" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 保存设置
        /// </summary>
        /// <param name="ServerName">服务器名称</param>
        /// <param name="serverSetting">服务器设置</param>
        /// <param name="sConnStr">数据库链接</param>
        public static void saveSetting(string ServerName,ServerSetting serverSetting,string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                OleDbDataReader odrReader;
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "update Setting set WoodPrice=" + serverSetting.WoodPrice + ",WoodLevel=" + serverSetting.WoodLevel + ",StonePrice=" + serverSetting.StonePrice + ",StoneLevel=" + serverSetting.StoneLevel + ",";
                odCommand.CommandText +="CrystalPrice="+serverSetting.CrystalPrice+",CryStalLevel="+serverSetting.CrystalLevel+",FoodPrice="+serverSetting.FoodPrice+",FoodLevel="+serverSetting.FoodLevel+",";
                odCommand.CommandText +="ShopLevel="+serverSetting.ShopLevel+",StoreLevel="+serverSetting.StoreLevel+" from Setting inner join ServerList on ServerList.ServerURL=Setting.Server where ServerList.ServerName='" + ServerName + "' ";
                odrReader = odCommand.ExecuteReader();
                
                odrReader.Close();
                odrReader.Dispose();
                odCommand.Dispose();
                odcConnection.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("saveSetting" + ex.Message + ex.Data);
            }
        }
        /// <summary>
        /// 设置锁定状态
        /// </summary>
        /// <param name="sServerName">服务器名称</param>
        /// <param name="sUserName">用户名</param>
        /// <param name="sStatus">锁定状态</param>
        /// <param name="sConnStr">链接字符串</param>
        public static void setLockStatus(string sServerURL, string sUserName, string sStatus, string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "update UserList set Lock='"+sStatus+"' from UserList where Server='"+sServerURL+"' and Email='"+sUserName+"'";
                odCommand.ExecuteNonQuery();
                odCommand.Dispose();
                odcConnection.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("setLockStatue" + ex.Message + ex.Data);
            }
        }
        public static void setAccountBlock(string sServerURL, string sUserName, string sConnStr)
        {
            try
            {
                OleDbConnection odcConnection = new OleDbConnection(sConnStr);
                odcConnection.Open();
                OleDbCommand odCommand = odcConnection.CreateCommand();
                odCommand.CommandText = "update UserList set [Section]='疑封' from UserList  where Server='" + sServerURL + "' and Email='" + sUserName + "'";
                odCommand.ExecuteNonQuery();
                odCommand.Dispose();
                odcConnection.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("setLockStatue" + ex.Message + ex.Data);
            }
        }

        /// <summary>
        /// 获取Npc价格列表
        /// </summary>
        /// <param name="webBrowser"></param>
        /// <returns></returns>
        public static string[] getNpcPrice(WebBrowser webBrowser)
        {
            string sReg = "<td>([0-9]*) 银币</td>";
            HTMLDocument doc = (HTMLDocument)webBrowser.Document.DomDocument;

            string sHtml = doc.documentElement.outerHTML;

            return getStringsByReg(sHtml, sReg);
        }
    }
}
